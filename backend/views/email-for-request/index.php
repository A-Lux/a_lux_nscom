<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\EmailForRequest;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\EmailForRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Элекронная почта';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-for-request-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'status',
                'filter' => EmailForRequest::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(EmailForRequest::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'email:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
