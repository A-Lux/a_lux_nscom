<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\EmailForRequest */

$this->title = 'Создание Элекронная почта';
$this->params['breadcrumbs'][] = ['label' => 'Элекронная почта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-for-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
