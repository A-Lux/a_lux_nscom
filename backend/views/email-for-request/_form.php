<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\EmailForRequest;

/* @var $this yii\web\View */
/* @var $model common\models\EmailForRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-for-request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->dropDownList(EmailForRequest::statusDescription()) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
