<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BonusSystem */

$this->title = 'Создание Бонуса';
$this->params['breadcrumbs'][] = ['label' => 'Бонус', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-system-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
