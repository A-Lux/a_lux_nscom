<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\BonusSystem;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\BonusSystem */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Бонус', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bonus-system-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sum',
            'bonus',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ArrayHelper::getValue(BonusSystem::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'created_at',
        ],
    ]) ?>

</div>
