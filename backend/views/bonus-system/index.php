<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\BonusSystem;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BonusSystemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Бонус';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-system-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'sum',
            'bonus',
            [
                'attribute' => 'status',
                'filter' => BonusSystem::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(BonusSystem::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
