<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BannerMobile */
/* @var $bannerProduct common\models\BannerMobileProduct */

$this->title = 'Редактировать Мобильный баннер: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Мобильный баннер', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="banner-mobile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'bannerProduct' => $bannerProduct,
    ]) ?>

</div>
