<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use common\models\BannerMobile;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $model common\models\BannerMobile */
/* @var $form yii\widgets\ActiveForm */
/* @var $bannerProduct[] common\models\BannerMobileProduct */

$products   = Product::getAll();
?>

<div class="banner-mobile-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Общие</a>
            </li>
            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <li class="nav-item">
                        <a id="<?= $locale?>-tab" data-toggle="tab" href="#lang-<?= $locale?>" role="tab" aria-controls="<?= $locale?>" aria-selected="false" class="nav-link"><?= $locale?></a>
                    </li>
                <? endif; ?>
            <? endforeach; ?>
            <li class="nav-item">
                <a id="product-tab" data-toggle="tab" href="#product" role="tab" aria-controls="product" aria-selected="false" class="nav-link">Продукты</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <div id="lang-<?= $locale?>" role="tabpanel" aria-labelledby="<?= $locale?>-tab" class="tab-pane fade">

                        <?= $form->field($model, 'title_'.$locale)->textInput(['maxlength' => true]) ?>

                    </div>
                <? endif; ?>
            <? endforeach; ?>

            <div id="product" role="tabpanel" aria-labelledby="product-tab" class="tab-pane fade">

                <div class="form-group">
                    <label> Продукты для баннера</label>
                    <select class="form-control products-banner" multiple="multiple" data-placeholder="Выберите товар" style="width: 100%;" name="products[]">
                        <? foreach ($products as $product): ?>
                            <option value="<?= $product->id; ?>"
                                <?= $model->isNewRecord ? '' : in_array($product->id, $bannerProduct) ? 'selected' : ''?> >
                                <?= $product->name; ?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </div>

            </div>

            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'status')->dropDownList(BannerMobile::statusDescription()) ?>

                <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload'            => false ,
                        'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
                        'initialPreviewAsData'  => true,
                        'initialCaption'        => $model->isNewRecord ? '': $model->image,
                        'showRemove'            => true ,
                        'deleteUrl'             => \yii\helpers\Url::to(['/banner-mobile/delete-image', 'id'=> $model->id]),
                    ] ,
                    'options' => ['accept' => 'image/*'],
                ]); ?>

            </div>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
