<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\BannerMobile;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BannerMobileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мобильный баннер';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-mobile-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>100]);
                }
            ],
            'link',
            [
                'attribute' => 'status',
                'filter' => BannerMobile::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(BannerMobile::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            //'sort',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
