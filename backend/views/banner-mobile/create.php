<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BannerMobile */

$this->title = 'Создание Мобильный баннер';
$this->params['breadcrumbs'][] = ['label' => 'Мобильный баннер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-mobile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
