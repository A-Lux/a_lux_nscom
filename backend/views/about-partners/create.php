<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutPartners */

$this->title = 'Создани';
$this->params['breadcrumbs'][] = ['label' => 'Партнеры компании', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-partners-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
