<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Orders;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return
                        Html::a($model->user->email, ['/users/view', 'id' => $model->user->id]);
                },
                'format' => 'raw',
            ],
            'sum',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Orders::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'username',
            'phone',
            'email:email',
            'address',
            'message:ntext',
            'statusPay',
            'typePay',
            [
                'attribute' => 'typePay',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Orders::typeDescription(), $model->typePay);
                },
                'format' => 'raw',
            ],
            'arrival_date',
            'created_at',
        ],
    ]) ?>

</div>
