<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FilterEntity */

$this->title = 'Редактировать Filter Entity: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Filter Entities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="filter-entity-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
