<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\FilterValue;
use common\models\Product;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FilterEntity */
/* @var $form yii\widgets\ActiveForm */


$attributes = \common\models\FilterAttr::getAll();
$values     = FilterValue::getAll();

?>

<div class="filter-entity-form">

    <?php $form = ActiveForm::begin(); ?>

    <h3>Значение</h3>
    <select class="js-example-theme-multiple" name="FilterEntity[value_id]">
        <option></option>
        <? foreach ($attributes as $attribute): ?>
            <optgroup label="<?= $attribute->name; ?> (<?= $attribute->category->name; ?>)">
                <? foreach ($values as $value): ?>
                    <? if($attribute->id == $value->attribute0->id): ?>
                        <option value="<?= $value->id; ?>"><?= $value->name ?></option>
                    <? endif;?>
                <? endforeach; ?>
            </optgroup>
        <? endforeach; ?>
    </select>

    <?= $form->field($model, 'product_id')->dropDownList(Product::getList()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success selectedBtn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
