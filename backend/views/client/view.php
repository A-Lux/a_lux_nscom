<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Client;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div style="display: flex; flex-direction: row; flex-wrap: wrap; justify-content: space-between">
        <p>
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить этот элемент?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <p>
            <? if(Client::getConfirmed($model->id)): ?>
                <?= Html::a('Подтвердить партнера', ['confirmed', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

                <?= Html::a('Отменить', ['not-confirmed', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
            <? endif; ?>
        </p>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return
                        Html::a($model->user->email, ['/users/view', 'id' => $model->user->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Client::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'username',
            'company',
            'region',
            'city',
            'work_phone',
            'mobile_phone',
            'bonus',
            'address',
        ],
    ]) ?>

</div>
