<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use common\widgets\ElfinderImageInput;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $model common\models\ProductsImages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-images-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->dropDownList(Product::getList()) ?>

<!--    --><?//= $form->field($model, 'image')->widget(FileInput::classname(), [
//        'pluginOptions' => [
//            'showUpload'            => false ,
//            'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
//            'initialPreviewAsData'  => true,
//            'initialCaption'        => $model->isNewRecord ? '': $model->image,
//            'showRemove'            => true ,
//            'deleteUrl'             => \yii\helpers\Url::to(['/product-images/delete-image', 'id'=> $model->id]),
//        ] ,'options' => ['accept' => 'image/*'],
//    ]); ?>


    <?= $form->field($model, 'image')->widget(ElfinderImageInput::className()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
