<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Catalog;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Каталог продуктов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'parent_id',
                'value' => function ($model) {
                    return  $model->parent
                        ? Html::a($model->parent->name, ['view', 'id' => $model->parent->id])
                        : '';
                },
                'format' => 'raw',
            ],
            'name',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>100]);
                }
            ],
            [
                'attribute' => 'status',
                'filter' => Catalog::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Catalog::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            //'sort',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
