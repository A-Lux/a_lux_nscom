<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Delivery */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Оплата и доставка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
