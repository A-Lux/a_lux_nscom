<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\GuaranteeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Гарантия и возврат';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guarantee-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'format' => 'raw',
                'attribute' => 'title',
                'value' => function($model){
                    return $model->title;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($model){
                    return $model->content;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
