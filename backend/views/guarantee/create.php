<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Guarantee */

$this->title = 'Создание гарантии';
$this->params['breadcrumbs'][] = ['label' => 'Гарантия и возврат', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guarantee-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
