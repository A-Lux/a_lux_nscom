<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Requisites */

$this->title = 'Создание Реквизита';
$this->params['breadcrumbs'][] = ['label' => 'Реквизиты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requisites-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
