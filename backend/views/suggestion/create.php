<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Suggestion */

$this->title = 'Создание Жалобы и предложения';
$this->params['breadcrumbs'][] = ['label' => 'Жалобы и предложения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suggestion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
