<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RequestSuggestion */

$this->title = 'Редактировать Жалобы и предложение пользователей: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Жалобы и предложение пользователей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="request-suggestion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
