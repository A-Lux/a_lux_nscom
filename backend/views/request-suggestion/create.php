<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RequestSuggestion */

$this->title = 'Создание Жалобы и предложение пользователей';
$this->params['breadcrumbs'][] = ['label' => 'Жалобы и предложение пользователей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-suggestion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
