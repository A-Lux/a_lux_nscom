<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\RequestSuggestion;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\RequestSuggestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Жалобы и предложение пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-suggestion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'status',
                'filter' => RequestSuggestion::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(RequestSuggestion::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'username',
            'phone',
            'email:email',
            //'message:ntext',
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
