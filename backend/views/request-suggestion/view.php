<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\RequestSuggestion;

/* @var $this yii\web\View */
/* @var $model common\models\RequestSuggestion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Жалобы и предложение пользователей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="request-suggestion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'filter' => RequestSuggestion::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(RequestSuggestion::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'username',
            'phone',
            'email:email',
            'message:ntext',
            'created_at',
        ],
    ]) ?>

</div>
