<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\RequestSuggestion;

/* @var $this yii\web\View */
/* @var $model common\models\RequestSuggestion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-suggestion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(RequestSuggestion::statusDescription()) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
