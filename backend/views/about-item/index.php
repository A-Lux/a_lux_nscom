<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\AboutItem;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AboutItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Преимущество компании';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-item-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'format' => 'raw',
                'attribute' => 'name',
                'value' => function($model){
                    return $model->name;
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>50]);
                }
            ],
            [
                'attribute' => 'status',
                'filter' => AboutItem::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(AboutItem::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
