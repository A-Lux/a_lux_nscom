<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;
use common\models\Brand;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Brand */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Бренды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="brand-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php $files = $model->getFile(); ?>
    <?php
    if($files != null)
        $file = ['attribute'=>'file','value' => Html::a($model->file,$files,['target'=>'_blank']),'format' => 'raw' ];
    else
        $file =  ['attribute'=>'file','value' => '']?>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'title',
            'content:ntext',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>150]);
                }
            ],
            $file,
            [
                'attribute' => 'statusButton',
                'filter' => Brand::statusButtonDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Brand::statusButtonDescription(), $model->statusButton);
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
