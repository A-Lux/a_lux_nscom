<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductsFeaturedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Рекомендуемые продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-featured-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return
                        Html::a($model->product->name, ['/product/view', 'id' => $model->product->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'recommended_id',
                'value' => function ($model) {
                    return
                        Html::a($model->recommended->name, ['/product/view', 'id' => $model->recommended->id]);
                },
                'format' => 'raw',
            ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
