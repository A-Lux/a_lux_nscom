<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $model common\models\ProductsFeatured */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-featured-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->dropDownList(Product::getList()) ?>

    <?= $form->field($model, 'recommended_id')->dropDownList(Product::getList()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
