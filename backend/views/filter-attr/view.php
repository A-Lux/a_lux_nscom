<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FilterAttr */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Filter Attrs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="filter-attr-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return
                        Html::a($model->category->name, ['/catalog/view', 'id' => $model->category->id]);
                },
                'format' => 'raw',
            ],
            'name',
            'type',
            'position',
            'sort',
        ],
    ]) ?>

</div>
