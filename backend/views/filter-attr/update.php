<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FilterAttr */

$this->title = 'Редактировать Filter Attr: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Filter Attrs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="filter-attr-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
