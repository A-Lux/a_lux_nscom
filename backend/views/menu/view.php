<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;
use common\models\Menu;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'url:url',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Menu::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'position',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Menu::positionDescription(), $model->position);
                },
                'format' => 'raw',
            ],
            'metaName',
            'metaDesc:ntext',
            'metaKey:ntext',
        ],
    ]) ?>

</div>
