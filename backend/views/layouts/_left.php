<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'По сайту', 'options' => ['class' => 'header']],
                    ['label' => 'Меню', 'icon' => 'bookmark-o', 'url' => ['/menu']],
                    ['label' => 'Каталог', 'icon' => 'bookmark-o', 'url' => ['/catalog']],
                    [
                        'label' => 'Продукция',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Продукт', 'icon' => 'ship', 'url' => ['/product']],
                            ['label' => 'Описание продукта', 'icon' => 'ship', 'url' => ['/product-description']],
                            ['label' => 'Спецификация продукта', 'icon' => 'ship', 'url' => ['/product-specifications']],
                            ['label' => 'Рекомендованные продукты', 'icon' => 'ship', 'url' => ['/products-featured']],
                            ['label' => 'Изображение продуктов', 'icon' => 'ship', 'url' => ['/products-images']],
                        ],
                    ],
                    [
                        'label' => 'Фильтры',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Аттрибут фильтра', 'icon' => 'ship', 'url' => ['/filter-attr']],
                            ['label' => 'Значение фильтра', 'icon' => 'ship', 'url' => ['/filter-value']],
                            ['label' => 'Продукты', 'icon' => 'ship', 'url' => ['/filter-entity']],
                        ],
                    ],
                    [
                        'label' => 'О компании',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'О компании', 'icon' => 'ship', 'url' => ['/about']],
                            ['label' => 'Преимущества', 'icon' => 'th', 'url' => ['/about-item'],],
                            ['label' => 'Партнеры компании', 'icon' => 'handshake-o', 'url' => ['/about-partners'],],
                        ],
                    ],
                    ['label' => 'Заказы', 'icon' => 'puzzle-piece', 'url' => ['/orders']],
                    ['label' => 'Контакты', 'icon' => 'puzzle-piece', 'url' => ['/contact']],
                    ['label' => 'Новости', 'icon' => 'calendar', 'url' => ['/news']],
                    ['label' => 'Оплата и доставка', 'icon' => 'info', 'url' => ['/delivery'],],
                    ['label' => 'Жалобы и предложения', 'icon' => 'ticket', 'url' => ['/suggestion'],],
                    ['label' => 'Гарантия и возврат', 'icon' => 'info', 'url' => ['/guarantee'],],
                    ['label' => 'Реквизиты', 'icon' => 'credit-card', 'url' => ['/requisites']],
                    ['label' => 'Бренды', 'icon' => 'handshake-o', 'url' => ['/brand']],
                    ['label' => 'Город', 'icon' => 'map', 'url' => ['/city']],
                    ['label' => 'Жалобы пользователей', 'icon' => 'ticket', 'url' => ['/request-suggestion']],
                ],
            ]
        ) ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Основное', 'options' => ['class' => 'header']],
                    ['label' => 'Переводы', 'icon' => 'language', 'url' => ['/source-message/']],
                    ['label' => 'Пользователи', 'icon' => 'user-circle-o', 'url' => ['/users']],
                    ['label' => 'Информация о клиентах', 'icon' => 'user-circle-o', 'url' => ['/client']],
                    ['label' => 'Язык', 'icon' => 'language', 'url' => ['/language']],
                    ['label' => 'Баннер', 'icon' => 'columns', 'url' => ['/banner']],
                    ['label' => 'Баннер мобильной', 'icon' => 'columns', 'url' => ['/banner-mobile']],
                    ['label' => 'Логотип', 'icon' => 'image', 'url' => ['/logo']],
                    ['label' => 'Документы', 'icon' => 'image', 'url' => ['/documents']],
                    ['label' => 'Бонусы', 'icon' => 'ship', 'url' => ['/bonus-system']],
                    ['label' => 'Элекронная почта', 'icon' => 'ship', 'url' => ['/email-for-request']],

                ],
            ]
        ) ?>

    </section>

</aside>
