<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSpecifications */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Спецификация продукта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-specifications-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return
                        Html::a($model->product->name, ['/product/view', 'id' => $model->product->id]);
                },
                'format' => 'raw',
            ],
            [
                'format' => 'raw',
                'attribute' => 'title',
                'value' => function($model){
                    return $model->title;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($model){
                    return $model->content;
                }
            ],
        ],
    ]) ?>

</div>
