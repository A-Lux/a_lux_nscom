<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserRequisites */

$this->title = 'Создание User Requisites';
$this->params['breadcrumbs'][] = ['label' => 'User Requisites', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-requisites-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
