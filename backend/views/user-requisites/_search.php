<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\UserRequisitesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-requisites-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'legal_name') ?>

    <?= $form->field($model, 'bin') ?>

    <?= $form->field($model, 'legal_address') ?>

    <?php // echo $form->field($model, 'name_bank') ?>

    <?php // echo $form->field($model, 'bik') ?>

    <?php // echo $form->field($model, 'iik') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'email') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
