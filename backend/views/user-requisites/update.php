<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserRequisites */

$this->title = 'Редактировать User Requisites: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Requisites', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="user-requisites-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
