<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FilterValue */

$this->title = 'Создание Значение фильтров';
$this->params['breadcrumbs'][] = ['label' => 'Значение фильтров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-value-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
