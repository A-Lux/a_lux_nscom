<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FilterValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Значение фильтров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-value-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'attribute_id',
                'value' => function ($model) {
                    return
                        Html::a($model->attribute0->name, ['/filter-attr/view', 'id' => $model->attribute0->id]);
                },
                'format' => 'raw',
            ],
            'name:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
