<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\FilterAttr;
use common\models\Catalog;

/* @var $this yii\web\View */
/* @var $model common\models\FilterValue */
/* @var $form yii\widgets\ActiveForm */

$attributes     = FilterAttr::getAll();
$catalogs       = Catalog::getAll();

?>

<div class="filter-value-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Общие</a>
            </li>
            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <li class="nav-item">
                        <a id="<?= $locale?>-tab" data-toggle="tab" href="#lang-<?= $locale?>" role="tab" aria-controls="<?= $locale?>" aria-selected="false" class="nav-link"><?= $locale?></a>
                    </li>
                <? endif; ?>
            <? endforeach; ?>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <? foreach (\common\models\i18n::locales() as $locale) : ?>
                <? if (Yii::$app->language <> $locale) : ?>
                    <div id="lang-<?= $locale?>" role="tabpanel" aria-labelledby="<?= $locale?>-tab" class="tab-pane fade">

                        <?= $form->field($model, 'name_' . $locale)->textInput(['maxlength' => true]) ?>

                    </div>
                <? endif; ?>
            <? endforeach; ?>

            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <h3>Фильтр</h3>
                <select class="js-example-theme-multiple" name="FilterValue[attribute_id]">
                    <option></option>
                    <? foreach ($catalogs as $catalog): ?>
                        <optgroup label="<?= $catalog->name; ?>">
                            <? foreach ($attributes as $attribute): ?>
                                <? if($catalog->id == $attribute->category->id): ?>
                                    <option value="<?= $attribute->id; ?>"><?= $attribute->name ?></option>
                                <? endif;?>
                            <? endforeach; ?>
                        </optgroup>
                    <? endforeach; ?>
                </select>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
