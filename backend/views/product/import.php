<?php

use yii\helpers\Html;
use backend\models\ImportProducts;

/* @var $this yii\web\View */
/* @var $model ImportProducts */

$this->title = 'Загрузить товары ';
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Загрузить товары';
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_import', [
        'model'         => $model,
    ]) ?>

</div>
