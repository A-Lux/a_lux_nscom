<?php

namespace backend\controllers;

use common\models\FilterValue;
use Yii;
use common\models\FilterEntity;
use backend\models\search\FilterEntitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FilterEntityController implements the CRUD actions for FilterEntity model.
 */
class FilterEntityController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createFilterEntity',
        'view'   => 'viewFilterEntity',
        'update' => 'updateFilterEntity',
        'index'  => 'indexFilterEntity',
        'delete' => 'deleteFilterEntity',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = FilterEntity::className();
            $this->searchModel = FilterEntitySearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the FilterEntity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FilterEntity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FilterEntity::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
