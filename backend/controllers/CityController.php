<?php

namespace backend\controllers;

use Yii;
use common\models\City;
use backend\models\search\CitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createCity',
        'view'   => 'viewCity',
        'update' => 'updateCity',
        'index'  => 'indexCity',
        'delete' => 'deleteCity',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = City::className();
            $this->searchModel = CitySearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = City::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
