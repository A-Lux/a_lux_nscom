<?php

namespace backend\controllers;

use Yii;
use common\models\BonusSystem;
use backend\models\search\BonusSystemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BonusSystemController implements the CRUD actions for BonusSystem model.
 */
class BonusSystemController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createBonusSystem',
        'view'   => 'viewBonusSystem',
        'update' => 'updateBonusSystem',
        'index'  => 'indexBonusSystem',
        'delete' => 'deleteBonusSystem',
    ];

//    protected $permissions = [
//        'create' => 'createNews',
//        'view'   => 'viewNews',
//        'update' => 'updateNews',
//        'index'  => 'indexNews',
//        'delete' => 'deleteNews',
//    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = BonusSystem::className();
            $this->searchModel = BonusSystemSearch::className();

            return true;
        }

        return false;
    }
}
