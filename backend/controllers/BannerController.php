<?php

namespace backend\controllers;

use common\models\BannerProduct;
use Yii;
use common\models\Banner;
use backend\models\search\BannerSearch;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createBanner',
        'view'   => 'viewBanner',
        'update' => 'updateBanner',
        'index'  => 'indexBanner',
        'delete' => 'deleteBanner',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Banner::className();
            $this->searchModel = BannerSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }

        $model = new Banner();

        $products   = Yii::$app->request->post()['products'];

        if ($model->load(Yii::$app->request->post())) {
            $this->createImage($model);

            if($model->save()) {
                if(!empty($products)) {
                    $this->saveProduct($products, $model);
                }
                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }

        $model = $this->findModel($id);
        $oldImage = $model->image;

        $products   = Yii::$app->request->post()['products'];

        $bannerProduct  = BannerProduct::getBannerProduct($model);

        if ($model->load(Yii::$app->request->post())) {
            $this->updateImage($model, $oldImage);

            if($model->save()) {
                if(!empty($products)) {
                    $this->saveProduct($products, $model);
                }
                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'bannerProduct' => $bannerProduct,
        ]);
    }

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Banner::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }

    protected function saveProduct($products, $model)
    {
        BannerProduct::deleteAll(['banner_id' => $model->id]);

        foreach ($products as $product){
                $bannerProduct              = new BannerProduct();
                $bannerProduct->banner_id   = $model->id;
                $bannerProduct->product_id  = $product;
                $bannerProduct->save();
        }
    }
}
