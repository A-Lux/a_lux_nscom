<?php

namespace backend\controllers;

use Yii;
use common\models\Delivery;
use backend\models\search\DeliverySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * DeliveryController implements the CRUD actions for Delivery model.
 */
class DeliveryController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createDelivery',
        'view'   => 'viewDelivery',
        'update' => 'updateDelivery',
        'index'  => 'indexDelivery',
        'delete' => 'deleteDelivery',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Delivery::className();
            $this->searchModel = DeliverySearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the Delivery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Delivery::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
