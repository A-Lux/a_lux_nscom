<?php

namespace backend\controllers;

use Yii;
use common\models\UserRequisites;
use backend\models\search\UserRequisitesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserRequisitesController implements the CRUD actions for UserRequisites model.
 */
class UserRequisitesController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createUserRequisites',
        'view'   => 'viewUserRequisites',
        'update' => 'updateUserRequisites',
        'index'  => 'indexUserRequisites',
        'delete' => 'deleteUserRequisites',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = UserRequisites::className();
            $this->searchModel = UserRequisitesSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new UserRequisites model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserRequisites();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UserRequisites model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UserRequisites model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes image an existing UserRequisites model.
     * If deletion is successful, the browser will be redirected to the 'form' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);

        $this->deleteImage($model);

        if($model->save(false)){
        return $this->redirect(['update?id='.$id]);
        }
    }

    /**
     * Finds the UserRequisites model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserRequisites the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserRequisites::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
