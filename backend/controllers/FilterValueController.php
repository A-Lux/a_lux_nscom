<?php

namespace backend\controllers;

use Yii;
use common\models\FilterValue;
use backend\models\search\FilterValueSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * FilterValueController implements the CRUD actions for FilterValue model.
 */
class FilterValueController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createFilterValue',
        'view'   => 'viewFilterValue',
        'update' => 'updateFilterValue',
        'index'  => 'indexFilterValue',
        'delete' => 'deleteFilterValue',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = FilterValue::className();
            $this->searchModel = FilterValueSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the FilterValue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = FilterValue::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
