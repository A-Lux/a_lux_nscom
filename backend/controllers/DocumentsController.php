<?php

namespace backend\controllers;

use Yii;
use common\models\Documents;
use backend\models\search\DocumentsSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * DocumentsController implements the CRUD actions for Documents model.
 */
class DocumentsController extends BackendController
{
    /**
     * @var array
     */

    protected $permissions = [
        'create' => 'createDocuments',
        'view'   => 'viewDocuments',
        'update' => 'updateDocuments',
        'index'  => 'indexDocuments',
        'delete' => 'deleteDocuments',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Documents::className();
            $this->searchModel = DocumentsSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new Documents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }

        $model = new Documents();

        if ($model->load(Yii::$app->request->post())) {
            $this->createFile($model);

            if($model->save()) {
                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Documents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }

        $model = $this->findModel($id);
        $oldFile = $model->file;

        if ($model->load(Yii::$app->request->post())) {
            $this->updateFile($model, $oldFile);

            if($model->save()) {
                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Documents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes image an existing Documents model.
     * If deletion is successful, the browser will be redirected to the 'form' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);

        $this->deleteImage($model);

        if($model->save(false)){
        return $this->redirect(['update?id='.$id]);
        }
    }

    /**
     * Finds the Documents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Documents::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
