<?php

namespace backend\controllers;

use Yii;
use common\models\OrderedProducts;
use backend\models\search\OrderedProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderedProductsController implements the CRUD actions for OrderedProducts model.
 */
class OrderedProductsController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createOrderedProducts',
        'view'   => 'viewOrderedProducts',
        'update' => 'updateOrderedProducts',
        'index'  => 'indexOrderedProducts',
        'delete' => 'deleteOrderedProducts',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = OrderedProducts::className();
            $this->searchModel = OrderedProductsSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new OrderedProducts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrderedProducts();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OrderedProducts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
