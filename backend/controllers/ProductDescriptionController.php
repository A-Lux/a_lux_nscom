<?php

namespace backend\controllers;

use Yii;
use common\models\ProductDescription;
use backend\models\search\ProductDescriptionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * ProductDescriptionController implements the CRUD actions for ProductDescription model.
 */
class ProductDescriptionController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createNews',
        'view'   => 'viewNews',
        'update' => 'updateNews',
        'index'  => 'indexNews',
        'delete' => 'deleteNews',
    ];

//    /**
//     * @var array
//     */
//    protected $permissions = [
//        'create' => 'createProductDescription',
//        'view'   => 'viewProductDescription',
//        'update' => 'updateProductDescription',
//        'index'  => 'indexProductDescription',
//        'delete' => 'deleteProductDescription',
//    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = ProductDescription::className();
            $this->searchModel = ProductDescriptionSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the ProductDescription model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = ProductDescription::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
