<?php

namespace backend\controllers;

use Yii;
use common\models\Guarantee;
use backend\models\search\GuaranteeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * GuaranteeController implements the CRUD actions for Guarantee model.
 */
class GuaranteeController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createGuarantee',
        'view'   => 'viewGuarantee',
        'update' => 'updateGuarantee',
        'index'  => 'indexGuarantee',
        'delete' => 'deleteGuarantee',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Guarantee::className();
            $this->searchModel = GuaranteeSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the Guarantee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Guarantee::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
