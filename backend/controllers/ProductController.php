<?php

namespace backend\controllers;

use backend\models\ImportProducts;
use common\models\ProductsFeatured;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use Yii;
use common\models\Product;
use backend\models\search\ProductSearch;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createProduct',
        'view'   => 'viewProduct',
        'update' => 'updateProduct',
        'index'  => 'indexProduct',
        'delete' => 'deleteProduct',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Product::className();
            $this->searchModel = ProductSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }

        $model      = new Product();

        if ($model->load(Yii::$app->request->post())) {
            $products   = isset(Yii::$app->request->post()['products'])
                ? Yii::$app->request->post()['products']
                : null;

            $this->createImage($model);
            $model->slug = $this->generateCyrillicToLatin(strip_tags($model->name));

            if($model->save()) {
                if(!empty($products)) {
                    $this->saveProduct($products, $model);
                }
                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }

        $model = $this->findModel($id);
        $oldImage = $model->image;

        $recommendedProducts  = ProductsFeatured::getRecommendedProductId($model);

        if ($model->load(Yii::$app->request->post())) {
            $products   = isset(Yii::$app->request->post()['products'])
                ? Yii::$app->request->post()['products']
                : null;

            $this->updateImage($model, $oldImage);
            $model->slug = $this->generateCyrillicToLatin(strip_tags($model->name) . '-' . $model->id);

            if($model->save()) {
                if(!empty($products)) {
                    $this->saveProduct($products, $model);
                }
                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model'                 => $model,
            'recommendedProducts'   => $recommendedProducts,
        ]);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Product::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }

    protected function saveProduct($products, $model)
    {
        ProductsFeatured::deleteAll(['product_id' => $model->id]);

        foreach ($products as $product){
            $bannerProduct                  = new ProductsFeatured();
            $bannerProduct->product_id      = $model->id;
            $bannerProduct->recommended_id  = $product;
            $bannerProduct->save();
        }
    }

    /**
     * Upload products.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
    public function actionImport()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }

        $model      = new ImportProducts();

        if ($model->load(Yii::$app->request->post())) {

            $excel      = $model->upload();
            $firstKey   = array_shift($excel);

            $array      = [];

            foreach ($excel as $item){
                foreach ($item as $key => $value) {
                    if(array_key_exists($key, $firstKey)){
                        $array[][$firstKey[$key]] = $value;
                    }
                }
            }

            print_r("<pre>");
            print_r($excel);die;



//            $spreadsheet = $reader->load($file->tempName);

//            if($model->save()) {
//
//                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
//                return $this->redirect(['index']);
//            }
        }

        return $this->render('import', [
            'model' => $model,
        ]);
    }
}
