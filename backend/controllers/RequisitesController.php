<?php

namespace backend\controllers;

use Yii;
use common\models\Requisites;
use backend\models\search\RequisitesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * RequisitesController implements the CRUD actions for Requisites model.
 */
class RequisitesController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createRequisites',
        'view'   => 'viewRequisites',
        'update' => 'updateRequisites',
        'index'  => 'indexRequisites',
        'delete' => 'deleteRequisites',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Requisites::className();
            $this->searchModel = RequisitesSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the Requisites model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Requisites::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
