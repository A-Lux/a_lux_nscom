<?php

namespace backend\controllers;

use Yii;
use common\models\ProductSpecifications;
use backend\models\search\ProductSpecificationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * ProductSpecificationsController implements the CRUD actions for ProductSpecifications model.
 */
class ProductSpecificationsController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createNews',
        'view'   => 'viewNews',
        'update' => 'updateNews',
        'index'  => 'indexNews',
        'delete' => 'deleteNews',
    ];

//    /**
//     * @var array
//     */
//    protected $permissions = [
//        'create' => 'createProductSpecifications',
//        'view'   => 'viewProductSpecifications',
//        'update' => 'updateProductSpecifications',
//        'index'  => 'indexProductSpecifications',
//        'delete' => 'deleteProductSpecifications',
//    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = ProductSpecifications::className();
            $this->searchModel = ProductSpecificationsSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the ProductSpecifications model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = ProductSpecifications::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
