<?php

namespace backend\controllers;

use Yii;
use common\models\ProductDownloads;
use backend\models\search\ProductDownloadsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * ProductDownloadsController implements the CRUD actions for ProductDownloads model.
 */
class ProductDownloadsController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createNews',
        'view'   => 'viewNews',
        'update' => 'updateNews',
        'index'  => 'indexNews',
        'delete' => 'deleteNews',
    ];

//    /**
//     * @var array
//     */
//    protected $permissions = [
//        'create' => 'createProductDownloads',
//        'view'   => 'viewProductDownloads',
//        'update' => 'updateProductDownloads',
//        'index'  => 'indexProductDownloads',
//        'delete' => 'deleteProductDownloads',
//    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = ProductDownloads::className();
            $this->searchModel = ProductDownloadsSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new ProductDownloads model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductDownloads();

        if ($model->load(Yii::$app->request->post())) {

            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductDownloads model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the ProductDownloads model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = ProductDownloads::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
