<?php

namespace backend\controllers;

use backend\models\ClientConfirmed;
use Yii;
use common\models\Client;
use backend\models\search\ClientSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ClientController implements the CRUD actions for Client model.
 */
class ClientController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createClient',
        'view'   => 'viewClient',
        'update' => 'updateClient',
        'index'  => 'indexClient',
        'delete' => 'deleteClient',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Client::className();
            $this->searchModel = ClientSearch::className();

            return true;
        }

        return false;
    }

    public function actionConfirmed($id)
    {
        $model = Client::findOne(['id' => $id]);

        if($request    = ClientConfirmed::isConfirmed($model)){
            \Yii::$app->session->setFlash('success', 'Вы успешно подтвердили статус-партнера пользователя!');

            return $this->redirect(['view', 'id' => $id]);
        }else{
            \Yii::$app->session->setFlash('error', 'Временная ошибка, попробуйте позднее!');

            return $this->redirect(['view', 'id' => $id]);
        }
    }

    public function actionNotConfirmed($id)
    {
        $model  = $this->findModel($id);

        if($request    = ClientConfirmed::notConfirmed($model)){
            \Yii::$app->session->setFlash('success', 'Вы успешно отменили статус-партнера пользователя!');

            return $this->redirect(['view', 'id' => $id]);
        }else{
            \Yii::$app->session->setFlash('error', 'Временная ошибка, попробуйте позднее!');

            return $this->redirect(['view', 'id' => $id]);
        }
    }
}
