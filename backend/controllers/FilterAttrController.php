<?php

namespace backend\controllers;

use Yii;
use common\models\FilterAttr;
use backend\models\search\FilterAttrSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * FilterAttrController implements the CRUD actions for FilterAttr model.
 */
class FilterAttrController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createFilterAttr',
        'view'   => 'viewFilterAttr',
        'update' => 'updateFilterAttr',
        'index'  => 'indexFilterAttr',
        'delete' => 'deleteFilterAttr',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = FilterAttr::className();
            $this->searchModel = FilterAttrSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the FilterAttr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = FilterAttr::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
