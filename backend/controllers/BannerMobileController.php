<?php

namespace backend\controllers;

use common\models\BannerMobileProduct;
use Yii;
use common\models\BannerMobile;
use backend\models\search\BannerMobileSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * BannerMobileController implements the CRUD actions for BannerMobile model.
 */
class BannerMobileController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createBanner',
        'view'   => 'viewBanner',
        'update' => 'updateBanner',
        'index'  => 'indexBanner',
        'delete' => 'deleteBanner',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = BannerMobile::className();
            $this->searchModel = BannerMobileSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new BannerMobile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }

        $model = new BannerMobile();

        $products   = Yii::$app->request->post()['products'];

        if ($model->load(Yii::$app->request->post())) {
            $this->createImage($model);

            if($model->save()) {
                $this->saveProduct($products, $model);
                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BannerMobile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }

        $model = $this->findModel($id);
        $oldImage = $model->image;

        $products   = Yii::$app->request->post()['products'];
        $bannerProduct  = BannerMobileProduct::getBannerProduct($model);

        if ($model->load(Yii::$app->request->post())) {
            $this->updateImage($model, $oldImage);

            if($model->save()) {
                $this->saveProduct($products, $model);
                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'bannerProduct' => $bannerProduct,
        ]);
    }

    /**
     * Finds the BannerMobile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = BannerMobile::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }

    protected function saveProduct($products, $model)
    {
        BannerMobileProduct::deleteAll(['banner_id' => $model->id]);

        foreach ($products as $product){
            $bannerProduct              = new BannerMobileProduct();
            $bannerProduct->banner_id   = $model->id;
            $bannerProduct->product_id  = $product;
            $bannerProduct->save();
        }
    }
}
