<?php

namespace backend\controllers;

use common\models\Client;
use Yii;
use common\models\Orders;
use backend\models\search\OrdersSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createNews',
        'view'   => 'viewNews',
        'update' => 'updateNews',
        'index'  => 'indexNews',
        'delete' => 'deleteNews',
    ];

//    /**
//     * @var array
//     */
//    protected $permissions = [
//        'create' => 'createOrders',
//        'view'   => 'viewOrders',
//        'update' => 'updateOrders',
//        'index'  => 'indexOrders',
//        'delete' => 'deleteOrders',
//    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Orders::className();
            $this->searchModel = OrdersSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($model->confirm  == 1){
                Client::addBonus($model->user_id, $model->sum);
            }

            if($model->save()) {
                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
