<?php

namespace backend\controllers;

use Yii;
use common\models\AboutPartners;
use backend\models\search\AboutPartnersSearch;
use yii\web\NotFoundHttpException;
use backend\controllers\BackendController;

/**
 * AboutPartnersController implements the CRUD actions for AboutPartners model.
 */
class AboutPartnersController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createAbout',
        'view'   => 'viewAbout',
        'update' => 'updateAbout',
        'index'  => 'indexAbout',
        'delete' => 'deleteAbout',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = AboutPartners::className();
            $this->searchModel = AboutPartnersSearch::className();

            return true;
        }

        return false;
    }
}
