<?php

namespace backend\controllers;

use Yii;
use common\models\Contact;
use backend\models\search\ContactSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * ContactController implements the CRUD actions for Contact model.
 */
class ContactController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createContact',
        'view'   => 'viewContact',
        'update' => 'updateContact',
        'index'  => 'indexContact',
        'delete' => 'deleteContact',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Contact::className();
            $this->searchModel = ContactSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the Contact model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Contact::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
