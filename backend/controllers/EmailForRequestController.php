<?php

namespace backend\controllers;

use Yii;
use common\models\EmailForRequest;
use backend\models\search\EmailForRequestSearch;

/**
 * EmailForRequestController implements the CRUD actions for EmailForRequest model.
 */
class EmailForRequestController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createEmailForRequest',
        'view'   => 'viewEmailForRequest',
        'update' => 'updateEmailForRequest',
        'index'  => 'indexEmailForRequest',
        'delete' => 'deleteEmailForRequest',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = EmailForRequest::className();
            $this->searchModel = EmailForRequestSearch::className();

            return true;
        }

        return false;
    }
}
