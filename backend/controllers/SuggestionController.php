<?php

namespace backend\controllers;

use Yii;
use common\models\Suggestion;
use backend\models\search\SuggestionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;
use common\models\MultilingualActiveRecord;

/**
 * SuggestionController implements the CRUD actions for Suggestion model.
 */
class SuggestionController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createSuggestion',
        'view'   => 'viewSuggestion',
        'update' => 'updateSuggestion',
        'index'  => 'indexSuggestion',
        'delete' => 'deleteSuggestion',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Suggestion::className();
            $this->searchModel = SuggestionSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the Suggestion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Suggestion::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
        throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
