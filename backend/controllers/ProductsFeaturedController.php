<?php

namespace backend\controllers;

use Yii;
use common\models\ProductsFeatured;
use backend\models\search\ProductsFeaturedSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductsFeaturedController implements the CRUD actions for ProductsFeatured model.
 */
class ProductsFeaturedController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createNews',
        'view'   => 'viewNews',
        'update' => 'updateNews',
        'index'  => 'indexNews',
        'delete' => 'deleteNews',
    ];

//    /**
//     * @var array
//     */
//    protected $permissions = [
//        'create' => 'createProductsFeatured',
//        'view'   => 'viewProductsFeatured',
//        'update' => 'updateProductsFeatured',
//        'index'  => 'indexProductsFeatured',
//        'delete' => 'deleteProductsFeatured',
//    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = ProductsFeatured::className();
            $this->searchModel = ProductsFeaturedSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new ProductsFeatured model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }

        $model = new ProductsFeatured();

        if ($model->load(Yii::$app->request->post())) {

            if($model->product_id == $model->recommended_id){
                \Yii::$app->session->setFlash('warning', 'Рекомендумеый не должен быть тот же товар');
                return $this->redirect(['create']);
            }

            if($model->save()) {
                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($model->product_id == $model->recommended_id){
                \Yii::$app->session->setFlash('warning', 'Рекомендумеый не должен быть тот же товар');
                return $this->redirect(['update', 'id' => $model->id]);
            }

            if($model->save()) {
                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
