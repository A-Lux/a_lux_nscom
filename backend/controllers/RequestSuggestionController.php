<?php

namespace backend\controllers;

use Yii;
use common\models\RequestSuggestion;
use backend\models\search\RequestSuggestionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequestSuggestionController implements the CRUD actions for RequestSuggestion model.
 */
class RequestSuggestionController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createNews',
        'view'   => 'viewNews',
        'update' => 'updateNews',
        'index'  => 'indexNews',
        'delete' => 'deleteNews',
    ];

//    /**
//     * @var array
//     */
//    protected $permissions = [
//        'create' => 'createRequestSuggestion',
//        'view'   => 'viewRequestSuggestion',
//        'update' => 'updateRequestSuggestion',
//        'index'  => 'indexRequestSuggestion',
//        'delete' => 'deleteRequestSuggestion',
//    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = RequestSuggestion::className();
            $this->searchModel = RequestSuggestionSearch::className();

            return true;
        }

        return false;
    }
}
