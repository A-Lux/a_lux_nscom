<?php

namespace backend\models;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use yii\base\Model;
use yii\web\UploadedFile;

class ImportProducts extends Model
{
    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // file required
            [['file'], 'required'],

            [['file'], 'file', 'skipOnEmpty' => false,
                'extensions' => 'xls, xlsx, csv, xlt, xlm, xlsm'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file'         => 'Файл',
        ];
    }

    public function pathFile()
    {
        return \Yii::getAlias('@backend') . '/web/uploads/files/import/';
    }

    public function getFile()
    {
        return ($this->file) ? '/backend/web/uploads/files/import/' . $this->file : '';
    }

    public function upload()
    {
        $file           = UploadedFile::getInstance($this, 'file');
        print_r("<pre>");
        print_r($file);die;
        $objPHPExcel    = IOFactory::load($file->tempName);
        $sheetData      = $objPHPExcel->getActiveSheet()->toArray(
            null, true, true, true
                );

        return $sheetData;
    }
}
