<?php

use yii\db\Migration;

/**
 * Class m200318_065740_add_columns_client_table
 */
class m200318_065740_add_columns_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'address', $this->string(255)->null());
        $this->addColumn('client', 'is_confirmed', $this->integer()->defaultValue(0)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('client', 'address');
        $this->dropColumn('client', 'is_confirmed');
    }
}
