<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products_images}}`.
 */
class m200305_143302_create_products_images_table extends Migration
{
    public $table               = 'products_images';
    public $productTable        = 'product';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'product_id'                => $this->integer()->null(),
            'image'                     => $this->string(255)->null(),
            'created_at'                => $this->dateTime()->null(),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->table}}}", 'product_id', "{{{$this->productTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->productTable}_{$this->table}", "{{{$this->productTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
