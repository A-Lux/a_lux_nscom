<?php

use yii\db\Migration;

/**
 * Class m200305_043019_add_hit_and_stock_product
 */
class m200305_043019_add_hit_and_stock_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'hit', $this->integer()->defaultValue(0)->null());
        $this->addColumn('product', 'stock', $this->integer()->defaultValue(0)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'hit');
        $this->dropColumn('product', 'stock');
    }
}
