<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bonus_system}}`.
 */
class m200310_031913_create_bonus_system_table extends Migration
{
    public $table               = 'bonus_system';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'sum'                       => $this->integer()->null(),
            'bonus'                     => $this->integer()->null(),
            'status'                    => $this->integer()->defaultValue(1)->notNull(),
            'created_at'                => $this->dateTime()->null(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
