<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%filter_entity}}`.
 */
class m200220_041642_create_filter_entity_table extends Migration
{
    public $table               = 'filter_entity';
    public $valueTable          = 'filter_value';
    public $productTable        = 'product';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'value_id'      => $this->integer()->notNull(),
            'product_id'    => $this->integer()->notNull(),
        ], $tableOptions);


        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }

        $this->addForeignKey("fk_{$this->table}_{$this->valueTable}", "{{{$this->table}}}", 'value_id', "{{{$this->valueTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->table}}}", 'product_id', "{{{$this->productTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->valueTable}", "{{{$this->valueTable}}}");
        $this->dropForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->productTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
