<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%language}}`.
 */
class m200128_040548_create_language_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%language}}', [
            'id'        => $this->primaryKey(),
            'name'      => $this->string(255)->notNull(),
            'code'      => $this->string(128)->notNull(),
            'image'     => $this->string(255)->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%language}}');
    }
}
