<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%favorites_product}}`.
 */
class m200304_064643_create_favorites_product_table extends Migration
{
    public $table               = 'favorites_product';
    public $userTable           = 'user';
    public $productTable        = 'product';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'user_id'                   => $this->integer()->null(),
            'product_id'                => $this->integer()->null(),
            'created_at'                => $this->dateTime()->null(),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->userTable}", "{{{$this->table}}}", 'user_id', "{{{$this->userTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->table}}}", 'product_id', "{{{$this->productTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->userTable}_{$this->table}", "{{{$this->userTable}}}");
        $this->dropForeignKey("fk_{$this->productTable}_{$this->table}", "{{{$this->productTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
