<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%request_suggestion}}`.
 */
class m200304_050245_create_request_suggestion_table extends Migration
{
    public $table               = 'request_suggestion';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'status'                    => $this->integer()->defaultValue(1)->notNull(),
            'username'                  => $this->string(255)->null(),
            'phone'                     => $this->string(255)->null(),
            'email'                     => $this->string(128)->null(),
            'message'                   => $this->text()->null(),
            'created_at'                => $this->dateTime()->null(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
