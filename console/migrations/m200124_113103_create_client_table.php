<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client}}`.
 */
class m200124_113103_create_client_table extends Migration
{
    public $table               = 'client';
    public $userTable           = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'user_id'                   => $this->integer()->notNull(),
            'status'                    => $this->integer()->defaultValue(1)->notNull(),
            'username'                  => $this->string(255)->notNull(),
            'company'                   => $this->string(255)->null(),
            'region'                    => $this->string(128)->null(),
            'city'                      => $this->string(128)->null(),
            'work_phone'                => $this->string(128)->null(),
            'mobile_phone'              => $this->string(128)->null(),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->userTable}", "{{{$this->table}}}", 'user_id', "{{{$this->userTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->userTable}_{$this->table}", "{{{$this->userTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
