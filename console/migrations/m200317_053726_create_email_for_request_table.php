<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%email_for_request}}`.
 */
class m200317_053726_create_email_for_request_table extends Migration
{
    public $table               = 'email_for_request';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'status'        => $this->integer()->defaultValue(0)->null(),
            'email'         => $this->string(255)->notNull(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
