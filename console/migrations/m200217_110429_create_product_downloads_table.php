<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_downloads}}`.
 */
class m200217_110429_create_product_downloads_table extends Migration
{
    public $table               = 'product_downloads';
    public $translationTable    = 'product_downloads_translation';
    public $productTable        = 'product';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'product_id'    => $this->integer()->notNull(),
            'name'          => $this->string(255)->notNull(),
            'file'          => $this->text()->null(),
        ], $tableOptions);

        $this->createTable("{{{$this->translationTable}}}", [
            'id'            => $this->integer()->notNull(),
            'language'      => $this->string(16)->notNull(),
            'name'          => $this->string(255)->null(),
            'file'          => $this->text()->null(),
        ], $tableOptions);

        $this->addPrimaryKey("pk_{$this->translationTable}_id_language", "{{{$this->translationTable}}}", ['id', 'language']);
        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}", 'id', "{{{$this->table}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->createIndex("idx_{$this->translationTable}_language", "{{{$this->translationTable}}}", 'language');

        $this->addForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->table}}}", 'product_id', "{{{$this->productTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->productTable}}}");
        $this->dropForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
