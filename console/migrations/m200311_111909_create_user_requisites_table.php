<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_requisites}}`.
 */
class m200311_111909_create_user_requisites_table extends Migration
{
    public $table               = 'user_requisites';
    public $userTable           = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                 => $this->primaryKey(),
            'user_id'            => $this->integer()->null(),
            'legal_name'         => $this->string(255)->null(),
            'bin'                => $this->string(255)->null(),
            'legal_address'      => $this->string(255)->null(),
            'name_bank'          => $this->string(255)->null(),
            'bik'                => $this->string(255)->null(),
            'iik'                => $this->string(255)->null(),
            'phone'              => $this->string(255)->null(),
            'email'              => $this->string(255)->null(),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->userTable}", "{{{$this->table}}}", 'user_id', "{{{$this->userTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->userTable}_{$this->table}", "{{{$this->userTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
