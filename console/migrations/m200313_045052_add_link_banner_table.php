<?php

use yii\db\Migration;

/**
 * Class m200313_045052_add_link_banner_table
 */
class m200313_045052_add_link_banner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('banner', 'link', $this->string(255)->null());
        $this->addColumn('banner', 'status', $this->integer()->defaultValue(0)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('banner', 'link');
        $this->dropColumn('banner', 'link');
    }
}
