<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%about_partners}}`.
 */
class m200211_084250_create_about_partners_table extends Migration
{
    public $table               = 'about_partners';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'        => $this->primaryKey(),
            'status'    => $this->integer()->defaultValue(1)->notNull(),
            'name'      => $this->string(255)->notNull(),
            'link'      => $this->string(128)->null(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
