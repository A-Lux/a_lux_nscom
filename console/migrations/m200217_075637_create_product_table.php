<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m200217_075637_create_product_table extends Migration
{
    public $table               = 'product';
    public $translationTable    = 'product_translation';
    public $catalogTable        = 'catalog';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'category_id'   => $this->integer()->notNull(),
            'status'        => $this->integer()->defaultValue(1)->notNull(),
            'name'          => $this->string(255)->notNull(),
            'code'          => $this->string(255)->null(),
            'price'         => $this->integer()->null(),
            'price_partner' => $this->integer()->null(),
            'slug'          => $this->text()->notNull(),
            'content'       => $this->text()->null(),
            'image'         => $this->string(255)->null(),
            'sort'          => $this->integer()->null(),
            'metaName'      => $this->string(255)->null(),
            'metaDesc'      => $this->text()->null(),
            'metaKey'       => $this->text()->null(),
            'create_at'     => $this->timestamp()->null(),
        ], $tableOptions);

        $this->createTable("{{{$this->translationTable}}}", [
            'id'            => $this->integer()->notNull(),
            'language'      => $this->string(16)->notNull(),
            'name'          => $this->string(255)->null(),
            'content'       => $this->text()->null(),
            'metaName'      => $this->string(255)->null(),
            'metaDesc'      => $this->text()->null(),
            'metaKey'       => $this->text()->null(),
        ], $tableOptions);

        $this->addPrimaryKey("pk_{$this->translationTable}_id_language", "{{{$this->translationTable}}}", ['id', 'language']);
        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}", 'id', "{{{$this->table}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->createIndex("idx_{$this->translationTable}_language", "{{{$this->translationTable}}}", 'language');

        $this->addForeignKey("fk_{$this->table}_{$this->catalogTable}", "{{{$this->table}}}", 'category_id', "{{{$this->catalogTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->catalogTable}", "{{{$this->catalogTable}}}");
        $this->dropForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
