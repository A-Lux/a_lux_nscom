<?php

use yii\db\Migration;

/**
 * Class m200326_052552_add_category_columns
 */
class m200326_052552_add_category_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('filter_attr', 'category_id', $this->integer()->defaultValue(0)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('filter_attr', 'category_id');
    }
}
