<?php

use yii\db\Migration;

/**
 * Class m200310_034737_add_bonus_columns
 */
class m200310_034737_add_bonus_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'bonus', $this->integer()->defaultValue(0)->null());
        $this->addColumn('orders', 'confirm', $this->integer()->defaultValue(0)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('client', 'bonus');
        $this->dropColumn('orders', 'confirm');
    }
}
