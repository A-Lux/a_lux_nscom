<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ordered_product}}`.
 */
class m200228_114033_create_ordered_product_table extends Migration
{
    public $table               = 'ordered_products';
    public $orderTable          = 'orders';
    public $productTable        = 'product';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'order_id'                  => $this->integer()->null(),
            'product_id'                => $this->integer()->null(),
            'count'                     => $this->integer()->null(),
            'created_at'                => $this->dateTime()->null(),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->orderTable}", "{{{$this->table}}}", 'order_id', "{{{$this->orderTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->table}}}", 'product_id', "{{{$this->productTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->orderTable}_{$this->table}", "{{{$this->orderTable}}}");
        $this->dropForeignKey("fk_{$this->productTable}_{$this->table}", "{{{$this->productTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
