<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "products_featured".
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $recommended_id
 * @property string|null $created_at
 *
 * @property Product $product
 * @property Product $recommended
 */
class ProductsFeatured extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products_featured';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'recommended_id'], 'integer'],
            [['created_at'], 'safe'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['recommended_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['recommended_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'product_id'        => 'Продукт',
            'recommended_id'    => 'Рекомендованный продукт',
            'created_at'        => 'Дата создания',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->created_at    = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * Gets query for [[Recommended]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecommended()
    {
        return $this->hasOne(Product::className(), ['id' => 'recommended_id']);
    }

    public static function getRecommendedProduct($product)
    {
        $recommended    = [];
        $mainFeatured   = self::findAll(['product_id' => $product->id]);

        foreach ($mainFeatured as $featured){
            $recommended[]  = $featured->recommended;
        }

        return $recommended;
    }

    public static function getRecommendedProductId($model)
    {
        $mainFeatured  = self::findAll(['product_id' => $model->id]);
        $array = [];

        foreach ($mainFeatured as $featured){
            $array[]    = $featured->recommended_id;
        }

        return $array;
    }
}
