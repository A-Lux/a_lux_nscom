<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "email_for_request".
 *
 * @property int $id
 * @property int|null $status
 * @property string $email
 */
class EmailForRequest extends \yii\db\ActiveRecord
{
    const STATUS_DISABLED       = 0;
    const STATUS_ORDER          = 1;
    const STATUS_SUGGESTION     = 2;
    const STATUS_ALL            = 16;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email_for_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['email'], 'required'],
            [['email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'status'    => 'Статус',
            'email'     => 'Электронная почта',
        ];
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_ALL            => 'all',
            self::STATUS_ORDER          => 'order',
            self::STATUS_SUGGESTION     => 'suggestion',
            self::STATUS_DISABLED       => 'disabled',
        ];
    }

    /**
     * @return array
     */
    public static function statusDescription()
    {
        return [
            self::STATUS_ALL            => 'все',
            self::STATUS_ORDER          => 'заказы',
            self::STATUS_SUGGESTION     => 'жалобы',
            self::STATUS_DISABLED       => 'закрытый',
        ];
    }

    public static function getAll()
    {
        return self::findAll(['status' => self::STATUS_ALL]);
    }

    public static function getOrder()
    {
        return self::findAll(['status' => self::STATUS_ORDER]);
    }

    public static function getSuggestion()
    {
        return self::findAll(['status' => self::STATUS_SUGGESTION]);
    }
}
