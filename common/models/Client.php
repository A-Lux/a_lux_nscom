<?php

namespace common\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property int $user_id
 * @property int $status
 * @property int $bonus
 * @property int $is_confirmed
 * @property string $username
 * @property string|null $company
 * @property string|null $region
 * @property string|null $city
 * @property string|null $work_phone
 * @property string|null $mobile_phone
 * @property string|null $address
 *
 * @property User $user
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * Client status user
     */
    const STATUS_USER       = 1;
    /**
     * Client status partner
     */
    const STATUS_PARTNER    = 2;


    const NOT_CONFIRMED_PARTNER     = 0;
    const IS_CONFIRMED_PARTNER      = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_PARTNER    => 'partner',
            self::STATUS_USER       => 'user',
        ];
    }

    /**
     * @return array
     */
    public static function statusDescription()
    {
        return [
            self::STATUS_PARTNER    => 'Партнер',
            self::STATUS_USER       => 'Пользователь',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['user_id', 'status', 'is_confirmed'], 'integer'],

            ['status', 'default', 'value' => self::STATUS_USER],
            ['status', 'in', 'range' => [self::STATUS_USER, self::STATUS_PARTNER]],

            [['username', 'company', 'address'], 'string', 'max' => 255],
            [['region', 'city', 'work_phone', 'mobile_phone'], 'string', 'max' => 128],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'user_id'       => 'Пользователь',
            'status'        => 'Статус',
            'is_confirmed'  => 'Подтверждение',
            'username'      => 'Имя',
            'company'       => 'Компания',
            'region'        => 'Регион',
            'city'          => 'Город',
            'address'       => 'Адрес',
            'work_phone'    => 'Рабочий телефон',
            'mobile_phone'  => 'Мобильный телефон',
            'bonus'         => 'Бонус',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param User $user
     * @return mixed
     */
    public static function getClient($user)
    {
        $model  = self::findOne(['user_id'  => $user->id]);

        if (null == $model) {
            $client = new Client();
            $model = $client->createClient($user);
        }

        return $model;
    }

    /**
     * Create new client
     * @param User $user
     * @return mixed
     */
    protected function createClient($user)
    {
        $model   = new Client();

        $model->user_id     = $user->id;
        $model->username    = $user->email;
        $model->status      = self::STATUS_USER;
        $model->save();

        return $model;
    }

    public static function addBonus($user_id, $sum)
    {
        if($user_id !== null){
            $client     = self::findOne(['user_id' => $user_id]);
            $bonus      = $sum * (1/100);
            $client->bonus  += $bonus;
            $client->save();
        }

        return true;
    }

    public static function getOne()
    {
        if(!Yii::$app->user->isGuest)
        {
            return self::findOne(['user_id' => Yii::$app->user->identity->id]);
        }

        return null;
    }

    public static function getConfirmed($user_id)
    {
        $user   = self::findOne(['id' => $user_id]);

        if($user->status == self::STATUS_PARTNER && $user->is_confirmed == self::NOT_CONFIRMED_PARTNER){
            return true;
        }

        return false;
    }
}
