<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_description".
 *
 * @property int $id
 * @property int $product_id
 * @property string $title
 * @property string|null $content
 *
 * @property Product $product
 * @property ProductDescriptionTranslation[] $productDescriptionTranslations
 */
class ProductDescription extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_description';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'title'], 'required'],
            [['product_id'], 'integer'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'product_id'    => 'Продукт',
            'title'         => 'Заголовок',
            'content'       => 'Содержание',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => ProductDescriptionTranslation::className(),
                'tableName'     => ProductDescriptionTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * Gets query for [[ProductDescriptionTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductDescriptionTranslations()
    {
        return $this->hasMany(ProductDescriptionTranslation::className(), ['id' => 'id']);
    }
}
