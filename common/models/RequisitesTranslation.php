<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "requisites_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $source
 * @property string|null $value
 *
 * @property Requisites $id0
 */
class RequisitesTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requisites_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['value'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['source'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Requisites::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'source' => 'Source',
            'value' => 'Value',
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Requisites::className(), ['id' => 'id']);
    }
}
