<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_downloads".
 *
 * @property int $id
 * @property int $product_id
 * @property string $name
 * @property string|null $file
 *
 * @property Product $product
 * @property ProductDownloadsTranslation[] $productDownloadsTranslations
 */
class ProductDownloads extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_downloads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'name'], 'required'],
            [['product_id'], 'integer'],
            [['file'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'product_id'    => 'Продукт',
            'name'          => 'Наименование',
            'file'          => 'Файл',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => ProductDownloadsTranslation::className(),
                'tableName'     => ProductDownloadsTranslation::tableName(),
                'attributes'    => ['name', 'file'],
            ],
        ]);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * Gets query for [[ProductDownloadsTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductDownloadsTranslations()
    {
        return $this->hasMany(ProductDownloadsTranslation::className(), ['id' => 'id']);
    }
}
