<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $address
 * @property string|null $header_phone
 * @property string|null $contact_phone
 * @property string|null $iframe
 *
 * @property ContactTranslation[] $contactTranslations
 */
class Contact extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'required'],
            [['address', 'contact_phone', 'iframe'], 'string'],
            [['header_phone'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'                => 'ID',
            'address'           => 'Адрес',
            'header_phone'      => 'Телефон в шапке',
            'contact_phone'     => 'Телефон-контакт',
            'iframe'            => 'Iframe',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => ContactTranslation::className(),
                'tableName'     => ContactTranslation::tableName(),
                'attributes'    => ['address'],
            ],
        ]);
    }

    /**
     * Gets query for [[ContactTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContactTranslations()
    {
        return $this->hasMany(ContactTranslation::className(), ['id' => 'id']);
    }

    public static function getOne()
    {
        return self::find()->orderBy(['id' => SORT_ASC])->one();
    }
}
