<?php

namespace common\models;

use frontend\models\search\AboutSearch;
use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property int $status
 * @property string $title
 * @property string|null $content
 * @property string|null $image
 *
 * @property AboutTranslation[] $aboutTranslations
 */
class About extends MultilingualActiveRecord
{
    const STATUS_MAIN           = 1;
    const STATUS_SECOND         = 2;
    const STATUS_THIRD          = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['title', 'status'], 'required'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 128],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'status'    => 'Статус',
            'title'     => 'Заголовок',
            'content'   => 'Содержание',
            'image'     => 'Изображение',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => AboutTranslation::className(),
                'tableName'     => AboutTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_MAIN       => 'main',
            self::STATUS_SECOND     => 'second',
            self::STATUS_THIRD      => 'third',
        ];
    }

    /**
     * @return array
     */
    public static function statusDescription()
    {
        return [
            self::STATUS_MAIN       => 'основной',
            self::STATUS_SECOND     => 'второй',
            self::STATUS_THIRD      => 'третий',
        ];
    }

    /**
     * Gets query for [[AboutTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAboutTranslations()
    {
        return $this->hasMany(AboutTranslation::className(), ['id' => 'id']);
    }

    public function path()
    {
        return Yii::getAlias('@backend') . '/web/uploads/images/about/';
    }

    public function getImage()
    {
        return ($this->image) ? '/backend/web/uploads/images/about/' . $this->image : '';
    }

    public static function getMain()
    {
        $request    = About::findOne(['status' => self::STATUS_MAIN]);

        return $request;
    }

    public static function getSecond()
    {
        $request    = About::findOne(['status' => self::STATUS_SECOND]);

        return $request;
    }

    public static function getThird()
    {
        $request    = AboutSearch::findAll(['status' => self::STATUS_THIRD]);

        return $request;
    }
}
