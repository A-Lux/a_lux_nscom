<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "brand".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $title
 * @property string|null $content
 * @property string|null $image
 * @property string|null $file
 * @property int $statusButton
 * @property string|null $slug
 *
 * @property BrandTranslation[] $brandTranslations
 */
class Brand extends MultilingualActiveRecord
{
    const STATUS_BUTTON_DISABLED       = 0;
    const STATUS_BUTTON_ACTIVE         = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['statusButton'], 'integer'],
            [['name', 'title', 'image', 'file', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'name'          => 'Наименование',
            'title'         => 'Заголовок',
            'content'       => 'Содержание',
            'image'         => 'Изображение',
            'file'          => 'Файл',
            'statusButton'  => 'Статус кнопки',
            'slug'          => 'slug',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => BrandTranslation::className(),
                'tableName'     => BrandTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * @return array
     */
    public static function statusesButton()
    {
        return [
            self::STATUS_BUTTON_ACTIVE          => 'disabled',
            self::STATUS_BUTTON_DISABLED        => 'active',
        ];
    }

    /**
     * @return array
     */
    public static function statusButtonDescription()
    {
        return [
            self::STATUS_BUTTON_ACTIVE          => 'активный',
            self::STATUS_BUTTON_DISABLED        => 'закрытый',
        ];
    }

    /**
     * Gets query for [[BrandTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrandTranslations()
    {
        return $this->hasMany(BrandTranslation::className(), ['id' => 'id']);
    }

    public function path()
    {
        return Yii::getAlias('@backend') . '/web/uploads/images/brand/';
    }

    public function getImage()
    {
        return ($this->image) ? '/backend/web/uploads/images/brand/' . $this->image : '';
    }

    public function pathFile()
    {
        return Yii::getAlias('@backend') . '/web/uploads/files/brand/';
    }

    public function getFile()
    {
        return ($this->file) ? '/backend/web/uploads/files/brand/' . $this->file : '';
    }

    public static function getMain()
    {
        return self::find()->all();
    }
}
