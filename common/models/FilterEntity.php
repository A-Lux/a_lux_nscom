<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "filter_entity".
 *
 * @property int $id
 * @property int $value_id
 * @property int $product_id
 *
 * @property FilterValue $value
 * @property Product $product
 */
class FilterEntity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'filter_entity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value_id', 'product_id'], 'required'],
            [['value_id', 'product_id'], 'integer'],
            [['value_id'], 'exist', 'skipOnError' => true, 'targetClass' => FilterValue::className(), 'targetAttribute' => ['value_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'value_id'      => 'Значение',
            'product_id'    => 'Товар',
        ];
    }

    /**
     * Gets query for [[Value]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getValue()
    {
        return $this->hasOne(FilterValue::className(), ['id' => 'value_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
