<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "requisites".
 *
 * @property int $id
 * @property string $source
 * @property string|null $value
 *
 * @property RequisitesTranslation[] $requisitesTranslations
 */
class Requisites extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requisites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['source'], 'required'],
            [['value'], 'string'],
            [['source'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'source'    => 'Ключ',
            'value'     => 'Значение',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => RequisitesTranslation::className(),
                'tableName'     => RequisitesTranslation::tableName(),
                'attributes'    => ['source', 'value'],
            ],
        ]);
    }

    /**
     * Gets query for [[RequisitesTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequisitesTranslations()
    {
        return $this->hasMany(RequisitesTranslation::className(), ['id' => 'id']);
    }
}
