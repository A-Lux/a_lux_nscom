<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "delivery".
 *
 * @property int $id
 * @property string $title
 * @property string|null $content
 *
 * @property DeliveryTranslation[] $deliveryTranslations
 */
class Delivery extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'title'     => 'Заголовок',
            'content'   => 'Содержание',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => DeliveryTranslation::className(),
                'tableName'     => DeliveryTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * Gets query for [[DeliveryTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryTranslations()
    {
        return $this->hasMany(DeliveryTranslation::className(), ['id' => 'id']);
    }
}
