<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "documents".
 *
 * @property int $id
 * @property string $key
 * @property string $name
 * @property string|null $file
 *
 * @property DocumentsTranslation[] $documentsTranslations
 */
class Documents extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'name'], 'required'],
            [['name'], 'string'],
            [['key', 'file'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'    => 'ID',
            'key'   => 'Ключ',
            'name'  => 'Название',
            'file'  => 'Файл',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => DocumentsTranslation::className(),
                'tableName'     => DocumentsTranslation::tableName(),
                'attributes'    => ['name'],
            ],
        ]);
    }

    /**
     * Gets query for [[DocumentsTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentsTranslations()
    {
        return $this->hasMany(DocumentsTranslation::className(), ['id' => 'id']);
    }

    public function pathFile()
    {
        return Yii::getAlias('@backend') . '/web/uploads/files/documents/';
    }

    public function getFile()
    {
        return ($this->file) ? '/backend/web/uploads/files/documents/' . $this->file : '';
    }

    public static function getAll()
    {
        $array = [];

        $documents  = self::find()->all();
        foreach ($documents as $document){
            $array[$document->key] = $document;
        }

        return $array;
    }
}
