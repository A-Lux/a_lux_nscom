<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_requisites".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $legal_name
 * @property string|null $bin
 * @property string|null $legal_address
 * @property string|null $name_bank
 * @property string|null $bik
 * @property string|null $iik
 * @property string|null $phone
 * @property string|null $email
 *
 * @property User $user
 */
class UserRequisites extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_requisites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['legal_name', 'bin', 'legal_address', 'name_bank', 'bik', 'iik', 'phone', 'email'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'user_id'       => 'Пользователь',
            'legal_name'    => 'Юридическое название компании',
            'bin'           => 'БИН/ИИН',
            'legal_address' => 'Юридический адрес',
            'name_bank'     => 'Наименование банка',
            'bik'           => 'БИК',
            'iik'           => 'ИИК',
            'phone'         => 'Номер телефона',
            'email'         => 'Электронная почта',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getOne()
    {
        return self::findOne(['user_id' => Yii::$app->user->identity->id]);
    }
}
