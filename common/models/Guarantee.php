<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "guarantee".
 *
 * @property int $id
 * @property string $title
 * @property string|null $content
 *
 * @property GuaranteeTranslation[] $guaranteeTranslations
 */
class Guarantee extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guarantee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'title'     => 'Заголовок',
            'content'   => 'Содержание',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => GuaranteeTranslation::className(),
                'tableName'     => GuaranteeTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * Gets query for [[GuaranteeTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGuaranteeTranslations()
    {
        return $this->hasMany(GuaranteeTranslation::className(), ['id' => 'id']);
    }
}
