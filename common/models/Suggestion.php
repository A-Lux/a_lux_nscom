<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "suggestion".
 *
 * @property int $id
 * @property string $title
 * @property string|null $content
 *
 * @property SuggestionTranslation[] $suggestionTranslations
 */
class Suggestion extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'suggestion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'title'     => 'Заголовок',
            'content'   => 'Содержание',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => SuggestionTranslation::className(),
                'tableName'     => SuggestionTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * Gets query for [[SuggestionTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSuggestionTranslations()
    {
        return $this->hasMany(SuggestionTranslation::className(), ['id' => 'id']);
    }
}
