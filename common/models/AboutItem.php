<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "about_item".
 *
 * @property int $id
 * @property int $status
 * @property string $name
 * @property string|null $image
 *
 * @property AboutItemTranslation[] $aboutItemTranslations
 */
class AboutItem extends MultilingualActiveRecord
{
    const STATUS_SPECIALIZATION             = 1;
    const STATUS_PARTNERS                   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name', 'status'], 'required'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'status'    => 'Статус',
            'name'      => 'Наименование',
            'image'     => 'Изображение',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => AboutItemTranslation::className(),
                'tableName'     => AboutItemTranslation::tableName(),
                'attributes'    => ['name'],
            ],
        ]);
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_SPECIALIZATION     => 'specialization',
            self::STATUS_PARTNERS           => 'partners',
        ];
    }

    /**
     * @return array
     */
    public static function statusDescription()
    {
        return [
            self::STATUS_SPECIALIZATION     => 'Категория специализации',
            self::STATUS_PARTNERS           => 'Предложение партнерам',
        ];
    }

    /**
     * Gets query for [[AboutItemTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAboutItemTranslations()
    {
        return $this->hasMany(AboutItemTranslation::className(), ['id' => 'id']);
    }

    public function path()
    {
        return Yii::getAlias('@backend') . '/web/uploads/images/about-item/';
    }

    public function getImage()
    {
        return ($this->image) ? '/backend/web/uploads/images/about-item/' . $this->image : '';
    }

    public static function getSpecialization()
    {
        $request    = AboutItem::findAll(['status' => self::STATUS_SPECIALIZATION]);
        return $request;
    }

    public static function getPartners()
    {
        $request    = AboutItem::findAll(['status' => self::STATUS_PARTNERS]);
        return $request;
    }
}
