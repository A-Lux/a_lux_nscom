<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "catalog_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $name
 * @property string|null $content
 * @property string|null $metaName
 * @property string|null $metaDesc
 * @property string|null $metaKey
 *
 * @property Catalog $id0
 */
class CatalogTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['content', 'metaDesc', 'metaKey'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['name', 'metaName'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Catalog::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'name' => 'Name',
            'content' => 'Content',
            'metaName' => 'Meta Name',
            'metaDesc' => 'Meta Desc',
            'metaKey' => 'Meta Key',
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'id']);
    }
}
