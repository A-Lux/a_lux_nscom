<?php

namespace common\models;

use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $status
 * @property string $title
 * @property string|null $description
 * @property string|null $content
 * @property string|null $image
 * @property string|null $metaName
 * @property string|null $metaDesc
 * @property string|null $metaKey
 * @property string|null $slug
 * @property string $date
 *
 * @property NewsTranslation[] $newsTranslations
 */
class News extends MultilingualActiveRecord
{
    const STATUS_DISABLED       = 0;
    const STATUS_ACTIVE         = 1;
    const STATUS_LATEST         = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['title'], 'required'],
            [['description', 'content', 'metaDesc', 'metaKey'], 'string'],
            [['date'], 'safe'],
            [['title', 'image', 'metaName', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'status'        => 'Статус',
            'title'         => 'Заголовок',
            'description'   => 'Краткое описание',
            'content'       => 'Содержание',
            'image'         => 'Изображение',
            'metaName'      => 'Мета название',
            'metaDesc'      => 'Мета описание',
            'metaKey'       => 'Мета ключи',
            'date'          => 'Дата',
            'slug'          => 'slug',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => NewsTranslation::className(),
                'tableName'     => NewsTranslation::tableName(),
                'attributes'    => ['title', 'description', 'content', 'metaName', 'metaDesc', 'metaKey'],
            ],
        ]);
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_DISABLED   => 'disabled',
            self::STATUS_ACTIVE     => 'active',
            self::STATUS_LATEST     => 'latest',
        ];
    }

    /**
     * @return array
     */
    public static function statusDescription()
    {
        return [
            self::STATUS_DISABLED   => 'закрытый',
            self::STATUS_ACTIVE     => 'активный',
            self::STATUS_LATEST     => 'свежие новости',
        ];
    }

    /**
     * Gets query for [[NewsTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNewsTranslations()
    {
        return $this->hasMany(NewsTranslation::className(), ['id' => 'id']);
    }

    public function path()
    {
        return Yii::getAlias('@backend') . '/web/uploads/images/news/';
    }

    public function getImage()
    {
        return ($this->image) ? '/backend/web/uploads/images/news/' . $this->image : '';
    }

    public static function getLatestModel($model)
    {
        $newsLatest = self::findAll(['status' => self::STATUS_LATEST]);
        $array      = [];

        foreach ($newsLatest as $news){
            if($model->id !== $news->id){
                $array[]    = $news;
            }
        }

        return $array;
    }
}
