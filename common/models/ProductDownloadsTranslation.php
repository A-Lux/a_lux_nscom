<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_downloads_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $name
 * @property string|null $file
 *
 * @property ProductDownloads $id0
 */
class ProductDownloadsTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_downloads_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['file'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['name'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductDownloads::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'name' => 'Name',
            'file' => 'File',
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProductDownloads::className(), ['id' => 'id']);
    }
}
