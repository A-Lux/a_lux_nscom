<?php

namespace common\models;

use phpDocumentor\Reflection\Types\Self_;
use Yii;
use omgdef\multilingual\MultilingualBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $image
 * @property string|null $link
 * @property int|null $status
 * @property int $sort
 *
 * @property BannerTranslation[] $bannerTranslations
 */
class Banner extends MultilingualActiveRecord
{
    const STATUS_DISABLED       = 0;
    const STATUS_ACTIVE         = 1;
    const STATUS_LINK           = 2;
    const STATUS_SEARCH         = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sort', 'status'], 'integer'],
            [['title', 'image', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'title'     => 'Текст',
            'image'     => 'Изображение',
            'link'      => 'Ссылка',
            'status'    => 'Статус',
            'sort'      => 'Сортировка',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => BannerTranslation::className(),
                'tableName'     => BannerTranslation::tableName(),
                'attributes'    => ['title'],
            ],
        ]);
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $model = Banner::find()->orderBy('sort DESC')->one();
            if(!$model || $this->id != $model->id){
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_DISABLED   => 'disabled',
            self::STATUS_ACTIVE     => 'active',
            self::STATUS_LINK       => 'link',
            self::STATUS_SEARCH     => 'search',
        ];
    }

    /**
     * @return array
     */
    public static function statusDescription()
    {
        return [
            self::STATUS_DISABLED   => 'закрытый',
            self::STATUS_ACTIVE     => 'активный',
            self::STATUS_LINK       => 'ссылка',
            self::STATUS_SEARCH     => 'поиск',
        ];
    }

    /**
     * Gets query for [[BannerTranslations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBannerTranslations()
    {
        return $this->hasMany(BannerTranslation::className(), ['id' => 'id']);
    }

    public function path()
    {
        return Yii::getAlias('@backend') . '/web/uploads/images/banner/';
    }

    public function getImage()
    {
        return ($this->image) ? '/backend/web/uploads/images/banner/' . $this->image : '';
    }

    public static function getAll()
    {
        return self::find()->orderBy('sort ASC')->all();
    }
}
