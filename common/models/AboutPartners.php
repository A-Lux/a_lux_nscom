<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "about_partners".
 *
 * @property int $id
 * @property int $status
 * @property string $name
 * @property string|null $link
 */
class AboutPartners extends \yii\db\ActiveRecord
{
    const STATUS_DISABLED       = 0;
    const STATUS_ACTIVE         = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_partners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name', 'status'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['link'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'status'    => 'Статус',
            'name'      => 'Название',
            'link'      => 'Ссылка',
        ];
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_DISABLED   => 'disabled',
            self::STATUS_ACTIVE     => 'active',
        ];
    }

    /**
     * @return array
     */
    public static function statusDescription()
    {
        return [
            self::STATUS_ACTIVE     => 'активный',
            self::STATUS_DISABLED   => 'закрытый',
        ];
    }

    public static function getAll()
    {
        $request    = AboutPartners::findAll(['status' => self::STATUS_ACTIVE]);

        return $request;
    }
}
