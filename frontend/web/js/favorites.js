function getUrl() {
    if (document.getElementsByTagName('html')[0].getAttribute('lang') != 'ru') {
        return `${window.location.origin}/${document.getElementsByTagName('html')[0].getAttribute('lang')}`;
    }
    else {
        return `${window.location.origin}`;
    }
}
function doLikeButton(button) {
      toggleButton(button);
}
  
  function toggleButton(button) {
      if (button.classList.contains('like-shaked')) {
        button.classList.remove('liked-shaked');
      }
      button.classList.toggle('liked');
      button.classList.toggle('not-liked');
      button.classList.toggle('fa-heart-o');
      button.classList.toggle('fa-heart');
      if(button.classList.contains("liked")) {
          button.classList.add('liked-shaked');
      }
  }
$('.btn-to-favorite').click(function () {
    var product_id  = $(this).attr('data-id');

    $.ajax({
        url: `${getUrl()}/favorites/add-favorite`,
        type: "GET",
        dataType: "json",
        data:{ id:product_id },
        context:this,
        success: function (data) {
            if(data.status === 1) {
                Swal.fire({
                    icon: "success",
                    text: data.message
                });
            doLikeButton($(this)[0]);
            }else if (data.status === 2){
                Swal.fire({
                    icon: "info",
                    text: data.message
                });
            doLikeButton($(this)[0]);
            }
        },
        error:function (data) {
            Swal.fire({
                icon: 'error',
                text: data.responseJSON.message
            });

        }
    });
});