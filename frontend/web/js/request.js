function getUrl() {
    if (document.getElementsByTagName('html')[0].getAttribute('lang') != 'ru') {
        return `${window.location.origin}/${document.getElementsByTagName('html')[0].getAttribute('lang')}`;
    }
    else {
        return `${window.location.origin}`;
    }
}
$('#suggestion-form').on('submit', function (e) {
    e.preventDefault();
    let form    = new FormData(document.forms.suggestionForm);

    axios.post(`${getUrl()}/suggestion/request`, form)
        .then(function (response) {
            if(response.data.status === 1) {
                Swal.fire({
                    icon: 'success',
                    text: response.data.message
                });
                window.location.href = "/suggestion";
            } else if(response.data.status === 0){
                Swal.fire({
                    icon: 'warning',
                    text: response.data.message
                });
            }
        })
        .catch(function (error) {
            // console.log(error)
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        });
});