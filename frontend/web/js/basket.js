function getUrl() {
    if (document.getElementsByTagName('html')[0].getAttribute('lang') != 'ru') {
        return `${window.location.origin}/${document.getElementsByTagName('html')[0].getAttribute('lang')}`;
    }
    else {
        return `${window.location.origin}`;
    }
}
$(document).ready(function () {
    let svgPreload = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin:auto;background:transparent;display:block;" width="25px" height="25px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="75" cy="50" fill="#ffffff" r="3.30057">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="-0.9166666666666666s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="-0.9166666666666666s"></animate></circle><circle cx="71.65063509461098" cy="62.5" fill="#ffffff" r="3.96723">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="-0.8333333333333334s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="-0.8333333333333334s"></animate></circle><circle cx="62.5" cy="71.65063509461096" fill="#ffffff" r="4.6339">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="-0.75s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="-0.75s"></animate></circle><circle cx="50" cy="75" fill="#ffffff" r="4.69943">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="-0.6666666666666666s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="-0.6666666666666666s"></animate></circle><circle cx="37.50000000000001" cy="71.65063509461098" fill="#ffffff" r="4.03277">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="-0.5833333333333334s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="-0.5833333333333334s"></animate></circle><circle cx="28.34936490538903" cy="62.5" fill="#ffffff" r="3.3661">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="-0.5s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="-0.5s"></animate></circle><circle cx="25" cy="50" fill="#ffffff" r="3">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="-0.4166666666666667s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="-0.4166666666666667s"></animate></circle><circle cx="28.34936490538903" cy="37.50000000000001" fill="#ffffff" r="3">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="-0.3333333333333333s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="-0.3333333333333333s"></animate></circle><circle cx="37.499999999999986" cy="28.349364905389038" fill="#ffffff" r="3">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="-0.25s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="-0.25s"></animate></circle><circle cx="49.99999999999999" cy="25" fill="#ffffff" r="3">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="-0.16666666666666666s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="-0.16666666666666666s"></animate></circle><circle cx="62.5" cy="28.349364905389034" fill="#ffffff" r="3">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="-0.08333333333333333s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="-0.08333333333333333s"></animate></circle><circle cx="71.65063509461096" cy="37.499999999999986" fill="#ffffff" r="3">  <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="1s" repeatCount="indefinite" begin="0s"></animate>  <animate attributeName="fill" values="#ffffff;#ffffff;#000000;#ffffff;#ffffff" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="1s" begin="0s"></animate></circle></svg>'
    $("body").on("click", ".btn-to-basket", function () {
        
        if(+this.getAttribute('data-session') === 1){
            this.innerHTML = svgPreload;
            window.location.href = '/basket';
        }
        else{
            var id = $(this).attr("data-id");
            this.innerHTML = svgPreload;
            $.ajax({
                url: `${getUrl()}/basket/add-product`,
                type: "GET",
                dataType: "json",
                data: { id: id },
                success: (data) => {
                    this.innerText = 'Перейти в корзину';
                    if (data.status) {
                        // console.log(data)
                        this.setAttribute('data-session',data.status);
                        $('body').overhang({
                            type: "success",
                            html: true,
                            message: data.message,
                            duration: 3,
                            closeConfirm: true,
                        });
                        $('.basket-quantity').html(data.count);
                    }
                },
                error: function (data) {
                    // console.log(data)
                    $('body').overhang({
                        type: "error",
                        html: true,
                        message: data.responseJSON.message,
                        // duration: 3,
                        closeConfirm: true
                    });
                }
    
            });
         }
    });
});

$('.down-product').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $.ajax({
        url: `${getUrl()}/basket/down-clicked`,
        type: "GET",
        dataType: "json",
        data: { id:id },
        success: function (data) {
            $('#countProduct-' + id).val(data.countProduct);
            $('.total-basket').html(data.total);
            document.getElementsByClassName('input-total-basket')[0].setAttribute('value', data.total);
        },
        error: function () {
            Swal.fire({
                icon: 'error',
                text: error.data.message
            });
        }
    });
});

$('.up-product').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $.ajax({
        url: `${getUrl()}/basket/up-clicked`,
        type: 'GET',
        dataType: "json",
        data:{ id:id },
        success: function (data) {
            $('#countProduct-' + id).val(data.countProduct);
            $('.total-basket').html(data.total);
            document.getElementsByClassName('input-total-basket')[0].setAttribute('value', data.total);
        },
        error: function(){
            Swal.fire({
                icon: 'error',
                text: error.data.message
            });
        }
    });
});

$(".quantity").on('change paste keyup', function () {
    var id = $(this).attr('data-id');
    var value = $(this).val();
    $.ajax({
        url: `${getUrl()}/basket/count-changed`,
        type:"GET",
        dataType: "json",
        data:{ id:id, value:value},
        success:function(data) {
            $('.total-basket').html(data.total);
            document.getElementsByClassName('input-total-basket')[0].setAttribute('value', data.total);
        },
        error:function(){
            Swal.fire({
                icon: 'error',
                text: 'Ошибка',
            });
        }
    })
});

$(".delete-product").click(function () {
    var id          = $(this).attr('data-id');
    var container   = $(this).parent().parent().parent().parent();
    var basketDiv   = document.getElementsByClassName('delete-product');
    $.ajax({
        url: `${getUrl()}/basket/delete-product`,
        type: "GET",
        dataType: "json",
        data:{ id:id },
        success: function (data) {
            container.remove();
            Swal.fire({
                icon:"success",
                text: data.message
            });
            $('.total-basket').html(data.total);
            document.getElementsByClassName('input-total-basket')[0].setAttribute('value', data.total);
            $('.basket-quantity').html(data.count);
            if(basketDiv.length  == 0 ){
                var div = document.createElement('div');
                div.innerText = 'К сожалению, на данный моммент Ваша корзина пуста.';
                document.getElementsByClassName('empty-basket')[0].appendChild(div);
                $('.not-empty').css('display','none');
                $('#basket-form').css('display','none');
            }
            
        },
        error:function (data) {
            Swal.fire({
                icon: 'error',
                text: data.message
            });

        }
    })

});

$('#basket-form').on('submit', function (e) {
    e.preventDefault();
    let form    = new FormData(document.forms.basketForm);

    axios.post(`${getUrl()}/order/form-basket`, form)
        .then(function (response) {
            if(response.data.user == 1){
                registration(form, response.data.id);
            }else {
                // console.log(response.data);
                window.location.href = `${getUrl()}/payment-method?id=` + response.data.id;
            }
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        })
});

function registration(form, id) {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger',
        },
        buttonsStyling: false
    });

    swalWithBootstrapButtons.fire({
        title: 'Хотите ли Вы зарегистрироваться?',
        text: "При регистрации, Вы получите доступ к личному кабинету и сможете получать гарантированные бонусы.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Да!',
        cancelButtonText: 'Нет!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            axios.get(`${getUrl()}/order/registration-user`, {
                    params: {
                        id: id
                    }
                })
                .then(function (response) {
                    // console.log(response.data);
                    if(response.data  == 1) {
                        swalWithBootstrapButtons.fire({
                            title: 'Отлично',
                            text: "Ваши данные придет к Вам на почту!",
                            icon: 'success',
                            confirmButtonText: 'Да!',
                        }).then(() => {
                            window.location.href = `${getUrl()}/payment-method?id=` + id;
                        })
                    }else{
                        Swal.fire({
                            icon: 'error',
                            text: 'Электронная почта уже зарегистрирована! Попробуйте зайти в личный кабинет!'
                        });
                    }
                })
                .catch(function (error) {
                    // console.log(error.response)
                    Swal.fire({
                        icon: 'error',
                        text: error.response.data.message
                    });
                });

        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire({
                title: 'Отмена',
                text: "Жаль",
                icon: 'error',
                confirmButtonText: 'Дальше',
            }).then(() => {
                window.location.href = `${getUrl()}/payment-method?id=` + id;
            })
        }
    })
}
