$(document).ready(function(){
    // $('.tab-content').removeClass('active-block');
    // $('.tabs-mobile').click(function(){
    //     $('.category-pills-mobile').addClass('active-none');
    //     $('.tab-content').addClass('active-block');
    // });
    // $('.back').click(function(){
    //     $('.category-pills-mobile').removeClass('active-none');
    //     $('.category-pills-mobile').addClass('active-block');
    //     $('.tab-pane').removeClass('active');
    //     $('.tab-content').addClass('active-none');
    // })
    $(function() {
      menu_top = $('.catalog-filters').offset().top;  
      $(window).scroll(function () {             
        if ($(window).scrollTop() > menu_top) {  
          if ($('.catalog-filters').css('position') != 'fixed') { 
            $('.catalog-filters').css('position','fixed');  
            $('.catalog-filters').css('top','0');   
            $('.catalog-filters').css('width', '255px');       
            
          }
        } else {                                 
          if ($('.catalog-filters').css('position') == 'fixed') {  
            $('.catalog-filters').css('position','');
            $('.catalog-filters').css('top','');
            // $('.content').css('margin-top','');
          }
        }
      });
    });
    var $main_nav = $('#main-nav');
    var $toggle = $('.toggle');

    var defaultData = {
      maxWidth: false,
      customToggle: $toggle,
      navTitle: 'Каталог товаров',
      levelTitles: true,
      insertClose: 2,
      closeLevels: false,
      position: 'bottom',
      insertClose: false,
      labelBack: 'Назад'

    };
    $main_nav.hcOffcanvasNav(defaultData);
    $('.burger-menu-btn').click(function(e){
		e.preventDefault();
		$(this).toggleClass('burger-menu-lines-active');
    $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
		$('body').toggleClass('body-overflow');
	});

$('.slideshow').slick({
   slidesToShow: 1,
  slidesToScroll: 1,
    dots:true,
  autoplay: true
 });

// $('#partnersss').change(function(){
//     let prVal = $(this).val();
//
//     if($(this).val() == 'partners'){
//       console.log('partnes')
//       $('.compain').css('display','none');
//     }
//     else{
//         $('.compain').css('display','block');
//     }
// });

    $('.status-client').change(function(){
        let prVal = $(this).val();

        if($(this).val() == '1'){
            console.log('partnes');
            $('.compain').css('display','none');
        }
        else{
            $('.compain').css('display','block');
        }
    });


 $('.product-sl').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slide-show'
});
$('.slide-show').slick({
   slidesToShow: 3,
  slidesToScroll: 1,
    dots:false,
    button:false,
  asNavFor: '.product-sl',
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 2
      }
    }
  ]
 });

$('.mob-product-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots:false,
    button:false,
    autoplay: true,
    autoplaySpeed: 2000,
 });

$(document).ready(function () {
  window.owlBrands= $('.owl-brands');
  window.owlBrands.owlCarousel({
      loop:true,
      autoplay:true,
      dots:false,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:5
          }
      }
  });
  $('.brands-next').click(function() {
      window.owlBrands.trigger('next.owl.carousel');
      console.log('Nurik')
  })
  $('.brands-prev').click(function() {
      window.owlBrands.trigger('prev.owl.carousel');
  })
})



var like_buttons = document.querySelectorAll(".like-button");
like_buttons.forEach(like => {
  if (+like.getAttribute('data-favorite') === 1) {
    if (like.classList.contains('not-liked')) {
      like.classList.remove('not-liked');
      like.classList.add('liked');
      like.classList.add('liked-shaked');
    }
  }
});
// if (like_button) {
//     like_button.addEventListener("click", doLikeButton);
// }

function doLikeButton(button) {
    toggleButton(button);
}

function toggleButton(button) {
    button.classList.remove('liked-shaked');
    button.classList.toggle('liked');
    button.classList.toggle('not-liked');
    button.classList.toggle('fa-heart-o');
    button.classList.toggle('fa-heart');

    if(button.classList.contains("liked")) {
        button.classList.add('liked-shaked');
    }
}

$('.mobile-menu-btn').on('click', function() {
	if($(this).hasClass('active')) {
		$('.mobile-menu-btn').removeClass('active');
	} else {
		$('.mobile-menu-btn').addClass('active');
	}
});

$('.title').on('click', function() {
	if($(this).hasClass('active')) {
		$('.title').removeClass('active');
	} else {
		$('.title').addClass('active');
	}
});

// $(document).ready(function(){
//     $( "#slider-range" ).slider({
//         range: true,
//         min: 0,
//         max: 100000,
//         values: [ 0, 10000 ],
//         slide: function( event, ui ) {
//           $( "#amount" ).val(  ui.values[ 0 ] );
//             $( "#amount-2" ).val(  ui.values[ 1 ]  );
//         }
//       });
//     function resizeInput(attr) {
//       attr.style.width = attr.value.length + 1 + "ch";
//     }
//     let uiDots = $('.ui-slider-handle');
    
//     uiDots.first().addClass('ainura-1').append('<p> <input type="text" id="amount" class="rangeVal-1" readonly style="border:0;"></p>');
//     uiDots.last().addClass('ainura-2').append('<p> <input type="text" id="amount-2"  class="rangeVal-2"readonly style="border:0;"></p>');
//       $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) ); 
//      $( "#amount-2" ).val( $( "#slider-range" ).slider( "values", 1 ) );
//     resizeInput(document.getElementsByClassName('rangeVal-1')[0]);
//     resizeInput(document.getElementsByClassName('rangeVal-2')[0]);
// })


     var clicks = 0;
    function minusPress() {
        clicks -= 1;
        document.getElementById("clicks").innerHTML = clicks;
    };   

  var clicks = 0;
    function plusPress() {
        clicks += 1;
        document.getElementById("clicks").innerHTML = clicks;
    };  

$(".e-list").slideUp(function() {
    $(".e-button").removeClass("open");
});

$(".e-button").on("click", function() {
    if ($(this).hasClass("open")) {
        $(".e-list").slideUp(function() {
            $(".e-button").removeClass("open");
        });
    } else {
        $(this).addClass("open");
        setTimeout(function() {
            $(".e-list").stop().slideDown();
        });
    }
});

$(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
document.querySelectorAll('.btn-to-basket').forEach(button => {
    if (+button.getAttribute('data-session') === 1) {
        button.innerText = 'Перейти в корзину'
    }
})

})

document.querySelectorAll('.nav-link.clicked-profile').forEach(link => {
    if (link.hash == window.location.hash) {
        link.click();
    }
});