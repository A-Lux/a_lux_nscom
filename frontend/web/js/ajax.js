function getUrl() {
    if (document.getElementsByTagName('html')[0].getAttribute('lang') != 'ru') {
        return `${window.location.origin}/${document.getElementsByTagName('html')[0].getAttribute('lang')}`;
    }
    else {
        return `${window.location.origin}`;
    }
}
$(document).ready(function () {
    let modelArr = [];
    let id = document.querySelector('.catalog-id').value;
    let appCatalog = document.querySelectorAll('.app-catalog');
    appCatalog.forEach(btn => btn.addEventListener('click', function () {
        axios.get(url, {
            params: {
                value: modelArr
            }
        })
        .then(response => {
                console.log(response.data)
                allProduct(response.data);
        })
    }));
    const url = `${getUrl()}/filter-api/filter?catalog=` + id;
    document.querySelectorAll('input[type="checkbox"]').forEach((checkbox, i) => checkbox.addEventListener('click', function () {
        if (checkbox.checked && typeof modelArr[i] === 'undefined') {
            modelArr.push(checkbox.nextElementSibling.nextElementSibling.getAttribute('data-id'));
        }
        else if (!checkbox.checked) {
            modelArr.pop(checkbox.nextElementSibling.nextElementSibling.getAttribute('data-id'))
        }
        else {
            return;
        }
        console.log(modelArr)
    }));

    let catalog = document.getElementById('catalog');
    function allProduct(itemCatalog) {
        catalog.innerHTML = '';
        itemCatalog.forEach(product => {
            let div = document.createElement('div');
            div.classList.add('col-xs-12', 'col-lg-3' ,'col-xl-4', 'col-md-6', 'all-product', 'col-sm-12');
            let productCard = document.createElement('div');
            let i = document.createElement('i');
            let a = document.createElement('a');
            let productsImg = document.createElement('div');
            let img = document.createElement('img');
            img.src = '/backend/web/uploads/images/product/' + product.product.image
            let p = document.createElement('p');
            p.innerText = product.product.name
            let divCost = document.createElement('div');
            let span = document.createElement('span');
            span.innerText = product.product.price ? 'KZT ' + product.product.price : 'Бесценный'
            let basket = document.createElement('button');
            basket.classList.add('basket-btn', 'btn-to-basket', 'w-100')
            basket.setAttribute('type', 'button');
            basket.setAttribute('data-id', product.product.id);
            basket.setAttribute('data-session', product.product.session);
            a.href = `/product/view?slug=${product.product.slug}`
            i.classList.add('fa', 'fa-2x', 'fa-heart-o', 'not-liked');
            i.setAttribute('data-id', product.product.id)
            i.setAttribute('data-favorite', product.favorite);
            div.classList.add('col-xs-12', 'col-lg-3', 'col-xl-3', 'col-md-6', 'all-product', 'col-sm-12');
            productCard.classList.add('product-card', 'products');
            productsImg.classList.add('products-img', 'catalog-img');
            divCost.classList.add('cost');
            divCost.style.textAlign = 'right';
            catalog.appendChild(div);
            div.appendChild(productCard);
            productCard.appendChild(i);
            productCard.appendChild(a);
            a.appendChild(productsImg);
            a.appendChild(p);
            productsImg.appendChild(img);
            a.appendChild(divCost);
            divCost.appendChild(span);
            productCard.appendChild(basket);
            basket.appendChild(document.createElement('img'));
            basket.innerText = 'В корзину';
            // basket.getElementsByTagName('a')[0].href = '';
            // basket.getElementsByTagName('a')[0].appendChild(document.createElement('img'));
            if (+i.getAttribute('data-favorite') === 1) {
                doLikeButton(like);
            }
        });
        basketAdd();
    }
});
var like_buttons = document.querySelectorAll(".like-button");
like_buttons.forEach(like => {
    if (like.getAttribute('data-favorite') == 1) {
        doLikeButton(like);
    }
});
function doLikeButton(button) {
    toggleButton(button);
}

function toggleButton(button) {
    button.classList.remove('liked-shaked');
    button.classList.toggle('liked');
    button.classList.toggle('not-liked');
    button.classList.toggle('fa-heart-o');
    button.classList.toggle('fa-heart');

    if(button.classList.contains("liked")) {
        button.classList.add('liked-shaked');
    }
}
function basketAdd() {
    $("body").on("click", ".btn-to-basket", function () {
        if(+this.getAttribute('data-session') === 1){
            this.innerHTML = svgPreload;
            window.location.href = '/basket';
        }
        else{
            var id = $(this).attr("data-id");
            this.innerHTML = svgPreload;
            $.ajax({
                url: `${getUrl()}/basket/add-product`,
                type: "GET",
                dataType: "json",
                data: { id: id },
                success: (data) => {
                    this.innerText = 'Перейти в корзину';
                    if (data.status) {
                        $('#countBasket').html(data.count);
                        this.setAttribute('data-session',data.status);
                        $('body').overhang({
                            type: "success",
                            html: true,
                            message: data.message,
                            duration: 3,
                            closeConfirm: true,
                        });
                    }
                },
                error: function (data) {
                    $('body').overhang({
                        type: "error",
                        html: true,
                        message: data.responseJSON.message,
                        duration: 3,
                        closeConfirm: true
                    });
                }

            });
        }
    });
}



