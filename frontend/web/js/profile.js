function getUrl() {
    if (document.getElementsByTagName('html')[0].getAttribute('lang') != 'ru') {
        return `${window.location.origin}/${document.getElementsByTagName('html')[0].getAttribute('lang')}`;
    }
    else {
        return `${window.location.origin}`;
    }
}
$('#secure-form').on('submit', function (e) {
    e.preventDefault();
    let form    = new FormData(document.forms.secureForm);

    axios.post(`${getUrl()}/profile/secure`, form)
        .then(function (response) {
            if(response.data.status === 1) {
                Swal.fire({
                    icon: 'success',
                    text: response.data.message
                });
                window.location.href = "/profile#password";
            } else if(response.data.status === 0){
                Swal.fire({
                    icon: 'warning',
                    text: response.data.message
                });
            }
        })
        .catch(function (error) {
            // console.log(error)
            Swal.fire({
                icon: 'error',
                text: error.response.data.message
            });
        });
});