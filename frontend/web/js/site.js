let lang = document.querySelectorAll('.language__btn');
let language__item = document.querySelectorAll('.language__item');
window.onload = function() {
    language__item.forEach((item) => {
        lang.forEach(language => {
            if (item.getAttribute('href') == language.getAttribute('href')) {
                language.setAttribute('data-src', item.getAttribute('data-src'));
                let img = language.children[0];
                img.src = language.getAttribute('data-src');
            }
        })

    });
};