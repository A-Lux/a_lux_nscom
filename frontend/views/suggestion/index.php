<?php

/* @var $this View */
/* @var $searchModel SuggestionSearch */
/* @var $menu \common\models\Menu */

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\models\search\SuggestionSearch;

?>

<section class="complaints">
    <div class="container">
        <div class="row d-flex justify-content-between">
            <div class="contact-title compl-title col-lg-12 d-flex align-items-center">
                <a href="/"> <span>Главная </span></a>
                <p style='padding: 0 10px;margin: 0'>></p>
                <p style='margin: 0'>Жалобы и предложения</p>
            </div>
            <div class="col-lg-12 title-contacts">
                <h1>Жалобы и предложения</h1>
            </div>
            <div class="complaints-content col-xl-6 col-12">
                <? foreach ($dataProvider->models as $model): ?>
                    <div class="complaintsDiv ">
                        <h3><?= $model->title; ?></h3>
                        <?= $model->content; ?>
                    </div>
                <? endforeach; ?>
            </div>
            <div class="complaints-forms col-xl-6 col-12">
                <?= $this->render('_blocks/_form_request')  ?>
            </div>
        </div>

    </div>
</section>
