<?php

    use yii\web\View;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use common\models\RequestSuggestion;


    /* @var $this View */
    /* @var $model  RequestSuggestion */
    /* @var $form ActiveForm */

    $model  = new RequestSuggestion();

?>

<div class="compl-forms d-flex flex-column">
    <form id="suggestion-form" name="suggestionForm">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />

        <h3>Отправить предложение или жалобу</h3>

        <p>Хотите оставить предложение или жалобу? Пожалуйста заполните информацию ниже и наши менеджеры
            рассмотрят Ваше обращение и свяжутся с вами.</p>

        <input type="text" placeholder="Имя" name="RequestForm[username]" required>

        <input type="text" placeholder="Телефон" name="RequestForm[phone]" required>

        <script>
            $('input[name="RequestForm[phone]"]').inputmask("+7(999) 999-9999");
        </script>

        <input type="text" placeholder='Email' name="RequestForm[email]" required>

        <textarea name="RequestForm[message]" required placeholder="Опишите предложение или жалобу"
                  id="" cols="20" rows="8">

                        </textarea>

        <button type="submit" class="col-xl-6 suggestion-request-btn">Отправить</button>

    </form>
</div>
