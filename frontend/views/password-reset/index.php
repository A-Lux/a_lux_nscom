<?php

use yii\web\View;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\ResetPasswordForm;


/* @var $this View */
/* @var $model  ResetPasswordForm */
/* @var $form ActiveForm */

?>

<div class="password-reset content-inner">
    <div class="container">
    <h1>Забыли пароль?</h1>
        <? $form    =   ActiveForm::begin([
            'id'        =>  'reset-password-form',
        ]); ?>
        <div class="reset-content content-page">
            <h3>Введите пожалуйста Ваш электронный адрес, и мы отправим Вам письмо с новым паролем</h3>

            <?= $form->field($model, 'email')->textInput([
                'placeholder'   => 'example@gmail.com',
                'required'      => 'required',
            ])->label(false) ?>

            <?= Html::submitButton('Отправить', ['class' => 'reset-btn']) ?>
        </div>

        <? ActiveForm::end(); ?>
    </div>
</div>