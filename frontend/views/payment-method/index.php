<?php

use yii\web\View;
use yii\helpers\Url;
use common\models\Orders;
use yii\helpers\Html;

/* @var $order Orders  */

?>

<div class="payment-page content-inner">
    <div class="container">
    <h1>Выберите удобный метод оплаты</h1>
        <div class="payment-content" data-order="<?= $order->id; ?>">
            <a href="<?= Url::to(['type', 'id' => $order->id, 'type' => 1]) ?>">Оплата наличными</a>
            <a href="<?= Url::to(['type', 'id' => $order->id, 'type' => 2]) ?>">Оплата на рассчетный счет</a>
            <a href="<?= Url::to(['type', 'id' => $order->id, 'type' => 3]) ?>">Оплата-онлайн</a>
        </div>
    </div>
</div>