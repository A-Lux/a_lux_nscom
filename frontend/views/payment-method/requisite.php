<?php

use yii\web\View;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\RequisiteForm;
use common\models\Orders;
use common\models\Client;
use common\models\UserRequisites;


/* @var $this View */
/* @var $model  RequisiteForm */
/* @var $form ActiveForm */
/* @var $order Orders */
/* @var $user Client */
/* @var $requisite UserRequisites */


?>
<div class="payment-page content-inner">
    <div class="container">
        <h1>Реквизиты</h1>
        <? $form    =   ActiveForm::begin([
            'id'        =>  'request-suggestion-form',
            'action'    => '/payment-method/requisite?id=' . $order->id,
        ]); ?>

            <div class="reset-content content-page">
                <h3>Для формирования счета нам необходимы Ваши реквизиты</h3>
                <div class="requisite">
                    <div class="requisite-field">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="requisite-title">
                                    <h2>Юридическое название компании</h2>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <?= $form->field($model, 'legal_name')->textInput([
                                    'placeholder'   => 'Юридическое название компании',
                                    'value'         => $requisite->legal_name,
                                    'required'      => 'required',
                                ])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="requisite-field">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="requisite-title">
                                    <h2> БИН/ИИН</h2>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <?= $form->field($model, 'bin')->textInput([
                                    'placeholder'   => 'БИН/ИИН',
                                    'value'         => $requisite->bin,
                                    'required'      => 'required',
                                ])->label(false) ?>
                            </div>
                            <script>
                                $('input[name="RequisiteForm[bin]"]').keyup(function(e) {
                                    var regex = /^[0-9]+$/;
                                    if (regex.test(this.value) !== true)
                                        this.value = this.value.replace(/[^0-9]+/, '');
                                });
                            </script>
                        </div>
                    </div>
                    <div class="requisite-field">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="requisite-title">
                                    <h2> Юридический адрес</h2>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <?= $form->field($model, 'legal_address')->textInput([
                                    'placeholder'   => 'Юридический адрес',
                                    'value'         => $requisite->legal_address,
                                    'required'      => 'required',
                                ])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="requisite-field">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="requisite-title">
                                    <h2> Наименование банка</h2>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <?= $form->field($model, 'name_bank')->textInput([
                                    'placeholder'   => 'Наименование банка',
                                    'value'         => $requisite->name_bank,
                                    'required'      => 'required',
                                ])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="requisite-field">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="requisite-title">
                                    <h2> БИК</h2>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <?= $form->field($model, 'bik')->textInput([
                                    'placeholder'   => 'БИК',
                                    'value'         => $requisite->bik,
                                    'required'      => 'required',
                                ])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="requisite-field">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="requisite-title">
                                    <h2> ИИК</h2>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <?= $form->field($model, 'iik')->textInput([
                                    'placeholder'   => 'ИИК',
                                    'value'         => $requisite->iik,
                                    'required'      => 'required',
                                ])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="requisite-field">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="requisite-title">
                                    <h2> Номер телефона</h2>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <?= $form->field($model, 'phone')->textInput([
                                    'placeholder'   => 'Номер телефона',
                                    'value'         => $requisite->phone ? $requisite->phone : $user->mobile_phone,
                                    'required'      => 'required',
                                ])->label(false) ?>
                            </div>
                            <script>
                                $('input[name="RequisiteForm[phone]"]').inputmask("+7(999) 999-9999");
                            </script>
                        </div>
                    </div>
                    <div class="requisite-field">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="requisite-title">
                                    <h2> Электронная почта</h2>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <?= $form->field($model, 'email')->textInput([
                                    'placeholder'   => 'Электронная почта',
                                    'value'         => $requisite->email ? $requisite->email : Yii::$app->user->identity->email,
                                    'required'      => 'required',
                                ])->label(false) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?= Html::submitButton('Отправить', ['class' => '']) ?>
            </div>

        <? ActiveForm::end(); ?>

    </div>
</div>
</div>