<?php

use yii\web\View;
use yii\helpers\Url;
use common\models\Orders;
use common\models\UserRequisites;
use yii\helpers\Html;

/* @var $order Orders  */
/* @var $order UserRequisites  */

?>
<body style="margin: 0">
<div class="invoice">
<div class="container" style="width: 100%; margin: 0 auto;">
        <p style="font-size: 17px;width:80%;margin:0 auto"> Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате
        обязательно, в противном случае не гарантируется наличие товара на складе. 
        Товар отпускается по факту  прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и документов 
        удостоверяющих личность.</p>

<div class="invoice-title" style="font-size: 16px !important; text-align:center;">
    <h1 style="font-size: 27px">Образец платежного поручения</h1>
</div>
    <table border="1" cellpadding="5" style="width: 80%;margin:0 auto; border-spacing:0">
        <tr>
            <td style="border: 0"><b>Бенефициар:</b></th>
            <th style="border: 0; border-left:1px solid">ИИК</th>
            <th style="border: 0; border-left:1px solid">Кбе</th>
        </tr>
        <tr>
            <td style="border: 0;border-top:1px solid"> <b>TOO “National Security & Communication”</b> </td>
            <th style="border: 0; border-left:1px solid;border-top:1px solid" rowspan="2">KZ039470398991290510</th>
            <th style="border: 0; border-left:1px solid;border-top:1px solid" rowspan="2">17</th>
        </tr>
        <tr>
            <td style="border:0;border-top:1px solid">БИН: 160340027955</td>
        </tr>
        <tr>
            <td style="border:0;border-top:1px solid"> <b> Банк бенефициара:</b></td>
            <th style="border: 0; border-left:1px solid;border-top:1px solid">БИК</th>
            <th style="border: 0; border-left:1px solid;border-top:1px solid">Код назначения платежа</th>
        </tr>
        <tr>
            <td style="border:0;border-top:1px solid">АО БД «Альфа-Банк», г.Алматы</td>
            <th style="border: 0; border-left:1px solid;border-top:1px solid">ALFAKZKA</th>
            <th style="border: 0; border-left:1px solid;border-top:1px solid" colspan="2"></th>
        </tr>
    </table>
 <div style="text-align: center">
     <h1 style="font-size: 27px"><b>Счет на оплату № 000000000001 от 11 марта 2020 г.</b></h1>
 </div>
 <div style="margin-bottom: 20px">
    <table border="1" cellpadding="5" style="width: 80%;margin:0 auto; border-spacing:0">
        <tr>
            <th style="border: 0">Поставщик:</th>
            <th style="border: 0; border-left:1px solid;" colspan="6">TOO “National Security & Communication”, БИН 160340027955, 050000 Республика Казахстан, г.Алматы, пр. Райымбека 162</th>
        </tr>
        <tr>
            <td style="border:0;border-top:1px solid">Покупатель:</td>
            <th style="border: 0; border-left:1px solid;border-top:1px solid" colspan="6"> </th>
        </tr>
        <tr>
            <td style="border:0;border-top:1px solid">Основание</td>
            <th style="border: 0; border-left:1px solid;border-top:1px solid" colspan="6"> </th>
        </tr>
    </table>
 </div>
 <div style="margin-bottom: 20px">
 <table border="1" width="60%" cellpadding="5" style="width: 80%;margin:0 auto; border-spacing:0">
        <tr>
            <th style="border: 0">№</th>
            <th style="border: 0; border-left:1px solid;" colspan="10">Наименование</th>
            <th style="border: 0; border-left:1px solid;">Кол-во</th>
            <th style="border: 0; border-left:1px solid;">Ед.</th>
            <th style="border: 0; border-left:1px solid;">Цена</th>
            <th style="border: 0; border-left:1px solid;">Сумма</th>
        </tr>
         <? foreach ($order->orderedProducts as $item): ?>
            <tr>
                <td style="border:0;border-top:1px solid">1</td>
                <td style="border: 0; border-left:1px solid;border-top:1px solid" colspan="10"><?= $item->product->name; ?> </td>
                <td style="border: 0; border-left:1px solid;border-top:1px solid;text-align:right;"><?= $item->count; ?></td>
                <td style="border: 0; border-left:1px solid;border-top:1px solid;text-align:right">шт</td>
                <td style="border: 0; border-left:1px solid;border-top:1px solid;text-align:right;"><?= $item->product->price; ?></td>
                <td style="border: 0; border-left:1px solid;border-top:1px solid;text-align:right;"><?= $item->getSumCount($item->product->price, $item->count) ?></td>
            </tr>
        <? endforeach; ?>
    </table>
 </div>
 <div style="margin-bottom: 20px">
 <table border="1" width="60%" cellpadding="5" style="width: 80%;margin:0 auto; border-spacing:0">
        <tr>
            <td style="border: 0" colspan="8">Всего наименований 1, на сумму <?= $order->sum; ?> KZT </td>
        </tr>
        <tr>
            <td style="border: 0; border-top:1px solid" colspan="8"><b>Всего к оплате: Двести пятьдесят тысяч тенге. 00 тиын</b></td>
        </tr>
    </table>
 </div>
 <div>
 <table border="0" width="60%" cellpadding="5" style="width: 80%;margin:0 auto; border-spacing:0">
        <tr>
            <th style="border: 0;width:20%">Исполнитель</th>
            <td style="border: 0;border-bottom:1px solid;width:30%" colspan="6"> </td>
            <td colspan="3"> /Бухгалтер/</td>
        </tr>
    </table>
 </div>
 </div>
</div>
</div>
</body>