<?php
    use yii\data\ActiveDataProvider;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\web\View;
    use frontend\models\search\BrandSearch;
?>

<section class="categories-filter">
    <div class="container category-content">
        <div class="col-lg-12 title-catalog d-flex align-items-center p-0">
            <h1>Каталог товаров</h1>
        </div>
        <div class="row d-flex justify-content-center">
            <div class=" col-lg-3  col-xl-3  d-flex flex-column ">
                <div class="catalog-filters">
                    <div class="model col-lg-12 p-0">
                        <button class="btn model-btn title" type="button" data-toggle="collapse"
                                data-target="#collapseExamples" aria-expanded="false" aria-controls="collapseExamples">
                            Модель

                        </button>
                        <div class="collapse filter-collapse" id="collapseExamples">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="element d-flex col-lg-12 ">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="element d-flex  col-lg-12 ">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="element d-flex col-lg-12 ">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="element d-flex col-lg-12 ">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <div class="all " style="text-align: left">
                                    <button class="btn all-btn" type="button" data-toggle="collapse"
                                            data-target="#collapseExamples-1-1" aria-expanded="false"
                                            aria-controls="collapseExamples-1-1">
                                        Показать все

                                    </button>
                                    <div class="collapse" id="collapseExamples-1-1">
                                        <div class="filters-btn d-flex flex-column ">
                                            <div class="element d-flex col-lg-12 ">
                                                <input type="radio" id="radio-all-1-1" name="radios-one" value="all"
                                                       checked>
                                                <label for="radio-all-1-1"></label>
                                                <span>AMD A Series</span>
                                            </div>
                                            <div class="element d-flex  col-lg-12 ">
                                                <input type="radio" id="radio-all-1-2" name="radios-one" value="all"
                                                       checked>
                                                <label for="radio-all-1-2"></label>
                                                <span>Intel Celeron</span>
                                            </div>
                                            <div class="element d-flex col-lg-12 ">
                                                <input type="radio" id="radio-all-1-3" name="radios-one" value="all"
                                                       checked>
                                                <label for="radio-all-1-3"></label>
                                                <span>AMD Ryzen 5</span>
                                            </div>
                                            <div class="element d-flex col-lg-12 ">
                                                <input type="radio" id="radio-all-1-4" name="radios-one" value="all"
                                                       checked>
                                                <label for="radio-all-1-4"></label>
                                                <span>Intel Core i7</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="application col-lg-9">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse" data-target="#collapseExamples-3"
                                aria-expanded="false" aria-controls="collapseExamples-3">
                            Процессор
                        </button>
                        <div class="collapse filter-collapse" id="collapseExamples-3">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="element d-flex col-lg-12 ">
                                    <input type="radio" id="radio3-1" name="radios-one" value="all" checked>
                                    <label for="radio3-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="element d-flex  col-lg-12 ">
                                    <input type="radio" id="radio3-2" name="radios-one" value="all" checked>
                                    <label for="radio3-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="element d-flex col-lg-12 ">
                                    <input type="radio" id="radio3-3" name="radios-one" value="all" checked>
                                    <label for="radio3-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="element d-flex col-lg-12 ">
                                    <input type="radio" id="radio3-4" name="radios-one" value="all" checked>
                                    <label for="radio3-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <div class="all-2 " style="text-align: left">
                                    <button class="btn" type="button" data-toggle="collapse"
                                            data-target="#collapseExamples-1-2" aria-expanded="false"
                                            aria-controls="collapseExamples-1-2">
                                        Показать все

                                    </button>
                                    <div class="collapse" id="collapseExamples-1-2">
                                        <div class="filters-btn d-flex flex-column ">
                                            <div class="element d-flex col-lg-12 ">
                                                <input type="radio" id="radio-all-1" name="radios-one" value="all"
                                                       checked>
                                                <label for="radio-all-1"></label>
                                                <span>AMD A Series</span>
                                            </div>
                                            <div class="element d-flex  col-lg-12 ">
                                                <input type="radio" id="radio-all-2" name="radios-one" value="all"
                                                       checked>
                                                <label for="radio-all-2"></label>
                                                <span>Intel Celeron</span>
                                            </div>
                                            <div class="element d-flex col-lg-12 ">
                                                <input type="radio" id="radio-all-3" name="radios-one" value="all"
                                                       checked>
                                                <label for="radio-all-3"></label>
                                                <span>AMD Ryzen 5</span>
                                            </div>
                                            <div class="element d-flex col-lg-12 ">
                                                <input type="radio" id="radio-all-4" name="radios-one" value="all"
                                                       checked>
                                                <label for="radio-all-4"></label>
                                                <span>Intel Core i7</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="application col-lg-9">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse" data-target="#collapseExamples-4"
                                aria-expanded="false" aria-controls="collapseExamples-4" style="text-align: left">
                            Операционная система
                        </button>

                        <div class="collapse filter-collapse" id="collapseExamples-4">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 ">
                                    <input type="radio" id="radio4-1" name="radios-one" value="all" checked>
                                    <label for="radio4-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 ">
                                    <input type="radio" id="radio4-2" name="radios-one" value="all" checked>
                                    <label for="radio4-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 ">
                                    <input type="radio" id="radio4-3" name="radios-one" value="all" checked>
                                    <label for="radio4-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 ">
                                    <input type="radio" id="radio4-4" name="radios-one" value="all" checked>
                                    <label for="radio4-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse" data-target="#collapseExamples-5"
                                aria-expanded="false" aria-controls="collapseExamples-5">
                            Производитель
                        </button>
                        <div class="collapse" id="collapseExamples-5">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 ">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 ">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 ">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 ">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse" data-target="#collapseExamples-6"
                                aria-expanded="false" aria-controls="collapseExamples-6">
                            Вес
                        </button>
                        <div class="collapse" id="collapseExamples-6">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse" data-target="#collapseExamples-7"
                                aria-expanded="false" aria-controls="collapseExamples-7" style="text-align: left">
                            Оперативная память
                        </button>
                        <div class="collapse" id="collapseExamples-7">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse" data-target="#collapseExamples-8"
                                aria-expanded="false" aria-controls="collapseExamples-8">
                            Тип видеокарты
                        </button>
                        <div class="collapse" id="collapseExamples-8">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse" data-target="#collapseExamples-9"
                                aria-expanded="false" aria-controls="collapseExamples-9" style="text-align: left">
                            Диагональ экрана, дюйм
                        </button>
                        <div class="collapse" id="collapseExamples-9">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse"
                                data-target="#collapseExamples-10" aria-expanded="false"
                                aria-controls="collapseExamples-10">
                            Тип жесткого диска
                        </button>
                        <div class="collapse" id="collapseExamples-10">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0 d-flex align-items-start">
                        <button class="btn title" type="button" data-toggle="collapse"
                                data-target="#collapseExamples-11" aria-expanded="false"
                                aria-controls="collapseExamples-11" style="text-align: left">
                            Объем видеопамяти
                        </button>
                        <div class="collapse" id="collapseExamples-11">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse"
                                data-target="#collapseExamples-12" aria-expanded="false"
                                aria-controls="collapseExamples-12">
                            Оптический привод
                        </button>
                        <div class="collapse" id="collapseExamples-12">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse"
                                data-target="#collapseExamples-13" aria-expanded="false"
                                aria-controls="collapseExamples-13">
                            Объем HDD диска
                        </button>
                        <div class="collapse" id="collapseExamples-13">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse"
                                data-target="#collapseExamples-14" aria-expanded="false"
                                aria-controls="collapseExamples-14">
                            Объем SSD диска
                        </button>
                        <div class="collapse" id="collapseExamples-14">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse"
                                data-target="#collapseExamples-15" aria-expanded="false"
                                aria-controls="collapseExamples-15">
                            Покрытие экрана
                        </button>
                        <div class="collapse" id="collapseExamples-15">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0">
                        <button class="btn title" type="button" data-toggle="collapse"
                                data-target="#collapseExamples-16" aria-expanded="false"
                                aria-controls="collapseExamples-16">
                            Разрешение экрана
                        </button>
                        <div class="collapse" id="collapseExamples-16">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>
                    <div class="category col-lg-12 p-0 d-flex justify-content-start">
                        <button class="btn title" type="button" data-toggle="collapse"
                                data-target="#collapseExamples-17" aria-expanded="false"
                                aria-controls="collapseExamples-17">
                            Опции
                        </button>
                        <div class="collapse" id="collapseExamples-17">
                            <div class="filters-btn d-flex flex-column ">
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-1" name="radios-one" value="all" checked>
                                    <label for="radio2-1"></label>
                                    <span>AMD A Series</span>
                                </div>
                                <div class="d-flex  col-lg-12 p-0">
                                    <input type="radio" id="radio2-2" name="radios-one" value="all" checked>
                                    <label for="radio2-2"></label>
                                    <span>Intel Celeron</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-3" name="radios-one" value="all" checked>
                                    <label for="radio2-3"></label>
                                    <span>AMD Ryzen 5</span>
                                </div>
                                <div class="d-flex col-lg-12 p-0">
                                    <input type="radio" id="radio2-4" name="radios-one" value="all" checked>
                                    <label for="radio2-4"></label>
                                    <span>Intel Core i7</span>
                                </div>
                                <button class="application">Применить</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="catalog-products col-lg-9 col-xl-9 d-flex">
                <div class="row">
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12 ">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12 ">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12 ">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12 ">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12 ">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12 ">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-xl-4 col-md-6  all-product col-sm-12">
                        <div class="product-card products">
                            <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                            <a href="product.html">
                                <div class="products-img catalog-img">
                                    <img src="/images/image/router.png" alt="">
                                </div>
                                <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                                <div class="cost" style="text-align: right">
                                    <span>$199</span>
                                </div>
                            </a>
                            <div class="basket d-flex align-items-center justify-content-center">
                                <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В
                                    КОРЗИНУ</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-12 col-md-12 d-flex justify-content-center">
                        <div class="pages">
                            <input type="radio" id="radio-page" name="radios-one" value="all" checked>
                            <label for="radio-page">1</label>
                        </div>
                        <div class="pages">
                            <input type="radio" id="radio-page-2" name="radios-one" value="all" checked>
                            <label for="radio-page-2">2</label>
                        </div>
                        <div class="pages">
                            <input type="radio" id="radio-page" name="radios-one" value="all" checked>
                            <label for="radio-page-2">...</label>
                        </div>
                        <div class="pages">
                            <input type="radio" id="radio-page-4" name="radios-one" value="all" checked>
                            <label for="radio-page-4">10</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
