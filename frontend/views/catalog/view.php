<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\FilterValue;
use common\models\FilterAttr;
use common\models\Product;
use common\models\Catalog;

/* @var $this yii\web\View */
/* @var $model              Catalog */
/* @var $product            Product */
/* @var $filterValue        FilterValue */
/* @var $filterAttr         FilterAttr */
/* @var $session */
/* @var $favorite */


?>

<input type="hidden" class="catalog-id" value="<?= $model->id; ?>">
<section class="categories-filter">
    <div class="container category-content">
        <div class="col-lg-12 title-catalog d-flex align-items-center p-0">
            <h1>Каталог товаров</h1>
        </div>
        <? if (empty($product)) : ?>
            <?= Yii::t('main', 'Временно в этой категории нет продуктов!'); ?>
        <? else : ?>
            <div class="row d-flex justify-content-center">
                <div class=" col-lg-3  col-xl-3 col-md-4 d-flex flex-column ">
                    <div class="catalog-filters">
                        <? foreach ($filterAttr as $attribute) : ?>
                            <div class="model col-lg-12 p-0">
                                <button class="btn model-btn title" type="button" data-toggle="collapse"
                                        data-target="#collapseExamples-<?= $attribute->id; ?>" aria-expanded="false"
                                        aria-controls="collapseExamples-<?= $attribute->id; ?>">
                                    <?= $attribute->name; ?>
                                </button>
                                <div class="collapse filter-collapse" id="collapseExamples-<?= $attribute->id; ?>">
                                    <div class="filters-btn d-flex flex-column ">
                                        <? foreach ($filterValue as $value) : ?>
                                            <? if ($value->attribute0->id == $attribute->id) : ?>
                                                <div class="element d-flex col-lg-12">
                                                    <input type="checkbox" id="radio2-<?= $value->id; ?>"
                                                           name="radios-one" value="all">
                                                    <label for="radio2-<?= $value->id; ?>"></label>
                                                    <span data-id="<?= $value->id; ?>"><?= $value->name; ?></span>
                                                </div>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                        <button class="application col-lg-9 app-catalog">Применить</button>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                    <div class="mobile-catalog-filters">
                        <button type="button" class="btn parameters-btn" data-toggle="modal"
                                data-target="#exampleModalCenter">
                            Подобрать по параметрам
                        </button>
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Подобрать по
                                            параметрам</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                    <div class="container category-content">
                                        <div class="row d-flex justify-content-center">
                                            <div class=" col-lg-12  col-xl-12  d-flex flex-column ">
                                                <div class="mobile-catalog-filters">
                                                    <? foreach ($filterAttr as $attribute) : ?>
                                                        <div class="model col-12 p-0">
                                                            <button class="btn model-btn title" type="button"
                                                                    data-toggle="collapse"
                                                                    data-target="#collapseExamples-<?= $attribute->id; ?>"
                                                                    aria-expanded="false"
                                                                    aria-controls="collapseExamples-<?= $attribute->id; ?>">
                                                                <?= $attribute->name; ?>
                                                            </button>
                                                                <div class="collapse filter-collapse" id="collapseExamples-<?= $attribute->id; ?>">
                                                                    <div class="filters-btn d-flex flex-column ">
                                                                        <? foreach ($filterValue as $value) : ?>
                                                                            <? if ($value->attribute0->id == $attribute->id) : ?>
                                                                                <div class="element d-flex col-lg-12 ">
                                                                                <input type="checkbox" id="radio2-mobile-<?= $value->id; ?>"
                                                                                        name="radios-one" value="all">
                                                                                    <label for="radio2-mobile-<?= $value->id; ?>"></label>
                                                                                    <span data-id="<?= $value->id; ?>"><?= $value->name; ?></span>
                                                                                </div>
                                                                            <? endif; ?>
                                                                        <? endforeach; ?>
                                                                        <button class="application col-lg-9 app-catalog">Применить
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <? endforeach; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!-- <div class="modal-footer">
                                            <button type="button" class="btn " data-dismiss="modal">Закрыть</button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="catalog-products col-lg-9 col-md-8 col-xl-9 d-flex">
                    <div class="row w-100" id="catalog">
                        <? foreach ($product as $item) : ?>
                            <div class="col-xs-12 col-lg-3 col-xl-4 col-md-6  all-product col-sm-12 mt-5">
                                <div class="product-card products">
                                    <i class="fa fa-2x fa-heart-o not-liked like-button btn-to-favorite"
                                       data-id="<?= $item->id; ?>"
                                       data-favorite="<?= in_array($item->id, $favorite) ?>">

                                    </i>
                                    <a href="<?= Url::to(['/product/view', 'slug' => $item->slug]); ?>">
                                        <div class="products-img catalog-img">
                                            <img src="<?= $item->getImage(); ?>" alt="">
                                        </div>
                                        <div class="products-info">
                                            <p><?= $item->name; ?></p>
                                            <? if($item->status  == Product::STATUS_NOT_AVAILABLE): ?>
                                                <p class='not-available'>Нет в наличии</p>
                                            <? endif; ?>
                                            <div class="cost" style="text-align: right">
                                                <span>KZT <?= $item->price; ?></span>
                                            </div>
                                        </div>
                                    </a>
                                    <a class=" btn-to-basket w-100" type="button" data-id="<?= $item->id; ?>"
                                       data-session=<?= in_array($item->id, $session) ? 1 : 0 ?>>
                                        <button class="basket-btn w-100">
                                            <img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png"
                                                 alt="">
                                            В КОРЗИНУ
                                        </button>
                                    </a>

                                </div>
                            </div>
                        <? endforeach; ?>

                        <!--                        <div class="col-xs-12 col-lg-12 col-md-12 d-flex justify-content-center">-->
                        <!--                            <div class="pages">-->
                        <!--                                <input type="radio" id="radio-page" name="radios-one" value="all" checked>-->
                        <!--                                <label for="radio-page">1</label>-->
                        <!--                            </div>-->
                        <!--                            <div class="pages">-->
                        <!--                                <input type="radio" id="radio-page-2" name="radios-one" value="all" checked>-->
                        <!--                                <label for="radio-page-2">2</label>-->
                        <!--                            </div>-->
                        <!--                            <div class="pages">-->
                        <!--                                <input type="radio" id="radio-page" name="radios-one" value="all" checked>-->
                        <!--                                <label for="radio-page-2">...</label>-->
                        <!--                            </div>-->
                        <!--                            <div class="pages">-->
                        <!--                                <input type="radio" id="radio-page-4" name="radios-one" value="all" checked>-->
                        <!--                                <label for="radio-page-4">10</label>-->
                        <!--                            </div>-->
                        <!--                        </div>-->
                    </div>
                </div>
            </div>
        <? endif; ?>
    </div>
</section>