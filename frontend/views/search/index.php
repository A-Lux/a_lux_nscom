<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Product;
use yii\web\View;

/* @var $text */
/* @var $favorite */
/* @var $session */
/* @var $products Product */

?>
<div class="search-page">
    <div class="container">
        <div class="search-content">
            <div class="search-title">
                <h1>Результаты по запросу:</h1>
                <h2><?= $text; ?></h2>
            </div>
            <div class="search-info">
                <div class="row">
                    <? foreach ($products as $product): ?>
                        <div class="col-xs-12 col-lg-4 col-xl-3 col-md-6  all-product col-sm-12">
                            <div class="product-card products">
                                <i class="fa fa-2x fa-heart-o not-liked like-button btn-to-favorite"
                                   data-id="<?= $product->id;?>" data-favorite="<?= in_array($product->id, $favorite) ?>">
                                </i>
                                <a href="<?= Url::to(['/product/view', 'slug' => $product->slug]); ?>">
                                    <div class="products-img catalog-img">
                                        <img src="<?= $product->getImage(); ?>" alt="">
                                    </div>
                                    <p><?= $product->name; ?></p>
                                    <div class="cost" style="text-align: right">
                                        <span>KZT <?= $product->price; ?></span>
                                    </div>
                                </a>
                                <a class="btn-to-basket w-100" type="button" data-id="<?= $product->id; ?>" data-session=<?= in_array($product->id, $session) ? 1 : 0 ?>>
                                    <button class="basket-btn w-100">
                                        <img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt="">
                                        В КОРЗИНУ
                                    </button>
                                </a>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>