<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\ToastrAlert;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- <meta charset="<?= Yii::$app->charset ?>"> -->
    <meta charset="utf8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="frontend/web/images/image/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="frontend/web/images/image/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="frontend/web/images/image/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="frontend/web/images/image/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="frontend/web/images/image/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="frontend/web/images/image/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="frontend/web/images/image/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="frontend/web/images/image/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="frontend/web/images/image/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="frontend/web/images/image/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="frontend/web/images/image/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="frontend/web/images/image/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="frontend/web/images/image/fav/favicon-16x16.png">
    <link rel="manifest" href="frontend/web/images/image/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="frontend/web/images/image/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
  
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/hc-offcanvas-nav@3.4.1/dist/hc-offcanvas-nav.css">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
    <script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/overhang@1.0.5/dist/overhang.min.css" />

    
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js" integrity="sha256-bd8XIKzrtyJ1O5Sh3Xp3GiuMIzWC42ZekvrMMD4GxRg=" crossorigin="anonymous"></script>
    
</head>
<body>
<?php $this->beginBody() ?>

<!-- HEADER -->
<?= $this->render('_header') ?>
<!-- END HEADER -->

<?= \yii2mod\alert\Alert::widget() ?>
<!-- CONTENT -->
    <?= $content ?>
<!-- END CONTENT -->

<!-- FOOTER -->
<?= $this->render('_footer') ?>
<!-- END FOOTER -->
 <script>
$('.navbar-sticky').affix({offset:{top:function(){return this.top=$('.navbar-sticky').offset().top }}});

     </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/overhang@1.0.5/dist/overhang.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/hc-offcanvas-nav@3.4.1/dist/hc-offcanvas-nav.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
