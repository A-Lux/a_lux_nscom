<?php

use yii\helpers\ArrayHelper;

$documents = Yii::$app->view->params['documents'];

?>

<section class="footer">
    <div class="container foot d-flex align-items-center">
        <div class="row d-flex justify-content-between foot-row">
            <div class="col-xs-1 col-lg-1 col-md-4 footer-card card-logo">
                <a href="/"><img src="<?= Yii::$app->view->params['logoFooter']->getImage(); ?>" alt=""></a>
            </div>
            <div class="col-xs-4 col-lg-4 footer-card col-md-4 d-flex flex-column">
                <a href="<?= ArrayHelper::getValue($documents, 'Договор оферты')->getFile(); ?>" class="footer-link" target="_blank">
                    <?= ArrayHelper::getValue($documents, 'Договор оферты')->name; ?>
                </a>
                <? foreach (Yii::$app->view->params['menuFooter'] as $menu): ?>
                    <a href="<?= $menu->url ?>" class="footer-link"><?= $menu->name; ?></a>
                <? endforeach; ?>
            </div>
            <div class="col-xs-4 col-lg-4 col-md-4 d-flex flex-column footer-card ">
                <a href="<?= ArrayHelper::getValue($documents, 'Политика приватности')->getFile(); ?>" class="footer-item" target="_blank">
                    <?= ArrayHelper::getValue($documents, 'Политика приватности')->name; ?>
                </a>
                <div class="footer-item">Copyright © 2021. Все права защищены</div>
                <p  class='logotype'> Разработано в <a href="https://www.a-lux.kz" target="_blank"><img src="/images/image/logo_a-lux.png" alt=""></a></p>
            </div>
        </div>
    </div>
</section>


<a href="#" id="toTopBtn" class="cd-top text-replace js-cd-top cd-top--is-visible cd-top--fade-out" data-abc="true"></a>

<script>
    $(document).ready(function() {
$(window).scroll(function() {
if ($(this).scrollTop() > 20) {
$('#toTopBtn').fadeIn();
} else {
$('#toTopBtn').fadeOut();
}
});

$('#toTopBtn').click(function() {
$("html, body").animate({
scrollTop: 0
}, 1000);
return false;
});
});
</script>