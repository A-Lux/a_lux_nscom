<?php

use yii\helpers\Url;

?>

<section class="head">
    <div class="header">
        <div class="container">
            <div class="row d-flex justify-content-between offsets">
                <div class="head-inner col-lg-8">
                    <nav class="navbar navbar-expand-lg navbar-light navs d-flex align-items-center">

                        <ul class="navbar-nav">
                            <a class="navbar-brand" href="/">
                                <img src="<?= Yii::$app->view->params['logoHeader']->getImage(); ?>">
                            </a>
                        </ul>
                        <ul class="navbar-nav">
                            <? foreach (Yii::$app->view->params['menuHeader'] as $item): ?>
                                <li class="nav-item  header-links">
                                    <a class="nav-link item company" href="<?= $item->url; ?>">
                                        <?= $item->name; ?>
                                    </a>
                                </li>
                            <? endforeach; ?>
                        </ul>

                    </nav>
                </div>

                <div class="header-filter col-lg-4">
                    <div class="header-links">
                        <a class="nav-link" href="tel:<?= Yii::$app->view->params['contact']->header_phone; ?>">
                            <img src="/images/icons/icon.png" alt="">
                            <?= Yii::$app->view->params['contact']->header_phone; ?>
                        </a>
                    </div>
                    <div class="dropdown header-filter-icons">
                        <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="/images/icons/icon2.png" alt=""> Алматы
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <? foreach (Yii::$app->view->params['city'] as $item): ?>
                                <a class="dropdown-item" href="#"><?= $item->name; ?></a>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <div class="dropdown  header-filter-icons">
                        <a class="btn dropdown-toggle language__btn"
                           href="<?= Url::to(array_merge(\Yii::$app->request->get(),
                               [\Yii::$app->controller->route, 'language' => Yii::$app->language])) ?>"
                           role="button" id="dropdownMenuLink"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="" alt=""> <?= Yii::$app->language; ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <? foreach (Yii::$app->view->params['language'] as $value): ?>
                                <a class="dropdown-item language__item"
                                   href="<?= Url::to(array_merge(\Yii::$app->request->get(),
                                       [\Yii::$app->controller->route, 'language' => $value->code])) ?>"
                                   data-src="<?= $value->getImage(); ?>">
                                    <?= $value->name; ?>
                                </a>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <div class='mobile-links '>
        <div class="mobile-contacts">
            <div class="mob-header-filter col-lg-12 d-flex  align-items-center">
                <div class="header-links">
                    <a class="nav-link" id="contact-link" href="tel:<?= Yii::$app->view->params['contact']->header_phone; ?>">
                        <img src="/images/icons/icon-phone.svg" alt="">
                        <?= Yii::$app->view->params['contact']->header_phone; ?>
                    </a>
                </div>
                <div class="dropdown header-filter-icons">
                    <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <img src="/images/icons/icon-map.svg" alt=""> Алматы
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <? foreach (Yii::$app->view->params['city'] as $item): ?>
                            <a class="dropdown-item" href="#"><?= $item->name; ?></a>
                        <? endforeach; ?>
                    </div>
                </div>
                <div class="dropdown  header-filter-icons">
                    <a class="btn dropdown-toggle language__btn"
                       href="<?= Url::to(array_merge(\Yii::$app->request->get(),
                           [\Yii::$app->controller->route, 'language' => Yii::$app->language])) ?>"
                       role="button" id="dropdownMenuLink" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <img src="" alt=""> <?= Yii::$app->language; ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <? foreach (Yii::$app->view->params['language'] as $value): ?>
                            <a class="dropdown-item language__item" href="<?= Url::to(array_merge(\Yii::$app->request->get(),
                                [\Yii::$app->controller->route, 'language' => $value->code])) ?>"
                               data-src="<?= $value->getImage(); ?>">
                                <?= $value->name; ?>
                            </a>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-header d-flex">
            <div class="container">
            <div class="row">
                <div class="col-4 col-md-5">
                    <div class="mobile-logo">
                    <a class="navbar-brand" href="/">
                        <img src="<?= Yii::$app->view->params['logoHeader']->getImage(); ?>" alt="">
                    </a>
                    </div>
                </div>
                <div class="col-4 col-md-5">
                    <div class="mobile-icons">
                        <ul class="d-flex">
                            <li class="nav-item">
                                <a class="nav-link item nav-icon mobile-nav-icon" href="/profile">
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link item nav-icon mobile-nav-icon"
                                   href="<?= Yii::$app->user->isGuest ? '/sign-up' : '/profile' ?>">
                                    <img src="/images/icons/Vector.svg" alt="">
                                </a>
                            </li>
                            <li class="nav-item icons">
                                <a class="nav-link item nav-icon mobile-nav-icon" href="/basket">
                                    <img src="/images/icons/icon-basket.svg" alt=""></a>
                                <span class="basket-quantity"><?= count($_SESSION['basket']) ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <div class="mobile-catalog ">
            <div class="container">
                <div class="row mob-row">
                    <div class="col-6 col-md-4 col-lg-4">
                        <div class="mobile-catalog">
                            <a class="toggle hc-nav-trigger hc-nav-1">
                                <span></span>
                                Каталог товаров
                            </a>
                            <nav id="main-nav">
                                <ul>
                                    <? foreach (Yii::$app->view->params['catalog'] as $catalog): ?>
                                        <? if ($catalog->parent_id == null): ?>
                                            <li>
                                                <? if ($catalog->child): ?>
                                                    <a href="#">
                                                        <?= $catalog->name; ?>
                                                    </a>
                                                    <ul>
                                                        <? foreach ($catalog->child as $category): ?>
                                                            <li>
                                                                <? if ($category->child): ?>
                                                                    <a href="#">
                                                                        <?= $category->name; ?>
                                                                    </a>
                                                                    <ul>
                                                                        <? foreach ($category->child as $value): ?>
                                                                        <li>
                                                                            <a href="<?= Url::to(['/catalog/view', 'slug' => $value->slug]) ?>">
                                                                                <?= $value->name; ?>
                                                                            </a>
                                                                        </li>
                                                                        <? endforeach; ?>
                                                                    </ul>
                                                                <? else: ?>
                                                                    <a href="<?= Url::to(['/catalog/view', 'slug' => $category->slug]) ?>">
                                                                        <?= $category->name; ?>
                                                                    </a>
                                                                <? endif; ?>
                                                            </li>
                                                        <? endforeach; ?>
                                                    </ul>
                                                <? else: ?>
                                                    <a href="<?= Url::to(['/catalog/view', 'slug' => $catalog->slug]) ?>">
                                                        <?= $catalog->name; ?>
                                                    </a>
                                                <? endif; ?>
                                            </li>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-6 col-md-5 col-lg-4">
                        <div class="mobile-search">
                            <form action="/search" method="get">
                                <input type="text" name="text">
                                <img class="mob-search-img" src="/images/icons/icon-search.svg" alt="">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="burger-menu">
        <a href="#" class="burger-menu-btn">
            <span class="burger-menu-lines"></span>
        </a>
    </div>
    <div class="nav-panel-mobil">
        <div class="container">
            <nav class="navbar-expand-lg navbar-light">
                <ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
                    <? foreach (Yii::$app->view->params['menuHeader'] as $item): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $item->url; ?>">
                                <?= $item->name; ?>
                            </a>
                        </li>
                    <? endforeach; ?>
                </ul>

            </nav>
        </div>
    </div>

</section>


<div class="bottom-header sticky-top">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light navs d-flex align-items-center justify-content-between">
                <ul class="navbar-nav bottom-links catalog-link col-xl-3 col-md-4">
                    <li class="nav-item catalog-btn ">
                        <div class="mobile-menu-btn e-button open d-flex justify-content-start">
                            <div>
                                <a class="btn hr" data-toggle="collapse" href="#collapseExample" role="button"
                                   aria-expanded="false" aria-controls="collapseExample">
                                    Каталог товаров
                                </a>
                                <div class="e-burger">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul class="navbar-nav bottom-links search-box ">
                    <li class="nav-item  search">
                        <form action="/search" method="get">
                            <input type="text" placeholder='Введите название товара' name="text" id="search_text">
                            <img class="search-img" src="/images/icons/Combined%20Shape.png" alt="" onclick="onMyClick()">
                            <script>
                                function onMyClick(){
                                    window.location.href = `/search?text=${document.querySelector('#search_text').value}`
                                }
                                </script>
                        </form>
                    </li>
                </ul>
                <ul class="navbar-nav bottom-links nav-icons">
                    <li class="nav-item catalog-button col-6 col-md-4 p-0">

                    </li>
                    <li class="nav-item">
                        <a class="nav-link item nav-icon" href="/profile#favorites">
                            <img src="/images/icons/Star%201.png" alt="">
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link item nav-icon"
                           href="<?= Yii::$app->user->isGuest ? '/sign-up' : '/profile' ?>">
                            <img src="/images/icons/Vector.svg" alt="">
                        </a>
                    </li>
                    <li class="nav-item icons">
                        <a class="nav-link item nav-icon" href="/basket"><img
                                    src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%BA%D0%B0.png" alt=""></a>
                        <span class="basket-quantity"><?= count($_SESSION['basket']) ?></span>
                    </li>
                </ul>
            </nav>


            <div class="collapse collapse-main e-list" id="collapseExample">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-xl-5 col-8 pr-0 ">
                <div class="nav flex-column nav-pills category-pills list-item" id="v-pills-tab" role="tablist"
                     aria-orientation="vertical">
                    <? foreach (Yii::$app->view->params['catalog'] as $catalog): ?>
                        <? if ($catalog->parent_id == null): ?>
                            <? if($catalog->child): ?>
                            <a class="nav-link tabs d-flex justify-content-between align-items-center"
                               id="v-pills-home-<?= $catalog->id; ?>-tab" data-toggle="pill"
                               href="#v-pills-home-<?=$catalog->id?>"
                               role="tab" aria-controls="v-pills-home-<?= $catalog->id; ?>"
                               aria-selected="true"><?= $catalog->name; ?> <img src="/images/icons/Arrow.png" alt="">
                            </a>
                            <? else: ?>
                                <a class="nav-link tabs d-flex justify-content-between align-items-center"
                                   href="<?= Url::to(['/catalog/view', 'slug' => $catalog->slug])?>">
                                    <?= $catalog->name; ?>
                                </a>
                            <? endif; ?>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
            </div>
            <div class="col-lg-7 col-xl-5 col-4 pl-0 pr-0">
                <div class="tab-content tab-item d-flex align-items-center" id="v-pills-tabContent ">
                    <? foreach (Yii::$app->view->params['catalog'] as $catalog): ?>
                        <? if ($catalog->child): ?>
                            <div class="tab-pane fade panel" id="v-pills-home-<?= $catalog->id; ?>" role="tabpanel"
                                 aria-labelledby="v-pills-home-<?= $catalog->id; ?>-tab">
                                <div class="row">
                                    <? foreach ($catalog->child as $category): ?>
                                        <div class="col-lg-6 tab-link d-flex flex-column">
                                            <a href="<?= Url::to(['/catalog/view', 'slug' => $category->slug]) ?>"
                                               class="categories-link">
                                                <?= $category->name; ?>
                                            </a>
                                            <? if ($category->child): ?>
                                                <? foreach ($category->child as $value): ?>
                                                    <a href="<?= Url::to(['/catalog/view', 'slug' => $value->slug]) ?>">
                                                        <?= $value->name; ?>
                                                    </a>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        <? endif; ?>
                    <? endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>

    <style>
        #v-pills-home-5{
            position: absolute;
            background: white;
            margin-top: 473px;
            overflow: scroll;
        }
        .tab-pane::-webkit-scrollbar {
        width: 0;
        }
        #v-pills-home-25{
            position: absolute;
            background: white;
            margin-top: 585px;
            overflow: scroll;
        }
        #v-pills-home-28{
            position: absolute;
            background: white;
            margin-top: 635px;
            overflow: scroll;
        }
        #v-pills-home-36{
            position: absolute;
            background: white;
            margin-top: 747px;
            overflow: scroll;
        }
        #v-pills-home-47{
            position: absolute;
            background: white;
            margin-top: 805px;
            overflow: scroll;
        }
        #v-pills-home-66{
            position: absolute;
            background: white;
            margin-top: 995px;
            overflow: scroll;
        }
        #v-pills-home-92{
            position: absolute;
            background: white;
            margin-top: 915px;
            overflow: scroll;
        }
        .search-img{
            cursor: pointer;
        }
    </style>


