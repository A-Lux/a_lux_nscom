<?php

/* @var $this View */
/* @var $searchModel GuaranteeSearch */
/* @var $menu \common\models\Menu */

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\models\search\GuaranteeSearch;

?>

<div class="garant">
    <div class="container">
        <?= $this->render('/partials/breadcrumbs', [
                'menu'  => $menu,
        ]); ?>
    </div>
    <div class="container">
        <div class="row garant-row">
            <div class="col-xl-4 col-12 col-md-12 garant-nav">
                <div class="garant-btn">
                    <div class="nav flex-column garant-content nav-pills" id="v-pills-tab" role="tablist"
                         aria-orientation="vertical">
                        <? foreach($dataProvider->models as $model): ?>
                            <a class="nav-link <?= $model->id == 1 ? 'active tab-return' : '' ?>  garant-tabs text-center"
                               id="#guarantee-<?= $model->id;?>-btn" data-toggle="pill" href="#guarantee-<?= $model->id;?>-btn"
                               role="tab" aria-controls="#guarantee-<?= $model->id;?>-btn"
                               aria-selected="<?= $model->id == 1 ? 'true' : 'false' ?>">
                                <?= $model->title; ?>
                            </a>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-md-12 col-12">
                <div class="garant-btn-content">
                    <div class="tab-content" id="v-pills-tabContent">
                        <? foreach($dataProvider->models as $model): ?>
                            <div class="tab-pane fade <?= $model->id == 1 ? 'show active' : '' ?>"
                                 id="guarantee-<?= $model->id;?>-btn" role="tabpanel"
                                 aria-labelledby="guarantee-<?= $model->id;?>-tab">
                                <div class="row garant-row">
                                    <?= $model->content; ?>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
