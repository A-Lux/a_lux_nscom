<?php

/* @var $this View */
/* @var $searchModel RequisitesSearch */
/* @var $menu \common\models\Menu */
/* @var $contact \common\models\Contact */

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\models\search\RequisitesSearch;

?>

<section class="contact">
    <div class="container">
        <div class="row d-flex flex-column">
            <?= $this->render('/partials/breadcrumbs', [
                    'menu' => $menu,
            ]); ?>
            <div class="contact-inner-forms d-flex pr-0">
                <div class="col-xl-6">
                    <div class="contact-item d-flex flex-column justify-content-center">
                        <h2>Адрес:</h2>
                        <?= $contact->address; ?>
                    </div>
                    <div class="contact-item phone ">
                        <h2>Контактные телефоны:</h2>
                        <div class="phone-content d-flex align-items-center">
                            <img src="/images/icons/phone.png" alt="">
                            <?= $contact->contact_phone; ?>
                        </div>
                    </div>
                    <div class="contact-item  ">
                        <h2>Реквизиты компании</h2>
                        <? foreach ($dataProvider->models as $model): ?>
                            <div class="d-flex align-items-center">
                                <div class="contact-content col-lg-6 p-0">
                                    <h3><?= $model->source; ?></h3>
                                </div>
                                <div class="contact-content-p col-lg-6 p-0 d-flex flex-column">
                                    <p><?= $model->value; ?></p>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="map">
                        <?= $contact->iframe; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>