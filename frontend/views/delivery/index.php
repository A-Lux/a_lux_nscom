<?php

/* @var $this View */
/* @var $searchModel DeliverySearch */
/* @var $menu \common\models\Menu */

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\models\search\DeliverySearch;


?>

<div class="delivery">
    <div class="container">
        <?= $this->render('/partials/breadcrumbs', [
                'menu'  => $menu,
        ]); ?>
    </div>
    <div class="container">
        <div class="row export-nav">
            <div class="col-xl-4 export-btn">
                <div class="export-item">
                    <div class="nav flex-column nav-pills export-content " id="v-pills-tab" role="tablist"
                         aria-orientation="vertical">
                        <? foreach($dataProvider->models as $model): ?>
                            <a class="nav-link <?= $model->id == 1 ? 'active' : '' ?> export-tabs text-center" data-toggle="pill"
                               href="#delivery-<?= $model->id;?>" role="tab" aria-controls="delivery-<?= $model->id;?>"
                               aria-selected="<?= $model->id == 1 ? 'true' : 'false' ?>">
                                <?= $model->title ?>
                            </a>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 export-btn-content">
                <div class="tab-content" id="v-pills-tabContent">
                    <? foreach ($dataProvider->models as $model): ?>
                        <div class="tab-pane fade <?= $model->id == 1 ? 'show active' : '' ?>" id="delivery-<?= $model->id;?>" role="tabpanel"
                             aria-labelledby="delivery-<?= $model->id;?>-tab">
                                <?= $model->content; ?>
                        </div>
                    <? endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>
