<?php

    use yii\web\View;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use frontend\models\SignupForm;


    /* @var $this View */
    /* @var $model  SignupForm */
    /* @var $form ActiveForm */

?>

<section class="authorization">
    <div class="container">
        <div class="col-xl-12 p-0">
            <div class="reg-title">
                <h1>Регистрация</h1>
            </div>
        </div>
        <? $form    =   ActiveForm::begin([
            'id'        =>  'sign-up-client-form',
        ]); ?>
        <div class="row author-row">
            <div class="col-xl-6 col-12 col-md-6">
                <div class="reg-form">
                    <h2>E-mail</h2>
                    <?= $form->field($model, 'email')->textInput([
                        'placeholder'   => 'example@gmail.com',
//                                'email'         => 'email',
                    ])->label(false) ?>
                </div>
            </div>
            <div class="col-xl-6 col-12 col-md-6">
                <div class="reg-form ">
                    <h2>Пароль</h2>
                    <?= $form->field($model, 'password')->passwordInput([
                        'id'    => 'password-field',
                        'class' => 'password-input form-control',
                    ])->label(false) ?>
                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    <a href="/password-reset"><p>Забыли пароль</p></a>
                </div>
            </div>
            <div class="reg-form author-form">
                <div class="col-xl-12">

                    <div class="author-btn">
                        <?= Html::submitButton('Авторизоваться', ['class' => 'author-button']) ?>
                    </div>

                </div>
            </div>

        </div>

        <? ActiveForm::end(); ?>
    </div>
</section>
