<?php

/* @var $this View */
/* @var $searchModel BrandSearch */
/* @var $menu \common\models\Menu */

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\models\search\BrandSearch;



?>

<div class="card-brand">
    <div class="container">
        <div class="col-xl-12 p-0">
            <div class="reg-title">
                <h1>Бренды</h1>
            </div>
        </div>
        <div class="col-xl-12 p-0">
            <div class="card-inner">
                <? foreach ($dataProvider->models as $model): ?>
                    <a href="<?= Url::to(['view', 'slug' => $model->slug]) ?>">
                        <div class="card-links">
                            <img src="<?= $model->getImage(); ?>" alt="">
                        </div>
                    </a>
                <? endforeach; ?>

            </div>
        </div>
    </div>
</div>
