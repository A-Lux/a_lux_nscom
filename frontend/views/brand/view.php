<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Brand */
/* @var $menu common\models\Menu */

?>

<div class="auth-inner">
    <div class="container">
        <div class="prof-title compl-title col-lg-12 ">
            <a href="/"> <span>Главная </span></a>
            <p style='padding: 0 10px;margin: 0'>></p>
            <p style='margin: 0'>Бренды</p>
            <p style='padding: 0 10px;margin: 0'>></p>
            <p style='margin: 0'><?= $model->name;?></p>
        </div>
        <div class="col-xl-12 p-0">
            <div class="reg-title">
                <h1><?= $model->name; ?></h1>
            </div>
        </div>
        <div class="row auth-row">
            <div class="col-xl-12">
                <div class="author-prev">
                    <span><img src="/images/icons/prev-author.png" alt=""></span>
                    <span>Назад</span>
                </div>
            </div>
            <div class="col-xl-12">
                <div class="author-content d-flex">
                    <div class="col-xl-4 col-12 col-md-5">
                        <div class="author-image">
                            <img src="<?= $model->getImage(); ?>" alt="">
                        </div>
                    </div>
                    <div class="col-xl-8 col-12 col-md-7">
                        <div class="author-txt">
                            <div class="reg-title auth-title">
                                <h1><?= $model->title; ?></h1>
                            </div>
                            <?= $model->content; ?>

                            <? if($model->statusButton == 1): ?>
                                <div class="author-btn auth-btn text-center">
                                    <button>
                                        <? if(empty($model->file)): ?>
                                            <?= Html::a('Скачать прайслист', $model->getFile(), ['target'=>'_blank']) ?>
                                        <? else: ?>
                                            <a href="/site/index#catalog-content">Перейти в каталог</a>
                                        <? endif; ?>
                                    </button>
                                </div>
                            <? endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
