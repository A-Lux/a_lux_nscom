<?php

/* @var $this View */
/* @var $searchModel NewsSearch */
/* @var $menu \common\models\Menu */

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\models\search\NewsSearch;

?>

<div class="news">
    <div class="container">
        <?= $this->render('/partials/breadcrumbs', [
                'menu' => $menu
        ]); ?>
    </div>
    <div class="container">
        <div class="row news-link">
            <? foreach ($dataProvider->models as $model): ?>
                <div class="col-xl-4 col-md-6">
                <a href="<?= Url::to(['view', 'slug' => $model->slug]) ?>" style="text-decoration: none">
                    <div class="news-content">
                        <div class="news-img">
                            <img src="<?= $model->getImage();?>" alt="">
                        </div>
                        <div class="news-txt">
                            <h3><?= $model->title; ?></h3>
                            <p><?= $model->description; ?></p>
                            <div class="news-btn text-center">
                                <button>ПОДРОБНЕЕ</button>
                            </div>

                        </div>
                    </div>
                </a>
            </div>
            <? endforeach; ?>

            <div class="col-xl-12">
                <div class="collapse" id="collapseExample-news">
                    <div class="row">
                        <div class="col-xl-4 col-md-6">
                            <a href="news-inner.html" style="text-decoration: none">
                                <div class="news-content">
                                    <div class="news-img">
                                        <img src="images/image/news.png" alt="">
                                    </div>
                                    <div class="news-txt">
                                        <h3>Купите два маршрутизатора <br> и получите 3-й в подарок</h3>
                                        <p>Акция действует с 28 ноября 2019г <br> по 30 ноября 2019г</p>
                                        <div class="news-btn text-center">
                                            <button>ПОДРОБНЕЕ</button>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-4 col-md-6">
                            <a href="news-inner.html" style="text-decoration: none">
                                <div class="news-content">
                                    <div class="news-img">
                                        <img src="images/image/news.png" alt="">
                                    </div>
                                    <div class="news-txt">
                                        <h3>Купите два маршрутизатора <br> и получите 3-й в подарок</h3>
                                        <p>Акция действует с 28 ноября 2019г <br> по 30 ноября 2019г</p>
                                        <div class="news-btn text-center">
                                            <button>ПОДРОБНЕЕ</button>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-4 col-md-6">
                            <a href="news-inner.html" style="text-decoration: none">
                                <div class="news-content">
                                    <div class="news-img">
                                        <img src="images/image/news.png" alt="">
                                    </div>
                                    <div class="news-txt">
                                        <h3>Купите два маршрутизатора <br> и получите 3-й в подарок</h3>
                                        <p>Акция действует с 28 ноября 2019г <br> по 30 ноября 2019г</p>
                                        <div class="news-btn text-center">
                                            <button>ПОДРОБНЕЕ</button>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="download-btn text-center">
                    <button class="btn" type="button" data-toggle="collapse" data-target="#collapseExample-news"
                            aria-expanded="false" aria-controls="collapseExample-news">
                        ЗАГРУЗИТЬ ЕЩЕ НОВОСТИ
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
