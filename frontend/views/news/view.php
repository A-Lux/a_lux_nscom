<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $latestNews common\models\News */
/* @var $menu \common\models\Menu */


?>

<div class="news-inner">
    <div class="container">
        <?= $this->render('/partials/breadcrumbs', [
                'name' => $model->title,
                'menu'  => $menu,
            ]); ?>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-12">
                <div class="news-inner-content">
                    <div class="news-inner-img">
                        <img src="<?= $model->getImage(); ?>" alt="">
                    </div>
                    <div class="news-inner-txt">
                        <h3><?= $model->title; ?></h3>
                        <div class="d-flex align-items-center date">
                            <img src="/images/icons/clock.png" alt="">
                            <p><?= $model->date; ?></p>
                        </div>
                        <h2><?= $model->description; ?></h2>
                        <?= $model->content; ?>
                    </div>

                </div>
            </div>
            <div class="col-xl-4 col-12">
                <? if(!empty($latestNews)): ?>
                    <div class="new-content ">
                        <h1>Свежие новости</h1>

                        <? foreach ($latestNews as $news): ?>
                            <div class="new-link">
                                <div class="new-item d-flex flex-column align-items-center justify-content-center">
                                    <a class='links' href="" style='text-decoration:none;'>
                                        <?= $news->title; ?>
                                    </a>
                                </div>
                            </div>
                        <? endforeach;?>

                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>

</div>
