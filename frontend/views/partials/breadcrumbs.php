<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $menu common\models\Menu */
/* @var $name string */


?>

<? if(!empty($name) && isset($name)): ?>
    <div class="prof-title compl-title col-lg-12 ">
        <a href="/"> <span>Главная </span></a>
        <p style='padding: 0 10px;margin: 0'>></p>
        <p style='margin: 0'><?= $menu->name; ?></p>
        <p style='padding: 0 10px;margin: 0'>></p>
        <p style='margin: 0'><?= $name;?></p>
    </div>
<? else: ?>
    <div class="prof-title compl-title col-lg-12 d-flex align-items-center">
        <a href="/"> <span>Главная </span></a>
        <p style='padding: 0 10px;margin: 0'>></p>
        <p style='margin: 0'><?= $menu->name; ?></p>
    </div>
    <div class="col-lg-12 prof-cab p-0">
        <h1><?= $menu->name; ?></h1>
    </div>
<? endif;?>


