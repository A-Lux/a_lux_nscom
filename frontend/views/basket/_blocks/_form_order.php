<?php

    use yii\web\View;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use frontend\models\OrderForm;


    /* @var $this View */
    /* @var $model  OrderForm */
    /* @var $form ActiveForm */
    /* @var $total  */

    $model  = new OrderForm();

?>
<? $form    =   ActiveForm::begin([
    'id'        =>  'request-suggestion-form',
    'action'    => '/order/checkout',
]); ?>
<div class="row">
    <div class="col-xl-6">
        <div class="basket-form d-flex">
            <div class="col-xl-6 basket-left">
                <h2>ФИО</h2>
                <?= $form->field($model, 'username')->textInput([
                    'placeholder'   => 'Иван Иванович Иванов',
                    'required'      => 'required',
                ])->label(false) ?>
            </div>
            <div class="col-xl-6">
                <h2>Телефон</h2>
                <?= $form->field($model, 'phone')->textInput([
                    'placeholder'   => 'Телефон',
                    'required'      => 'required',
                ])->label(false) ?>
            </div>
        </div>
        <div class="basket-form d-flex">
            <div class="col-xl-6 basket-left">
                <h2>Адрес доставки </h2>
                <?= $form->field($model, 'address')->textInput([
                    'placeholder'   => 'Город, улица',
                    'required'      => 'required',
                ])->label(false) ?>
            </div>
            <div class="col-xl-6">
                <h2>E-mail</h2>
                <?= $form->field($model, 'email')->textInput([
                    'placeholder'   => 'example@gmail.com',
                    'required'      => 'required',
                ])->label(false) ?>
            </div>
        </div>
        <div class="comment-basket d-flex">
            <div class="col-xl-12 basket-left">
                <h2>Комментарий</h2>
                <?= $form->field($model, 'message')->textInput([
                    'placeholder'   => 'Введите комментарий',
                    'required'      => 'required',
                ])->label(false) ?>
            </div>
        </div>
    </div>
    <?= $form->field($model, 'sum')->textInput([
        'hidden'    => true,
        'class'     => 'input-total-basket',
        'value'     => $total,
    ])->label(false) ?>
    <div class="col-xl-6">
        <div class="total">
            <div class="total-item d-flex justify-content-between">
                <h2>Всего к оплате</h2>
                <span class="total-basket"><?= $total;?></span>
            </div>
            <div class="total-condition">
                <div class="total-checkbox">
                    <input id="box1" type="checkbox">
                    <label for="box1">Я принимаю условия <a href="" style="text-decoration: none">договора </a></label>
                </div>
            </div>
            <div class="total-btn d-flex ">
                <div class="col-xl-6 pl-0 continue">
                    <button>ПРОДОЛЖИТЬ ПОКУПКУ</button>
                </div>
                <div class="col-xl-6 pl-0 oform text-right">
                    <?= Html::submitButton('ОФОРМИТЬ ПОКУПКУ', ['class' => '']) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<? ActiveForm::end(); ?>
