<?php

use yii\web\View;
use yii\helpers\Html;
use common\models\Product;
use common\models\Client;

/* @var $products Product */
/* @var $user     Client */
?>

<div class="basket-inner">
    <div class="container">
        <div class="prof-title compl-title col-lg-12 d-flex align-items-center">
            <a href="/"> <span>Главная </span></a>
            <p style='padding: 0 10px;margin: 0'>></p>
            <p style='margin: 0'>Корзина</p>
        </div>
        <div class="col-lg-12 prof-cab p-0">
            <h1>Корзина</h1>
        </div>
    </div>
    <div class="container empty-basket">
        <? if(empty($products)): ?>
            <div class="empty-div"> К сожалению, на данный моммент Ваша корзина пуста.</div>
        <? else: ?>
            <div class="row not-empty">
                <? foreach ($products as $product): ?>
                    <div class="col-xl-12">
                        <div class="basket-contents d-flex justify-content-between">
                            <div class="col-xl-3 col-md-3">
                                <div class="basket-img">
                                    <img src="<?= $product->getImage(); ?>" alt="">
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-5">
                                <div class="basket-items ">
                                    <h2><?= $product->name; ?></h2>
                                    <span><?= $product->price; ?></span>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-4 pr-0">
                                <div class="basket-buttons d-flex justify-content-between align-items-center">
                                    <div class="basket-count-btn down-product" data-id="<?= $product->id ?>">
                                        <img src="/images/image/%D0%BC%D0%B8%D0%BD%D1%83%D1%81.png" alt="">
                                    </div>
                                    <input type="text" min="1" class="basket-count quantity" id="countProduct-<?= $product->id ?>"
                                           value="<?= $product->count ?>" data-id="<?= $product->id; ?>">
                                    <div class="basket-count-btn up-product" data-id="<?= $product->id ?>">
                                        <img src="/images/image/%D0%BF%D0%BB%D1%8E%D1%81.png" alt="">
                                    </div>
                                    <button class="baskets delete-product" data-id="<?= $product->id; ?>">
                                        <img src="/images/image/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0-3.png"
                                                                 alt="">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endforeach;?>
            </div>
            <form name="basketForm" id="basket-form">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                <div class="row">
                    <div class="col-xl-6">
                        <div class="basket-form d-flex">
                            <div class="col-xl-6 basket-left">
                                <h2>ФИО</h2>
                                <input type="text" name="Orders[username]" placeholder="Иван Иванович Иванов"
                                       value="<?= $user->username; ?>" required>
                            </div>
                            <div class="col-xl-6">
                                <h2>Телефон</h2>
                                <input type="text" name="Orders[phone]" placeholder="+7(999) 999-9999"
                                       value="<?= $user->mobile_phone; ?>" required>
                            </div>
                            <script>
                                $('input[name="Orders[phone]"]').inputmask("+7(999) 999-9999");
                            </script>
                        </div>
                        <div class="basket-form d-flex">
                            <div class="col-xl-6 basket-left">
                                <h2>Адрес доставки </h2>
                                <input type="text" name="Orders[address]" placeholder="Россия"
                                       value="" required>
                            </div>
                            <div class="col-xl-6">
                                <h2>E-mail</h2>
                                <input type="text" name="Orders[email]" placeholder="example@mail.com"
                                       value="<?= Yii::$app->user->identity->email; ?>" required>
                            </div>
                        </div>
                        <div class="comment-basket d-flex">
                            <div class="col-xl-12 basket-left">
                                <h2>Комментарий</h2>
                                <input type="text" name="Orders[message]"
                                       placeholder="Введите комментарий">
                            </div>
                        </div>
                        <input type="hidden" name="Orders[sum]" class="input-total-basket" value="<?=$total?>">
                    </div>
                    <div class="col-xl-6">
                        <div class="total">
                            <div class="total-item d-flex justify-content-between">
                                <h2>Всего к оплате</h2>
                                <span class="total-basket"><?= $total;?></span>
                            </div>
                            <div class="total-condition">
                                <div class="total-checkbox">
                                    <input id="box1" type="checkbox"/>
                                    <label for="box1">Я принимаю условия <a href="" style="text-decoration: none">договора </a></label>
                                </div>
                            </div>
                            <div class="total-btn d-flex ">
                                <div class="col-xl-6 col-12 pl-0 ">
                                    <div class="continue">
                                        <a href="/site/index#catalog-content">ПРОДОЛЖИТЬ ПОКУПКУ</a>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-12 pl-0 oform text-right">
                                    <button type="submit" class="btn-basket-pay">ОФОРМИТЬ ПОКУПКУ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        <? endif;?>
    </div>
</div>
