<?php

/* @var $this View */
/* @var $menu \common\models\Menu */

/* @var $aboutMain      About */
/* @var $aboutSecond    About */
/* @var $aboutThird     About */

/* @var $itemSpecialization AboutItem */
/* @var $itemPartners       AboutItem */

/* @var $aboutPartners       AboutPartners */


use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\AboutItem;
use common\models\About;
use common\models\AboutPartners;


?>

<div class="company">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 ">
                <div class="company-about d-flex">
                    <div class="col-xl-8 pl-0 col-12 col-md-6">
                        <div class="reg-title company-title">
                            <h1>О компании</h1>
                        </div>
                        <?= $aboutMain->content; ?>
                    </div>
                    <div class="col-xl-4 col-12 col-md-6">
                        <img src="<?= $aboutMain->getImage(); ?>" alt="">
                    </div>
                </div>
            </div>
            <div class="company-line">

            </div>
            <div class="col-xl-12">
                <div class="company-link">
                    <h1>Мы специализируемся на поставках оборудования и проектных решений по следующим категориям:</h1>
                    <div class="company-content">
                        <? foreach ($itemSpecialization as $item): ?>
                            <div class="company-items">
                                <div class="company-image">
                                    <img src="<?= $item->getImage(); ?>"
                                         alt="">
                                </div>
                                <p><?= $item->name; ?></p>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="company-line">
            </div>
            <div class="col-xl-12">
                <div class="company-link company-partners-title">
                    <h1>Мы предлагаем нашим Партнёрам:</h1>
                </div>
            </div>

            <? foreach ($itemPartners as $item): ?>
                <div class="col-xl-4 col-md-6">
                    <div class="company-partners d-flex">
                        <img src="<?= $item->getImage(); ?>" alt="">
                        <p><?= $item->name; ?></p>
                    </div>
                </div>
            <? endforeach; ?>

            <div class="company-line">
            </div>


            <div class="partners-inner d-flex">
                <div class="col-xl-7 pl-0">
                    <div class="company-link about-partner">
                        <h1><?= $aboutSecond->title; ?></h1>
                        <?= $aboutSecond->content; ?>
                    </div>
                </div>
                <div class="col-xl-5 pr-0">
                    <div class="partners-list">
                        <? foreach ($aboutPartners as $item): ?>
                            <a href="<?= $item->link; ?>" target="_blank">
                                <?= $item->name; ?>,
                            </a>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            
            
            <div class="col-xl-12 p-0">
            <div class="brands mb-5 mt-2">
            <div class="container content">
            
                <div class="slide-inner">
                    <div class="brands-prev">
                        <img src="/images/icons/prev2.png" alt="">
                    </div>
                    <div class="brands-next">
                        <img src="/images/icons/next2.png" alt="">
                    </div>
                    <div class="brands-prev-line">
                    </div>
                    <div class="owl-carousel owl-theme owl-brands d-flex align-items-center  ">
                        <? foreach ($brands as $brand): ?>
                            <div class="item carousel-img">
                                <a href="<?= Url::to(['/brand/view', 'slug' => $brand->slug]) ?>">
                                    <img src="<?= $brand->getImage(); ?>" alt="">
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>
                    <div class="brands-next-line">
                    </div>
                </div>
               
            </div>
        </div>

                <? foreach ($aboutThird as $item): ?>
                    <div class="about-inner d-flex">
                        <div class="col-xl-6 pl-0">
                            <div class="about-image">
                                <img src="<?= $item->getImage(); ?>" alt="">
                            </div>
                        </div>
                        <div class="col-xl-6 pl-0">
                            <div class="company-link about-company">

                                <h1><?= $item->title; ?></h1>
                                <?= $item->content; ?>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</div>
