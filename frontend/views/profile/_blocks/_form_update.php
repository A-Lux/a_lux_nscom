<?php

use yii\web\View;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use common\models\Client;
use frontend\models\ProfileForm;


/* @var $this View */
/* @var $model  User */
/* @var $user  Client */
/* @var $form ActiveForm */

$model  = new ProfileForm();

?>

<? $form    =   ActiveForm::begin([
    'id'        =>  'user-form',
    'action'    => '/profile/update-user',
]); ?>
    <div class="profile-inner">
        <div class="profile-link  set-link d-flex">
            <div class="col-xl-5 p-0">
                <h3>Имя</h3>
            </div>
            <div class="col-xl-10 ">
                <?= $form->field($model, 'username')->textInput([
                    'placeholder'   => 'Имя',
                    'value'         => $user->username,
                ])->label(false) ?>
            </div>
        </div>
        <? if($model->status    == Client::STATUS_PARTNER): ?>
            <div class="profile-link set-link d-flex">
                <div class="col-xl-5 p-0">
                    <h3>Компания</h3>
                </div>
                <div class="col-xl-10">
                    <?= $form->field($model, 'company')->textInput([
                        'placeholder'   => 'Компания',
                        'value'         => $user->company,
                    ])->label(false) ?>
                </div>
            </div>
        <? endif; ?>
        <div class="profile-link set-link d-flex">
            <div class="col-xl-5 p-0">
                <h3>Регион</h3>
            </div>
            <div class="col-xl-10">
                <?= $form->field($model, 'region')->textInput([
                    'placeholder'   => 'Регион',
                    'value'         => $user->region,
                ])->label(false) ?>
            </div>
        </div>
        <div class="profile-link set-link d-flex">
            <div class="col-xl-5 p-0">
                <h3>Город</h3>
            </div>
            <div class="col-xl-10">
                <?= $form->field($model, 'city')->textInput([
                    'placeholder'   => 'Город',
                    'value'         => $user->city,
                ])->label(false) ?>
            </div>
        </div>
        <div class="profile-link set-link d-flex">
            <div class="col-xl-5 p-0">
                <h3>Рабочий телефон</h3>
            </div>
            <div class="col-xl-10">
                <?= $form->field($model, 'work_phone')->textInput([
                    'placeholder'   => 'Рабочий телефон',
                    'value'         => $user->work_phone,
                ])->label(false) ?>
            </div>
        </div>
        <div class="profile-link set-link d-flex">
            <div class="col-xl-5 p-0">
                <h3>Мобильный телефон</h3>
            </div>
            <div class="col-xl-10">
                <?= $form->field($model, 'mobile_phone')->textInput([
                    'placeholder'   => 'Мобильный телефон',
                    'value'         => $user->mobile_phone,
                ])->label(false) ?>
            </div>
        </div>

        <?= Html::submitButton('Изменить', ['class' => 'change-btn']) ?>
    </div>

<? ActiveForm::end(); ?>


