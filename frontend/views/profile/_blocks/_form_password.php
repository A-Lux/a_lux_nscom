<?php

use yii\web\View;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use common\models\Client;
use frontend\models\SecureForm;


/* @var $this View */
/* @var $user  Client */
/* @var $form ActiveForm */

$model  = new SecureForm();

?>

<form id="secure-form" name="secureForm">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
    <div class="profile-inner">
        <div class="profile-link  set-link d-flex">
            <div class="col-xl-5 p-0">
                <h3>Текущий пароль</h3>
            </div>
            <div class="col-xl-10 ">
                <input type="password" name="SecureForm[password]" required>
            </div>
        </div>
        <div class="profile-link set-link d-flex">
            <div class="col-xl-5 p-0">
                <h3>Новый пароль</h3>
            </div>
            <div class="col-xl-10">
                <input type="password" name="SecureForm[new_password]" required>
            </div>
        </div>
        <div class="profile-link set-link d-flex">
            <div class="col-xl-5 p-0">
                <h3>Подтвердите пароль</h3>
            </div>
            <div class="col-xl-10">
                <input type="password" name="SecureForm[new_password_repeat]" required>
            </div>
        </div>

        <button type="submit" class="change-btn">Сохранить</button>
    </div>
</form>