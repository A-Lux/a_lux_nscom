<?php

/* @var $this View */
/* @var $menu \common\models\Menu */
/* @var $model User */
/* @var $user Client */

/* @var $orders Orders */

/* @var $favorite */
/* @var $session */

/* @var $favoriteProduct    \common\models\FavoritesProduct */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\User;
use common\models\Client;
use yii\helpers\ArrayHelper;
use common\models\Orders;

?>

<section class="profile">
    <div class="container">
        <div class="prof-title compl-title col-lg-12 d-flex align-items-center">
            <a href="/"> <span>Главная </span></a>
            <p style='padding: 0 10px;margin: 0'>></p>
            <p style='margin: 0'>Личный кабинет</p>
        </div>
        <div class="col-lg-12 prof-cab p-0">
            <h1>Личный кабинет</h1>
        </div>
    </div>
    <div class="container">
        <div class="row profile-nav ">

            <div class="col-xl-3 p-0 ">
                <div class="profile-item">
                    <div class="nav flex-column nav-pills profile-content " id="v-pills-tab" role="tablist"
                         aria-orientation="vertical">
                        <a class="nav-link active clicked-profile  profile-tabs" id="profile1" data-toggle="pill" href="#profile"
                           role="tab" aria-controls="profile" aria-selected="true">Личный кабинет <img
                                    src="/images/icons/Arrow.png" alt=""></a>
                        <a class="nav-link clicked-profile profile-tabs" id="orders1" data-toggle="pill" href="#orders" role="tab"
                           aria-controls="orders" aria-selected="false">Заказы <img src="/images/icons/Arrow.png"
                                                                                    alt=""></a>
                        <a class="nav-link clicked-profile profile-tabs" id="bonus-1" data-toggle="pill" href="#bonus" role="tab"
                           aria-controls="bonus" aria-selected="false">Бонусы <img src="/images/icons/Arrow.png" alt=""></a>
                        <a class="nav-link clicked-profile profile-tabs" id="favorites1" data-toggle="pill" href="#favorites" role="tab"
                           aria-controls="favorites" aria-selected="false">Избранное <img src="/images/icons/Arrow.png"
                                                                                          alt=""></a>
                        <a class="nav-link clicked-profile profile-tabs" id="settings1" data-toggle="pill" href="#settings" role="tab"
                           aria-controls="settings" aria-selected="false">Настройки <img src="/images/icons/Arrow.png"
                                                                                         alt=""></a>
                        <a class="nav-link clicked-profile profile-tabs" id="password-tab" data-toggle="pill" href="#password" role="tab"
                           aria-controls="password" aria-selected="false">Сменить пароль <img src="/images/icons/Arrow.png"
                                                                                         alt=""></a>
                        <a href="/login/logout" class="nav-link profile-tabs">Выход</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 p-0">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active profile-pills" id="profile" role="tabpanel"
                         aria-labelledby="profile-tab">
                        <div class="row">
                            <div class="col-xl-6">

                                <h1 class="p-title">Основная информация</h1>
                                <div class="profile-inner">
                                    <div class="profile-link d-flex">
                                        <div class="col-xl-5 p-0">
                                            <h3>ФИО</h3>
                                        </div>
                                        <div class="col-xl-5 ">
                                            <p><?= $user->username ?></p>
                                        </div>
                                    </div>
                                    <div class="profile-link d-flex">
                                        <div class="col-xl-5 p-0">
                                            <h3>Электронная почта</h3>
                                        </div>
                                        <div class="col-xl-5">
                                            <p><?= $model->email; ?></p>
                                        </div>
                                    </div>
                                    <? if($model->status    == Client::STATUS_PARTNER): ?>
                                        <div class="profile-link d-flex">
                                            <div class="col-xl-5 p-0">
                                                <h3>Компания</h3>
                                            </div>
                                            <div class="col-xl-5">
                                                <p><?= $user->company; ?></p>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <div class="profile-link d-flex">
                                        <div class="col-xl-5 p-0">
                                            <h3>Регион</h3>
                                        </div>
                                        <div class="col-xl-5">
                                            <p><?= $user->region; ?></p>
                                        </div>
                                    </div>
                                    <div class="profile-link d-flex">
                                        <div class="col-xl-5 p-0">
                                            <h3>Город</h3>
                                        </div>
                                        <div class="col-xl-5">
                                            <p><?= $user->city; ?></p>
                                        </div>
                                    </div>
                                    <div class="profile-link d-flex">
                                        <div class="col-xl-5 p-0">
                                            <h3>Рабочий телефон</h3>
                                        </div>
                                        <div class="col-xl-5">
                                            <p><?= $user->work_phone; ?></p>
                                        </div>
                                    </div>
                                    <div class="profile-link d-flex">
                                        <div class="col-xl-5 p-0">
                                            <h3>Мобильный телефон</h3>
                                        </div>
                                        <div class="col-xl-5">
                                            <p><?= $user->mobile_phone; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade orders-panel " id="orders" role="tabpanel" aria-labelledby="orders-tab">
                        <div class="orders-inner">
                            <h1>Список заказов</h1>
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="orders-list">
                                            <table class="table table-hover profile-table">
                                                <thead class="table-title">
                                                <tr>
                                                    <th scope="col">№ заказа</th>
                                                    <th scope="col">
                                                        Дата
                                                    </th>
                                                    <th scope="col">Количество</th>
                                                    <th scope="col">
                                                        <div class="dropdown">
                                                            <button class="btn dropdown-toggle btn-to-dropdown"
                                                                    type="button" id="dropdownMenuButton"
                                                                    data-toggle="dropdown" aria-haspopup="true"
                                                                    aria-expanded="false">
                                                                Статус
                                                            </button>
                                                            <div class="dropdown-menu"
                                                                 aria-labelledby="dropdownMenuButton">
                                                                <a class="dropdown-item" href="#">Активный</a>
                                                                <a class="dropdown-item" href="#">Не активный</a>
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody class="table-body">
                                                    <? foreach ($orders as $order): ?>
                                                        
                                                        <tr data-toggle="modal" data-target="#modal-tab-<?= $order->id; ?>">
                                                            <th scope="row"><?= $order->order_id; ?></th>
                                                            <td><?= $order->created_at; ?></td>
                                                            <td><?= $order->sum;?></td>
                                                            <td><?= ArrayHelper::getValue(Orders::statusDescription(), $order->status)?></td>
                                                        </tr>
                                                       
                                                        
                                                   
                                                    <? endforeach; ?>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <? foreach($orders as $order): ?>
                        <div class="modal fade" id="modal-tab-<?= $order->id; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-tab-<?= $order->id; ?>" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-body">
                                <div class="order-inner">
                            <div class="row">

                                <? foreach ($order->orderedProducts as $item): ?>
                                    <div class="orders-item col-xl-12 d-flex">
                                        <div class="col-xl-5 col-md-5">
                                            <div class="orders-img">
                                            <img src="<?= $item->product->getImage(); ?>" alt="">
                                        </div>
                                    </div>
                                            <div class="col-xl-7 col-md-7">
                                                <h1><?= $item->product->name; ?></h1>
                                                <div class="order property d-flex">
                                                    <div class="col-xl-6 p-0">
                                                        <h5>Номер заказа:</h5>
                                                    </div>
                                                    <div class="col-xl-6 text-right">
                                                        <p><?= $item->order->order_id; ?></p>
                                                    </div>
                                                </div>
                                                <div class="order property d-flex">
                                                    <div class="col-xl-6 p-0">
                                                        <h5>Статус:</h5>
                                                    </div>
                                                    <div class="col-xl-6 text-right order-text active">
                                                        <p><?= $item->order->statusPay; ?></p>
                                                    </div>
                                                </div>
                                                <div class="order property d-flex ">
                                                    <div class="col-xl-6 p-0">
                                                        <h5>Цена:</h5>
                                                    </div>
                                                    <div class="col-xl-6 text-right">
                                                        <p><?= $item->product->price; ?> тг</p>
                                                    </div>
                                                </div>
                                                <div class="order d-flex">
                                                    <div class="col-xl-6  p-0">
                                                        <h5>Дата прибытие:</h5>
                                                    </div>
                                                    <div class="col-xl-6 text-right">
                                                        <p><?= $item->order->arrival_date; ?></p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                <? endforeach; ?>

                            </div>
                        </div>
                                </div>
                                <div class="modal-footer profile-modal">
                                    <button type="button" class="btn close-btn" data-dismiss="modal">Закрыть</button>
                                </div>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>

                    <div class="tab-pane fade bonus-panel" id="bonus" role="tabpanel" aria-labelledby="bonus-tab">
                        <div class="row">
                            <div class="col-xl-12 ">
                                <div class="balans">
                                    <h2>Баланс:</h2>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="bonuses d-flex align-items-center">
                                    <div class="bonus-img">
                                        <img src="/images/image/%D0%B1%D0%BE%D0%BD%D1%83%D1%81.png" alt="">
                                    </div>
                                    <div class="count-bonus d-flex ">
                                        <h3><?= $user->bonus ?></h3>
                                        <h4>бонусов</h4>
                                    </div>
                                    <div class="obmen">
                                        <button>ОБМЕНЯТЬ</button>
                                    </div>
                                    <div class="snyatie">
                                        <button>СНЯТЬ</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade favorite-panel" id="favorites" role="tabpanel"
                         aria-labelledby="favorites-tab">

                        <div class="row">
                            <? foreach ($favoriteProduct as $item): ?>
                                <div class="col-xl-4">
                                    <div class="product-card products">
                                        <i class="fa fa-2x fa-heart-o not-liked like-button btn-to-favorite"
                                           data-id="<?= $item->product->id;?>" data-favorite="<?= in_array($item->product->id, $favorite) ?>">
                                        </i>
                                        <a href="<?= Url::to(['/product/view', 'slug' => $item->product->slug]); ?>">
                                            <div class="products-img catalog-img">
                                                <img src="<?= $item->product->getImage(); ?>" alt="">
                                            </div>
                                            <p><?= $item->product->name; ?></p>
                                            <div class="cost" style="text-align: right">
                                                <span><?= $item->product->price; ?></span>
                                            </div>
                                        </a>
                                        <a class=" btn-to-basket w-100" type="button" data-id="<?= $item->product->id; ?>"
                                           data-session=<?= in_array($item->product->id, $session) ? 1 : 0 ?>>
                                            <button class="basket-btn w-100">
                                                <img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png"
                                                     alt="">
                                                В КОРЗИНУ
                                            </button>
                                        </a>

                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <div class="tab-pane fade setting" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                        <div class="row">
                            <div class="col-xl-6">

                                <h1 class="p-title">Основная информация</h1>
                                <?= $this->render('_blocks/_form_update', [
                                    'user' => $user,
                                ]) ?>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade setting" id="password" role="tabpanel" aria-labelledby="password-tab">
                        <div class="row">
                            <div class="col-xl-6">

                                <h1 class="p-title">Сменить пароль</h1>
                                <?= $this->render('_blocks/_form_password', [
                                    'user' => $user,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
