<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Banner;
use common\models\BannerMobile;
use common\models\Product;

/* @var $banners  Banner */
/* @var $bannersMobile  BannerMobile */
/* @var $catalog */
/* @var $brands */
/* @var $hitProduct */
/* @var $stockProduct */
/* @var $favorite */
/* @var $session */

$iteration = 0;
?>

<section class="slider">
    <div class="container">
    <div class="slideshow">
        <? foreach ($banners as $banner) : ?>
            <? if ($banner->status == Banner::STATUS_LINK): ?>
                <a href="https://<?= $banner->link ?>" target="_blank">
                    <div class="slide">
                        <img src="<?= $banner->getImage(); ?>" alt="<?= $banner->title; ?>">
                    </div>
                </a>
            <? elseif ($banner->status == Banner::STATUS_SEARCH): ?>
                <a href="<?= Url::to(['banner', 'id' => $banner->id]) ?>" target="_blank">
                    <div class="slide">
                        <img src="<?= $banner->getImage(); ?>" alt="<?= $banner->title; ?>">
                    </div>
                </a>
            <? endif; ?>
        <? endforeach; ?>
    </div>
    </div>
</section>
<section class='mobile-slider'>
    <div class="slideshow">
        <? foreach ($bannersMobile as $banner): ?>
            <? if ($banner->status == Banner::STATUS_LINK): ?>
                <a href="https://<?= $banner->link ?>" target="_blank">
                    <div class="slide">
                        <img src="<?= $banner->getImage(); ?>" alt="<?= $banner->title; ?>">
                    </div>
                </a>
            <? elseif ($banner->status == Banner::STATUS_SEARCH): ?>
                <a href="<?= Url::to(['banner', 'id' => $banner->id]) ?>" target="_blank">
                    <div class="slide">
                        <img src="<?= $banner->getImage(); ?>" alt="<?= $banner->title; ?>">
                    </div>
                </a>
            <? endif; ?>
        <? endforeach; ?>
    </div>
</section>
<section class='product-items'>
    <div class="catalog">
        <!-- <div class="container">
            <div class="content" id="catalog-content">
                <h1>Каталог товаров</h1>
                <div class="row d-flex align-items-center">
                    <? foreach ($catalog as $category) : ?><? $iteration++; ?>
                        <? if ($iteration == 1) : ?>
                            <div class="col-12 col-xs-12 col-lg-4 ">
                                <div class="catalog-item">
                                    <a href="<?= Url::to(['/catalog/view', 'slug' => $category->slug]); ?>">
                                        <div class="link">
                                            <p><?= $category->name; ?></p>
                                            <div class="img-tovar">
                                                <img src="<?= $category->getImage(); ?>" alt="">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <? elseif ($iteration == 2 || $iteration == 3) : ?>
                            <? if ($iteration == 2) : ?>
                                <div class="col-12 col-xs-12 col-lg-4 ">
                            <? endif; ?>
                            <div class="services catalog-item">
                                <a href="<?= Url::to(['/catalog/view', 'slug' => $category->slug]); ?>">
                                    <div class="link-2">
                                        <p><?= $category->name; ?></p>
                                        <div class="tovar2">
                                            <img src="<?= $category->getImage(); ?>" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <? if ($iteration == 3) : ?>
                                </div>
                            <? endif; ?>
                        <? elseif ($iteration == 4 || $iteration == 5) : ?>
                            <? if ($iteration == 4) : ?>
                                <div class="col-12 col-xs-12 col-lg-4 ">
                            <? endif; ?>
                            <div class="services catalog-item">
                                <a href="<?= Url::to(['/catalog/view', 'slug' => $category->slug]); ?>">
                                    <div class="link-2">
                                        <p><?= $category->name; ?></p>
                                        <div class="tovar2">
                                            <img src="<?= $category->getImage(); ?>" alt="">

                                        </div>
                                    </div>
                                </a>
                            </div>
                            <? if ($iteration == 5) : ?>
                                </div>
                            <? endif; ?>
                        <? else : ?>
                        <? endif; ?>
                    <? endforeach; ?>
                    <div class="line">

                    </div>
                </div>
            </div>
        </div> -->
        <div class="brands">
            <div class="container content">
                <h1>Бренды</h1>
                <div class="slide-inner">
                    <div class="brands-prev">
                        <img src="/images/icons/prev2.png" alt="">
                    </div>
                    <div class="brands-next">
                        <img src="/images/icons/next2.png" alt="">
                    </div>
                    <div class="brands-prev-line">
                    </div>
                    <div class="owl-carousel owl-theme owl-brands d-flex align-items-center  ">
                        <? foreach ($brands as $brand): ?>
                            <div class="item carousel-img">
                                <a href="<?= Url::to(['/brand/view', 'slug' => $brand->slug]) ?>">
                                    <img src="<?= $brand->getImage(); ?>" alt="">
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>
                    <div class="brands-next-line">
                    </div>
                </div>
                <div class="line">
                </div>
            </div>
        </div>
        <div class="hit-sales">
            <div class="container sales d-flex justify-content-between align-items-center">
                <div class="sales-title">
                    <h1>Хиты продаж</h1>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="recom-product product-company ">
                <? foreach ($hitProduct as $product) : ?>
                    <div class="product-card products-rec mt-4">
                        <i class="fa fa-2x fa-heart-o not-liked like-button btn-to-favorite"
                           data-id="<?= $product->id; ?>" data-favorite="<?= in_array($product->id, $favorite) ?>">
                        </i>
                        <a href="<?= Url::to(['/product/view', 'slug' => $product->slug]); ?>">
                            <div class="products-img">
                                <img src="<?= $product->getImage(); ?>" alt="">
                            </div>
                            <div class="products-info">
                                <p><?= $product->name; ?></p>
                                <? if($product->status  == Product::STATUS_NOT_AVAILABLE): ?>
                                    <p class='not-available'>Нет в наличии</p>
                                <? endif; ?>
                                <div class="cost" style="text-align: right">
                                    <span>KZT <?= $product->price; ?></span>
                                </div>
                            </div>
                        </a>
                        <a class="btn-to-basket w-100" type="button" data-id="<?= $product->id; ?>"
                           data-session=<?= in_array($product->id, $session) ? 1 : 0 ?>>
                            <button class="basket-btn w-100">
                                <img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt="">
                                В КОРЗИНУ
                            </button>
                        </a>
                    </div>
                <? endforeach; ?>
            </div>
            <div class="mob-recom-product">
                <div class="mob-product-slider d-flex ">
                    <? foreach ($hitProduct as $product) : ?>
                        <div class="product-card products-rec mt-4">
                            <i class="fa fa-2x fa-heart-o not-liked like-button btn-to-favorite"
                               data-id="<?= $product->id; ?>" data-favorite="<?= in_array($product->id, $favorite) ?>">
                            </i>
                            <a href="<?= Url::to(['/product/view', 'slug' => $product->slug]); ?>">
                                <div class="products-img">
                                    <img src="<?= $product->getImage(); ?>" alt="">
                                </div>
                                <p><?= $product->name; ?></p>
                                <? if($product->status  == Product::STATUS_NOT_AVAILABLE): ?>
                                    <p class='not-available'>Нет в наличии</p>
                                <? endif; ?>
                                <div class="cost" style="text-align: right">
                                    <span>KZT <?= $product->price; ?></span>
                                </div>
                            </a>
                            <a class="btn-to-basket w-100" type="button" data-id="<?= $product->id; ?>"
                               data-session=<?= in_array($product->id, $session) ? 1 : 0 ?>>
                                <button class="basket-btn w-100">
                                    <img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt="">
                                    В КОРЗИНУ
                                </button>
                            </a>

                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>

        <div class="hit-sales">
            <div class="container sales d-flex justify-content-between align-items-center">
                <div class="sales-title">
                    <h1>Акционные товары</h1>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="recom-product product-company ">
                <? foreach ($stockProduct as $product) : ?>
                    <div class="product-card products-rec mt-4">
                        <i class="fa fa-2x fa-heart-o not-liked like-button btn-to-favorite"
                           data-id="<?= $product->id; ?>" data-favorite="<?= in_array($product->id, $favorite) ?>">
                        </i>
                        <a href="<?= Url::to(['/product/view', 'slug' => $product->slug]); ?>">
                            <div class="products-img">
                                <img src="<?= $product->getImage(); ?>" alt="">
                            </div>
                            <div class="products-info">
                                <p><?= $product->name; ?></p>
                                <? if($product->status  == Product::STATUS_NOT_AVAILABLE): ?>
                                    <p class='not-available'>Нет в наличии</p>
                                <? endif; ?>
                                <div class="cost" style="text-align: right">
                                    <span>KZT <?= $product->price; ?></span>
                                </div>
                            </div>
                        </a>
                        <a class="btn-to-basket w-100" type="button" data-id="<?= $product->id; ?>"
                           data-session=<?= in_array($product->id, $session) ? 1 : 0 ?>>
                            <button class="basket-btn w-100">
                                <img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt="">
                                В КОРЗИНУ
                            </button>
                        </a>
                    </div>
                <? endforeach; ?>
            </div>
            <div class="mob-recom-product">
                <div class="mob-product-slider d-flex ">
                    <? foreach ($stockProduct as $product) : ?>
                        <div class="product-card products-rec mt-4">
                            <i class="fa fa-2x fa-heart-o not-liked like-button btn-to-favorite"
                               data-id="<?= $product->id; ?>" data-favorite="<?= in_array($product->id, $favorite) ?>">
                            </i>
                            <a href="<?= Url::to(['/product/view', 'slug' => $product->slug]); ?>">
                                <div class="products-img">
                                    <img src="<?= $product->getImage(); ?>" alt="">
                                </div>
                                <p><?= $product->name; ?></p>
                                <? if($product->status  == Product::STATUS_NOT_AVAILABLE): ?>
                                    <p class='not-available'>Нет в наличии</p>
                                <? endif; ?>
                                <div class="cost" style="text-align: right">
                                    <span>KZT <?= $product->price; ?></span>
                                </div>
                            </a>
                            <a class="btn-to-basket w-100" type="button" data-id="<?= $product->id; ?>"
                               data-session=<?= in_array($product->id, $session) ? 1 : 0 ?>>
                                <button class="basket-btn w-100">
                                    <img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt="">
                                    В КОРЗИНУ
                                </button>
                            </a>

                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>

    </div>

</section>