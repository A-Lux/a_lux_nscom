<?php
use yii\helpers\Html;
?>

<section id="product">
    <div class="container">
        <div class="col-lg-12 product-title">
            <h1>Маршрутизатор D-Link DIR-615A/A1A беспроводной</h1>
        </div>
        <div class="row">
            <div class="product-inner d-flex justify-content-center">
                <div class="col-xl-4 col-lg-4">
                    <div class="product-link">
                        <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                        <div class="product-sl">
                            <div class="link-image">
                                <img src="/images/image/router.svg" alt="">
                            </div>
                            <div class="link-image">
                                <img src="/images/image/router.svg" alt="">
                            </div>
                            <div class="link-image">
                                <img src="/images/image/router.svg" alt="">
                            </div>
                            <div class="link-image">
                                <img src="/images/image/router.svg" alt="">
                            </div>
                            <div class="link-image">
                                <img src="/images/image/router.svg" alt="">
                            </div>
                        </div>
                        <div class="slide-show">
                            <div class="slide1 slide-image">
                                <img src="/images/image/router.svg" alt="">
                            </div>
                            <div class="slide2  slide-image">
                                <img src="/images/image/router.svg" alt="">
                            </div>

                            <div class="slide3  slide-image">
                                <img src="/images/image/router.svg" alt="">
                            </div>
                            <div class="slide4  slide-image">
                                <img src="/images/image/router.svg" alt="">
                            </div>
                            <div class="slide5  slide-image">
                                <img src="/images/image/router.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="description col-xl-8 col-lg-8 pr-0 ">
                    <div class="description-title col-lg-12">
                        <h3>Описание</h3>
                    </div>
                    <div class="description-content col-lg-12 d-flex flex-column">
                        <p>Используя беспроводной маршрутизатор DIR-615, Вы сможете быстро организовать беспроводную
                            сеть дома и в офисе, предоставив доступ к сети Интернет компьютерам и мобильным устройствам
                            практически в любой точке (в зоне действия беспроводной сети). Маршрутизатор может выполнять
                            функции базовой станции для подключения к беспроводной сети устройств, работающих по
                            стандартам 802.11b, 802.11g и 802.11n (со скоростью беспроводного соединения до 300
                            Мбит/с).</p>
                        <h6>Артикул товара: DIR-615A/A1A</h6>
                        <span>$199</span>
                        <button class="basket-btn col-lg-4 col-md-4">
                            <img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В КОРЗИНУ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row pills-row">
            <ul class="nav nav-pills p-0 pills-product" id="pills-tab" role="tablist" style="width: 100%">
                <li class="nav-item col-lg-4 p-0 product-descrip">
                    <a class="nav-link tabs-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                       role="tab" aria-controls="pills-home" aria-selected="true"
                       style="text-align: center;">Описание</a>
                </li>
                <li class="nav-item col-lg-4 p-0 product-descrip">
                    <a class="nav-link tabs-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                       role="tab" aria-controls="pills-profile" aria-selected="false" style="text-align: center;">Характеристики</a>
                </li>
                <li class="nav-item col-lg-4 p-0 product-descrip">
                    <a class="nav-link tabs-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
                       role="tab" aria-controls="pills-contact" aria-selected="false" style="text-align: center;">Загрузки</a>
                </li>
            </ul>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade active show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="row">
                    <div class="pr-content col-xl-6">
                        <h3>Безопасное беспроводное соединение</h3>
                        <p>В маршрутизаторе реализовано множество функций для беспроводного интерфейса. Устройство
                            поддерживает несколько стандартов безопасности (WEP, WPA/WPA2), фильтрацию подключаемых
                            устройств по MAC-адресу, а также позволяет использовать технологии WPS и WMM.
                            Кроме того, устройство оборудовано кнопкой для выключения/включения Wi-Fi-сети. В случае
                            необходимости, например, уезжая из дома, Вы можете выключить беспроводную сеть
                            маршрутизатора одним нажатием на кнопку, при этом устройства, подключенные к LAN-портам
                            маршрутизатора, останутся в сети1.</p>
                        <h3>Расширенные возможности беспроводной сети</h3>
                        <p>Возможность настройки гостевой Wi-Fi-сети позволит Вам создать отдельную беспроводную сеть с
                            индивидуальными настройками безопасности и ограничением максимальной скорости. Устройства
                            гостевой сети смогут подключиться к Интернету, но будут изолированы от устройств и ресурсов
                            локальной сети маршрутизатора.
                            Функция интеллектуального распределения Wi-Fi-клиентов будет полезна для сетей, состоящих из
                            нескольких точек доступа или маршрутизаторов D-Link – настроив работу функции на каждом из
                            них, Вы обеспечите подключение клиента к точке доступа (маршрутизатору) с максимальным
                            уровнем сигнала.</p>
                    </div>
                    <div class="pr-content col-xl-6">
                        <h3>4-портовый коммутатор</h3>
                        <p>Встроенный 4-портовый коммутатор маршрутизатора позволяет подключать компьютеры, оснащенные
                            Ethernet-адаптерами, игровые консоли и другие устройства к Вашей сети.</p>
                        <h3>Безопасность</h3>
                        <p>Беспроводной маршрутизатор DIR-615 оснащен встроенным межсетевым экраном. Расширенные функции
                            безопасности позволяют минимизировать последствия действий хакеров и предотвращают вторжения
                            в Вашу сеть.
                            Встроенный сервис Яндекс.DNS обеспечивает защиту от вредоносных и мошеннических сайтов, а
                            также позволяет ограничить доступ детей к «взрослым» материалам.</p>
                        <h3>Простая настройка и обновление</h3>
                        <p>Для настройки беспроводного маршрутизатора DIR-615 используется простой и удобный встроенный
                            web-интерфейс (доступен на русском и английском языках).
                            Мастер настройки позволяет быстро перевести DIR-615 в режим маршрутизатора (для подключения
                            к проводному или беспроводному провайдеру), точки доступа, повторителя или клиента и задать
                            все необходимые настройки для работы в выбранном режиме за несколько простых шагов.</p>
                        <p>Вы легко можете обновить встроенное ПО – маршрутизатор сам находит проверенную версию ПО на
                            сервере обновлений D-Link и уведомляет пользователя о готовности установить его.</p>
                    </div>

                </div>
            </div>
            <div class="tab-pane specific-pane fade" id="pills-profile" role="tabpanel"
                 aria-labelledby="pills-profile-tab">
                <div class="row">
                    <div class="specific col-xl-6">
                        <div class="specificDiv">
                            <h2>Аппаратные характеристики</h2>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Интерфейс</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>4 порта LAN 10/100 Мбит/с <br>
                                        1 порт WAN 10/100 Мбит/с</p>
                                </div>
                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Кнопки</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>Кнопка WPS/Reset<br>
                                        Кнопка вкл./выкл. Wi-Fi<br>
                                        Кнопка вкл./выкл. питания</p>
                                </div>


                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Внешний источник питания </h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>12 В / 1 А</p>
                                </div>

                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Размеры (ШхДхВ)</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>230 × 144 × 37 мм</p>
                                </div>
                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Антенна</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>3 × 2,4 ГГц антенны <br>
                                        2 × 5 ГГц антенны</p>
                                </div>

                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Крепление</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>Отверстия для настенного <br> крепления</p>
                                </div>
                            </div>
                        </div>
                        <div class="specificDiv">
                            <h2>Аппаратные характеристики</h2>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Стандарты беспроводных сетей</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>IEEE 802.11ac/n/a 5 ГГц<br>
                                        IEEE 802.11b/g/n 2,4 ГГц </p>
                                </div>
                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-5 specific-left p-0">
                                    <h4>Скорость беспроводной передачи данных</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>до 450 Мбит/с (2,4 ГГц)<br>
                                        до 867 Мбит/с (5 ГГц)</p>
                                </div>


                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-5 specific-left p-0">
                                    <h4>Диапазон частот (приём и передача) </h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>2,4-2,4835 ГГц <br>
                                        5150-5250 ГГц</p>
                                </div>

                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>EIRP (Мощность беспроводного сигнала)</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>13,5 дБм макс. (2,4 ГГц) <br>
                                        17 дБм макс. (5 ГГц)</p>
                                </div>
                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Режимы работы</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>Беспроводной маршрутизатор <br>
                                        WDS (Four Address) <br>
                                        WDS (AP+APC)</p>
                                </div>

                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Защита беспроводной сети</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>Отверстия для настенного <br> крепления</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="specific-2 col-xl-6">
                        <div class="specificDiv-2">
                            <h2>Параметры беспроводного модуля</h2>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Стандарты беспроводных сетей</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>IEEE 802.11ac/n/a 5 ГГц<br>
                                        IEEE 802.11b/g/n 2,4 ГГц </p>
                                </div>
                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-5 specific-left p-0">
                                    <h4>Скорость беспроводной передачи данных</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>до 450 Мбит/с (2,4 ГГц)<br>
                                        до 867 Мбит/с (5 ГГц)</p>
                                </div>


                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-5 specific-left p-0">
                                    <h4>Диапазон частот (приём и передача) </h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>2,4-2,4835 ГГц <br>
                                        5150-5250 ГГц</p>
                                </div>

                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>EIRP (Мощность беспроводного сигнала)</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>13,5 дБм макс. (2,4 ГГц) <br>
                                        17 дБм макс. (5 ГГц)</p>
                                </div>
                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Режимы работы</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>Беспроводной маршрутизатор <br>
                                        WDS (Four Address) <br>
                                        WDS (AP+APC)</p>
                                </div>

                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Защита беспроводной сети</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>Отверстия для настенного <br> крепления</p>
                                </div>
                            </div>
                        </div>
                        <div class="specificDiv-2">
                            <h2>Аппаратные характеристики</h2>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Интерфейс</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>4 порта LAN 10/100 Мбит/с <br>
                                        1 порт WAN 10/100 Мбит/с</p>
                                </div>
                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Кнопки</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>Кнопка WPS/Reset<br>
                                        Кнопка вкл./выкл. Wi-Fi<br>
                                        Кнопка вкл./выкл. питания</p>
                                </div>


                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Внешний источник питания </h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>12 В / 1 А</p>
                                </div>

                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Размеры (ШхДхВ)</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>230 × 144 × 37 мм</p>
                                </div>
                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Антенна</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>3 × 2,4 ГГц антенны <br>
                                        2 × 5 ГГц антенны</p>
                                </div>

                            </div>
                            <div class="specific-item d-flex justify-content-between col-xl-12 p-0">
                                <div class="col-lg-6 specific-left p-0">
                                    <h4>Крепление</h4>
                                </div>
                                <div class="col-lg-6 specific-right p-0">
                                    <p>Отверстия для настенного <br> крепления</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">...</div>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-12 product-title recom">
            <h1>Рекомендованные товары</h1>
        </div>
        <div class="recom-product d-flex justify-content-between">
            <div class="product-card product-link products-rec">
                <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                <a href="product.html">
                    <div class="products-img">
                        <img src="/images/image/router.png" alt="">
                    </div>
                    <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                    <div class="cost" style="text-align: right">
                        <span>$199</span>
                    </div>
                </a>
                <div class="basket d-flex align-items-center justify-content-center">
                    <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В КОРЗИНУ</a>
                </div>
            </div>
            <div class="product-card products-rec">
                <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                <a href="product.html">
                    <div class="products-img">
                        <img src="/images/image/router.png" alt="">
                    </div>
                    <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                    <div class="cost" style="text-align: right">
                        <span>$199</span>
                    </div>
                </a>
                <div class="basket d-flex align-items-center justify-content-center">
                    <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В КОРЗИНУ</a>
                </div>

            </div>
            <div class="product-card products-rec">
                <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                <a href="product.html">
                    <div class="products-img">
                        <img src="/images/image/router.png" alt="">
                    </div>
                    <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                    <div class="cost" style="text-align: right">
                        <span>$199</span>
                    </div>
                </a>
                <div class="basket d-flex align-items-center justify-content-center">
                    <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В КОРЗИНУ</a>
                </div>

            </div>

            <div class="product-card products-rec">
                <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                <a href="product.html">
                    <div class="products-img">
                        <img src="/images/image/router.png" alt="">
                    </div>
                    <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                    <div class="cost" style="text-align: right">
                        <span>$199</span>
                    </div>
                </a>
                <div class="basket d-flex align-items-center justify-content-center">
                    <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В КОРЗИНУ</a>
                </div>

            </div>
            <div class="product-card products-rec">
                <i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i>
                <a href="product.html">
                    <div class="products-img">
                        <img src="/images/image/router.png" alt="">
                    </div>
                    <p>Маршрутизатор D-Link DIR-615A/A1A беспроводной</p>
                    <div class="cost" style="text-align: right">
                        <span>$199</span>
                    </div>
                </a>
                <div class="basket d-flex align-items-center justify-content-center">
                    <a href=""><img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt=""> В КОРЗИНУ</a>
                </div>

            </div>
        </div>
    </div>

</section>
