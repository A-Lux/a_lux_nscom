<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $product common\models\Product */
/* @var $favorite   */
/* @var $session   */

?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css"/>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.umd.js"></script>

<section id="product">
    <div class="container">
        <div class="col-lg-12 product-title">
            <h1><?= $product->name; ?></h1>
        </div>
        <div class="row">
            <div class="product-inner d-flex justify-content-center">
                <div class="col-xl-4 col-lg-4">
                    <div class="product-link">
                        <i class="fa fa-2x fa-heart-o not-liked like-button btn-to-favorite"
                           data-id="<?= $product->id;?>" data-favorite="<?= in_array($product->id, $favorite) ?>" style="z-index: 99999999">
                        </i>
                        <div class="product-sl">
                            <div class="link-image">
                                <a data-fancybox="gallery" data-src="<?= $product->getImage(); ?>">
                                <img src="<?= $product->getImage(); ?>" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="slide-show">
                            <div class="slide1 slide-image">
                                <a data-fancybox="gallery" data-src="<?= $product->getImage(); ?>">
                                <img src="<?= $product->getImage(); ?>" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="description col-xl-8 col-lg-8 pr-0 ">
                    <div class="description-title col-lg-12">
                        <h3>Описание</h3>
                    </div>
                    <div class="description-content col-lg-12 d-flex flex-column">
                        <?= $product->content; ?>
                        <h6>Артикул товара: <?= $product->code; ?></h6>
                        <span>KZT <?= $product->price; ?></span>
                        <button class="basket-btn col-lg-4 col-md-4 btn-to-basket" data-id="<?=$product->id;?>"
                                data-session=<?= in_array($product->id, $session) ? 1 : 0 ?>>
                            <img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt="">
                            В КОРЗИНУ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row pills-row">
            <ul class="nav nav-pills p-0 pills-product" id="pills-tab" role="tablist" style="width: 100%">
                <li class="nav-item col-lg-4 p-0 product-descrip">
                    <a class="nav-link tabs-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                       role="tab" aria-controls="pills-home" aria-selected="true"
                       style="text-align: center;">Описание</a>
                </li>
                <li class="nav-item col-lg-4 p-0 product-descrip">
                    <a class="nav-link tabs-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                       role="tab" aria-controls="pills-profile" aria-selected="false" style="text-align: center;">Характеристики</a>
                </li>
                <li class="nav-item col-lg-4 p-0 product-descrip">
                    <a class="nav-link tabs-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
                       role="tab" aria-controls="pills-contact" aria-selected="false" style="text-align: center;">Загрузки</a>
                </li>
            </ul>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade active show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="row">
                    <? foreach($product->description as $description): ?>
                    <div class="pr-content col-xl-6">
                        <h3><?= $description->title; ?></h3>
                        <?= $description->content; ?>
                    </div>
                    <? endforeach;?>

                </div>
            </div>
            <div class="tab-pane specific-pane fade" id="pills-profile" role="tabpanel"
                 aria-labelledby="pills-profile-tab">
                <div class="row">
                    <? foreach ($product->specification as $specification): ?>
                        <div class="specific col-xl-6">
                            <div class="specificDiv">
                                <h2><?= $specification->title; ?></h2>
                                <?= $specification->content; ?>
                            </div>
                        </div>
                    <? endforeach;?>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">...</div>
        </div>
    </div>
    <div class="container">
        <?= $this->render('_blocks/_recommended', [
                'recommended'   => $recommended,
                'recommend'     => $recommend,
                'favorite'      => $favorite,
                'session'       => $session,
        ]) ?>
    </div>

</section>


