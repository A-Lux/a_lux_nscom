<?php

use yii\web\View;
use yii\helpers\Url;
use common\models\Product;
use yii\helpers\Html;

    /* @var $recommended    Product  */
    /* @var $recommend    Product  */
    /* @var $favorite  */
    /* @var $session  */

?>

<div class="col-lg-12 product-title recom">
    <h1>Рекомендованные товары</h1>
</div>
<div class="recom-product d-flex justify-content-between">

    <? foreach($recommended as $product): ?>
        <div class="product-card products-rec mt-4">
            <i class="fa fa-2x fa-heart-o not-liked like-button btn-to-favorite"
               data-id="<?= $product->id;?>" data-favorite="<?= in_array($product->id, $favorite) ?>">
            </i>
            <a href="<?= Url::to(['view', 'slug' => $product->slug]); ?>">
                <div class="products-img">
                    <img src="<?= $product->getImage(); ?>" alt="">
                </div>
                <p><?= $product->name; ?></p>
                <div class="cost" style="text-align: right">
                    <span>KZT <?= $product->price; ?></span>
                </div>
            </a>
            <a class="btn-to-basket w-100" type="button" data-id="<?= $product->id; ?>" data-session=<?= in_array($product->id, $session) ? 1 : 0 ?>>
                <button class="basket-btn w-100">
                    <img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt="">
                    В КОРЗИНУ
                </button>
            </a>

        </div>
    <? endforeach; ?>

    <? foreach($recommend as $product): ?>
        <div class="product-card products-rec mt-4">
            <i class="fa fa-2x fa-heart-o not-liked like-button btn-to-favorite"
               data-id="<?= $product->id;?>" data-favorite="<?= in_array($product->id, $favorite) ?>">
            </i>
            <a href="<?= Url::to(['view', 'slug' => $product->slug]); ?>">
                <div class="products-img">
                    <img src="<?= $product->getImage(); ?>" alt="">
                </div>
                <p><?= $product->name; ?></p>
                <div class="cost" style="text-align: right">
                    <span>KZT <?= $product->price; ?></span>
                </div>
            </a>
            <a class="btn-to-basket w-100" type="button" data-id="<?= $product->id; ?>" data-session=<?= in_array($product->id, $session) ? 1 : 0 ?>>
                <button class="basket-btn w-100">
                    <img src="/images/icons/%D0%BA%D0%BE%D1%80%D0%B7%D0%B8%D0%BD%D0%B0.png" alt="">
                    В КОРЗИНУ
                </button>
            </a>

        </div>
    <? endforeach; ?>
</div>
