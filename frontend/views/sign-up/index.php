<?php

    use yii\web\View;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use frontend\models\SignupForm;
    use common\models\Client;


    /* @var $this View */
    /* @var $model  SignupForm*/
    /* @var $user  \frontend\models\ClientForm*/
    /* @var $form ActiveForm */

?>
<section class="registration">
    <div class="container">
        <div class="col-xl-12 p-0">
            <div class="reg-title">
                <h1>Регистрация</h1>
            </div>
        </div>
        <? $form    =   ActiveForm::begin([
            'id'        =>  'sign-up-client-form',
        ]); ?>
        <?= $form->errorSummary([$model, $user]) ?>
            <div class="row reg-row">

                <div class="col-xl-4">
                    <div class="reg-form">
                        <h2>ФИО</h2>
                        <?= $form->field($user, 'username')->textInput([
                            'maxlength'     => true,
                            'autofocus'     => true,
                            'placeholder'   => 'Иван Иванович Иванов',
                        ])->label(false) ?>
                    </div>
                    <div class="reg-form">
                        <h2>Регион (страна)</h2>
                        <?= $form->field($user, 'region')->textInput([
                                'placeholder'    => 'Россия',
                        ])->label(false) ?>
                    </div>
                    <div class="reg-form">
                        <h2>Моб. Телефон</h2>
                        <?= $form->field($user, 'mobile_phone')->textInput([
                                'placeholder'   => '+7 700 123 45 67',
                        ])->label(false) ?>

                        <script>
                            $('input[name="ClientForm[mobile_phone]"]').inputmask("+7(999) 999-9999");
                        </script>
                    </div>
                </div>

                <div class="col-xl-4">
                    <div class="reg-form compain">
                        <h2>Название компании</h2>
                        <?= $form->field($user, 'company')->textInput([
                                'placeholder'   => 'Компания',
                        ])->label(false) ?>
                    </div>
                    <div class="reg-form">
                        <h2>Город</h2>
                        <?= $form->field($user, 'city')->textInput([
                                'placeholder'   => 'Москва',
                        ])->label(false) ?>
                    </div>
                    <div class="reg-form">
                        <h2>E-mail</h2>
                        <?= $form->field($model, 'email')->textInput([
                                'placeholder'   => 'example@gmail.com',
//                                'email'         => 'email',
                        ])->label(false) ?>
                    </div>

                </div>
                <div class="col-xl-4">
                    <div class="reg-form">
                        <h2>Статус</h2>
                        <?= $form->field($user, 'status')->dropDownList(Client::statusDescription() , [
                            'class'   => 'status-client',
                        ])->label(false) ?>
<!--                        <select name="" id="partnersss">-->
<!--                            <option value="clients">Клиент</option>-->
<!--                            <option value="partners">Партнер</option>-->
<!--                        </select>-->
                    </div>
                    <div class="reg-form">
                        <h2>Рабочий телефон</h2>
                        <?= $form->field($user, 'work_phone')->textInput([
                                'placeholder'   => '+7 700 123 45 67',
                        ])->label(false) ?>
                        <script>
                            $('input[name="ClientForm[work_phone]"]').inputmask("+7(999) 999-9999");
                        </script>
                    </div>
                    <div class="reg-form password-form">
                        <h2>Пароль</h2>
                        <?= $form->field($model, 'password')->passwordInput([
                                'id'    => 'password-field',
                                'class' => 'password-input form-control',
                        ])->label(false) ?>
                        <span toggle="#password-field"
                              class="fa fa-fw fa-eye field-icon toggle-password password-span"></span>
                    </div>
                </div>
                <div class="col-xl-12 col-12">
                    <div class="reg-form-btn d-flex">
                        <div class="col-xl-5 col-12 col-md-6">
                            <div class="reg-button">
                                <?= Html::submitButton('Зарегистрироваться', ['class' => '']) ?>
                            </div>
                        </div>

                        <div class="col-xl-6 col-12 col-md-6">
                            <div class="author-btn">
                                <a href="/login" class="author-button">Авторизоваться</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        <? ActiveForm::end(); ?>
    </div>
</section>
