<?php

namespace frontend\controllers;

use common\models\Client;
use common\models\Product;
use frontend\models\Basket;

class BasketController extends FrontendController
{
    public function actionIndex()
    {
        $products       = Basket::getProducts($_SESSION['basket']);
        $total          = Basket::getPrice($_SESSION['basket']);
        $user           = Client::getOne();

        return $this->render('index', [
            'products'  => $products,
            'total'     => $total,
            'user'      => $user,
        ]);
    }

    public function actionAddProduct()
    {
        $id = $_GET['id'];

        $item   = Basket::getAdd($id);

//        print_r("<pre>");
//        print_r($item); die;

        if(!in_array($item, $_SESSION['basket'])){
            if(array_push($_SESSION['basket'], $item)){
                $count  = count($_SESSION['basket']);
                $response = ['status' => 1, 'message' => 'Товар добавлен в корзину', 'count' => $count];

                return json_encode($response);
            }else{
                $count  = count($_SESSION['basket']);
                $response = ['status' => 0, 'message' => 'Товар не получилось добавить в корзину!', 'count' => $count];

                return json_encode($response);
            }
        }else {
            $response = ['status' => 0, 'message' => 'Товар уже в корзине!'];
            \Yii::$app->response->statusCode = 405;

            return json_encode($response);
        }
    }

    public function actionDownClicked()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $count      = Basket::downProduct($_GET['id']);

        $total      = Basket::getPrice($_SESSION['basket']);
        $array      = ['countProduct' => $count, 'total' => $total];

        return $array;
    }

    public function actionUpClicked()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $count      = Basket::upProduct($_GET['id']);

        $total      = Basket::getPrice($_SESSION['basket']);
        $array      = ['countProduct' => $count, 'total' => $total];

        return $array;
    }

    public function actionCountChanged()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Basket::countProduct($_GET['id'], $_GET['value']);

        $total      = Basket::getPrice($_SESSION['basket']);
        $array      = ['total' => $total];

        return $array;
    }

    public function actionDeleteProduct()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $status = Basket::deleteProduct($_GET['id']);

        if($status == true){
            $count  = count($_SESSION['basket']);
            $response   = ['status' => 1, 'message' => \Yii::t('main', 'Вы успешно удалили данный Товар'), 'count' => $count];
            return $response;
        }else{
            $response = ['status' => 0, 'message' => 'Ошибка сервера. Попробуйте позднее!'];
            \Yii::$app->response->statusCode = 405;
            return $response;
        }
    }

    public function actionTest()
    {
       unset($_SESSION['basket']); die;
        print_r("<pre>");
        print_r($_SESSION['basket']);die;
    }
}