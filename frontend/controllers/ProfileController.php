<?php

namespace frontend\controllers;

use common\models\Client;
use common\models\Orders;
use common\models\User;
use frontend\models\Basket;
use common\models\FavoritesProduct;
use common\modules\user\models\BaseUser;
use frontend\models\ClientForm;
use frontend\models\ProfileForm;
use frontend\models\SecureForm;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Profile controller
 */
class ProfileController extends FrontendController
{
    /**
     * @var array
     */
    protected $roles = ['user', 'operator', 'manager', 'admin', 'developer'];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'     => true,
                        'roles'     => $this->roles,
                    ],
                    [
                        'allow'             => true,
                        'roles'             => ['@'],
                        'matchCallback'     => function ($rule, $action) {
                            if (!\Yii::$app->user->isGuest) {
                                \Yii::$app->user->logout();
                            }
                            throw new ForbiddenHttpException('У Вас нет доступа к личному кабинету!');
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays index Profile.
     * @return mixed
     *
     */
    public function actionIndex()
    {
//        \Yii::$app->session->setFlash('success', 'Вы успешно вошли в свой аккаунт');
        if(\Yii::$app->user->isGuest){
            \Yii::$app->session->setFlash('Вы либо не зарегистрировались либо Ваш аккаунт не подтвержден!');
            return $this->redirect('/');
        }

        $model              = User::getOne();
        $user               = Client::getClient($model);
        $orders             = Orders::getUserOrders();

        $favoriteProduct    = FavoritesProduct::getAll($model->id);
        $favorite           = FavoritesProduct::allProduct();
        $session            = Basket::getSession();

        return $this->render('index',[
            'model'             => $model,
            'user'              => $user,
            'orders'            => $orders,
            'favorite'          => $favorite,
            'session'           => $session,
            'favoriteProduct'   => $favoriteProduct,
        ]);
    }

    public function actionUpdateUser()
    {
        $model = new ProfileForm();

        if($model->load(\Yii::$app->request->post()) && $model->updateClient()){
            \Yii::$app->session->setFlash('success', 'Вы успешно обновили свои данные!');
            return $this->redirect('/profile');
        }
    }

    /**
     * Смена пароля
     * @return mixed
     */
    public function actionSecure()
    {
        $model = new SecureForm();

//        print_r("<pre>");
//        print_r($model->load(\Yii::$app->request->post()));die;

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->updatePassword()) {
                $response   = ['status' => 1, 'message' => \Yii::t('main-message', 'Пароль изменён')];
                return json_encode($response);
            } else {
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = ['status' => 0, 'message' => $message];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }
    }
}