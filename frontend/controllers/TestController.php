<?php

namespace frontend\controllers;

use common\models\Catalog;
use common\models\Documents;
use common\models\FilterEntity;
use common\models\FilterValue;
use common\models\OrderedProducts;
use common\models\Orders;
use common\models\Product;
use frontend\models\RecommendedProduct;
use yii\helpers\ArrayHelper;

class TestController extends FrontendController
{

    public function actionEmail()
    {
        $order  = Orders::findOne(['id' => 1]);

        $orderedProducts    = OrderedProducts::findAll(['order_id' => $order->id]);

        $this->sendEmail($order, $orderedProducts);

        return 1;

    }

    protected function sendEmail($order, $orderedProducts)
    {
        return \Yii::$app
            ->mailer
            ->compose(
                ['html' => 'orderUser-html', 'text' => 'orderUser-text'],
                ['order' => $order, 'orderedProducts' => $orderedProducts]
            )
            ->setFrom([\Yii::$app->params['supportEmail'] => 'NSCOM robot'])
            ->setTo($order->email)
            ->setSubject('Вы оформили заказ на сайте NSCOM')
            ->send();
    }

    public function actionRecommend()
    {
        $product    = Product::findOne(['id' => 2]);
        $model  = RecommendedProduct::getProduct($product);

        print_r("<pre>");
        print_r($model);die;
    }

    public function actionDocuments()
    {
        $documents  = Documents::getAll();

        $value  = ArrayHelper::getValue($documents, 'Договор оферты');

        print_r("<pre>");
        print_r($value);die;
    }

    public function actionIndex()
    {
        $array = [];

        foreach($_SESSION['basket'] as $value) {
            $array[]    = $value['id'];
        }
        print_r("<pre>");
        print_r($_SESSION['basket']);die;
    }

    public function actionCatalog()
    {
        $catalogOne    = Catalog::findOne(['id' => 5]);

        $tree = $this->get_tree($catalogOne->id);

        $product    = $this->getFilterAttribute($tree);

        print_r("<pre>");
        print_r($tree);die;
    }

    public function getFilterEntity($product)
    {
        $array = [];
        foreach ($product as $item){
            $array[] = $item->id;
        }

        $arr = implode(',', $array);

        $filter = FilterEntity::find()->where('product_id in (' . $arr . ')')->all();

        return $filter;
    }

    public function getFilterValue($tree)
    {
        $entity = $this->getFilterEntity($tree);

        $array  = [];
        $temp   = [];
        foreach ($entity as $value){
            if(!in_array($value->value->id, $temp)) {
                $temp[]     = $value->value->id;
                $array[$value->value->id]    = $value->value;
            }
        }

        return $array;
    }

    public function getFilterAttribute($tree)
    {
        $filterValue  = $this->getFilterValue($tree);
        $array = [];
        $temp   = [];

        foreach ($filterValue as $value){
            if(!in_array($value->attribute0->id, $temp)) {
                $temp[] = $value->attribute0->id;
                $array[$value->attribute0->id] = $value->attribute0;
            }
        }

        return $array;
    }

    public function get_tree($id)
    {
        $result = $this->getCat();

        $arr = [];
        foreach ($result as $val1) {
            foreach ($val1 as $val2) {
                if ($val2->id == $id) {
                    $arr[] = $val2->id;
                    if (!empty($result[$val2->id])) {
                        foreach ($result[$val2->id] as $val3) {
                            $arr[] = $val3->id;
                            if (!empty($result[$val3->id])) {
                                foreach ($result[$val3->id] as $val4) {
                                    $arr[] = $val4->id;
                                    if (!empty($result[$val4->id])) {
                                        foreach ($result[$val4->id] as $val5) {
                                            $arr[] = $val5->id;
                                            if (!empty($result[$val5->id])) {
                                                foreach ($result[$val5->id] as $val6) {
                                                    $arr[] = $val6->id;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        if ($arr) {
            $arr = Product::find()->where('category_id in (' . implode(',', $arr) . ')')->all();
        }

        return $arr;
    }

    public function getCat()
    {
        $catalog    = Catalog::findAll(['status' => Catalog::STATUS_ACTIVE]);
        $array = [];
        foreach ($catalog as $category){
            if(empty($array[$category['parent_id']])){
                $array[$category['parent_id']] = [];
            }
            $array[$category['parent_id']][]    = $category;
        }

        return $array;
    }

    public function getCatalog($categories, $catalogItems = [])
    {
        foreach ($categories->child as $category){
            $catalogItems[]    = $category->id;
        }

        foreach ($categories->child as $category){
            if($category->child){
                return $this->getCatalog($category, $catalogItems);
            }
        }

        return $catalogItems;
    }

    public function drawCatalog($catalogItems)
    {
        $html   = [];

        foreach ($catalogItems as $item){

            $html[] = $item->name;

            if(!empty($item->child)){
                $html[] = $this->drawCatalogItems($catalogItems, $item->id);
            }
        }

        return $html;

    }

//    public function actionIndex()
//    {
//        $catalog = Catalog::find()->where(['status' => Catalog::STATUS_ACTIVE])->all();
//
//        $result = $this->getCatalogItems($catalog);
//
//        $tree   = $this->drawCatalogItems($result);
//
//        print_r("<pre>");
//        print_r($tree);die;
//    }

    public function getCatalogItems($categories)
    {
        $catalogItems = [];
        foreach ($categories as $category){
            $catalogItems[$category->id]    = $category;
        }

        foreach ($categories as $category){
            if(isset($catalogItems[$category->parent_id])){
                $catalogItems[$category->parent_id]->child[] = $category;
            }
        }

        return $catalogItems;
    }

    public function drawCatalogItems($catalogItems, $parent_id = null)
    {
        $html   = "";

        foreach ($catalogItems as $item){
            if($item->parent_id !== $parent_id){
                continue;
            }

            $html .= '<p>' . $item->name . '</p>';

            if(!empty($item->child)){
                $html .= '<ul>' . $this->drawCatalogItems($catalogItems, $item->id) . '</ul>';
            }
        }

        return $html;

    }
}