<?php

namespace frontend\controllers;

use common\models\Menu;
use common\models\MultilingualActiveRecord;
use common\models\News;
use frontend\models\search\NewsSearch;
use yii\web\NotFoundHttpException;

/**
 * News controller
 */
class NewsController extends FrontendController
{
    /**
     * news page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel    = new NewsSearch();
        $dataProvider   = $searchModel->search(\Yii::$app->request->queryParams);
        $menu           = Menu::findOne(['url' => '/news']);

        return $this->render('index', [
            'searchModel'       => $searchModel,
            'dataProvider'      => $dataProvider,
            'menu'              => $menu,
        ]);
    }

    /**
     * Displays a single model.
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {
        $model      = $this->findModel($slug);
        $this->setMeta($model->metaName, $model->metaDesc, $model->metaKey);

        $latestNews = News::getLatestModel($model);
        $menu       = Menu::findOne(['url' => '/news']);

        return $this->render('view', [
            'model'         => $model,
            'latestNews'    => $latestNews,
            'menu'          => $menu,
        ]);
    }

    /**
     * Finds the Brand model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = News::find()->where(['slug' => $slug])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}