<?php

namespace frontend\controllers;

use common\models\FavoritesProduct;
use common\models\Product;
use frontend\models\Basket;

class SearchController extends FrontendController
{
    public function actionIndex()
    {
        $text = $_GET['text'];
        $favorite       = FavoritesProduct::allProduct();
        $session        = Basket::getSession();

        $products = Product::find()->where("name LIKE '%$text%'")->all();

        return $this->render('index', compact('products', 'text', 'favorite', 'session'));
    }
}