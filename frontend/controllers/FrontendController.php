<?php

namespace frontend\controllers;

use common\models\Catalog;
use common\models\City;
use common\models\Contact;
use common\models\Documents;
use common\models\Language;
use common\models\Logo;
use common\models\Menu;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;

class FrontendController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function init()
    {
//        $menu           = Menu::getOne(\Yii::$app->request->url);
//        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);


        $menuHeader     = Menu::getHeader();
        $menuFooter     = Menu::getFooter();
        $language       = Language::getAll();
        $catalog        = Catalog::getAll();
        $documents      = Documents::getAll();
        $logoHeader     = Logo::getHeader();
        $logoFooter     = Logo::getFooter();
        $contact        = Contact::getOne();
        $city           = City::getAll();

        \Yii::$app->view->params['menuHeader']          = $menuHeader;
        \Yii::$app->view->params['menuFooter']          = $menuFooter;
        \Yii::$app->view->params['language']            = $language;
        \Yii::$app->view->params['catalog']             = $catalog;
        \Yii::$app->view->params['documents']           = $documents;
        \Yii::$app->view->params['logoHeader']          = $logoHeader;
        \Yii::$app->view->params['logoFooter']          = $logoFooter;
        \Yii::$app->view->params['contact']             = $contact;
        \Yii::$app->view->params['city']                = $city;

//        \Yii::$app->view->params['menu']                = $menu;

        if (session_status() != PHP_SESSION_ACTIVE) {
            session_start();
        }

        if(!isset($_SESSION['basket'])) {
            $_SESSION['basket'] = [];
        }

        parent::init();
    }

    protected function setMeta($title = null, $description = null, $keywords = null)
    {
        $this->view->title      =   $title;
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
    }
}
