<?php

namespace frontend\controllers;

use common\models\FavoritesProduct;
use common\models\MultilingualActiveRecord;
use common\models\Product;
use common\models\ProductsFeatured;
use frontend\models\Basket;
use frontend\models\RecommendedProduct;
use yii\web\NotFoundHttpException;

class ProductController extends FrontendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays a single model.
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {
        $product        = $this->findModel($slug);
        $favorite       = FavoritesProduct::allProduct();

        $recommended    = ProductsFeatured::getRecommendedProduct($product);
        $recommend      = RecommendedProduct::getProduct($product);
        $session        = Basket::getSession();


        return $this->render('view', [
            'product'       => $product,
            'favorite'      => $favorite,
            'recommended'   => $recommended,
            'recommend'     => $recommend,
            'session'       => $session,
        ]);
    }

    /**
     * Finds the Brand model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Product::find()->where(['slug' => $slug])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}