<?php

namespace frontend\controllers;


use common\models\Client;
use common\models\Orders;
use common\models\Requisites;
use common\models\UserRequisites;
use frontend\models\RequisiteForm;
use kartik\mpdf\Pdf;
use yii\db\Exception;

/**
 * Payment Method controller
 */
class PaymentMethodController extends FrontendController
{
    public function actionIndex($id)
    {
        $order  = Orders::findOne(['id' => $id]);

        return $this->render("index", compact('order'));
    }

    public function actionType($id, $type)
    {
        $order   = Orders::findOne(['id' => $id]);

        if($type == 1){
            $order->typePay     = $type;
            $order->save();

            $order->orderEmail($order);
            \Yii::$app->session->setFlash('success', 'Благодарим Вас за покупку! Скоро менеджеры свяжутся с Вами!');
            return $this->redirect('/');
        }elseif ($type == 2){
            $order->typePay     = $type;
            $order->save();

            return $this->redirect(['requisite', 'id' => $id]);
        }elseif($type == 3){
            $order->typePay     = $type;
            $order->save();

            $order->orderEmail($order);

            \Yii::$app->session->setFlash('success', 'Благодарим Вас за покупку! Скоро менеджеры свяжутся с Вами!');
            return $this->redirect('/');
        }else{
            \Yii::$app->session->setFlash('error', 'Ошибка сервера! Попробуйте позднее');
            return $this->redirect('/');
        }
    }

    public function actionRequisite($id)
    {
        $order      = Orders::findOne(['id' => $id]);
        $model      = new RequisiteForm();
        $user       = Client::getOne();
        $requisite  = UserRequisites::getOne();

        if(\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())){

            try {
                if ($requisite = $model->requisite()) {
                    return $this->redirect(['pdf', 'order_id' => $id, 'requisite_id' => $requisite->id]);
                } else {
                    $errors = $model->firstErrors;
                    \Yii::$app->session->setFlash('error', $errors);
                    throw new Exception(reset($errors));
                }
            }catch (\Exception $e){
                \Yii::$app->session->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('requisite', compact('order', 'model', 'user', 'requisite'));
    }

    public function actionPdf($order_id, $requisite_id)
    {
        $order      = Orders::findOne(['id' => $order_id]);
        $requisite  = UserRequisites::findOne(['id' => $requisite_id]);
        $order->orderEmail($order);

        $content    = $this->renderPartial('pdf', compact('order', 'requisite'));

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,

            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting

            // set mPDF properties on the fly
            'methods' => [
                'SetTitle' => 'Microsoft Word - NSCom Proforma Invoice Sample',]

        ]);

        return $pdf->render();
    }

    public function actionTest()
    {
        $order      = Orders::findOne(['id' => 1]);
        $requisite  = UserRequisites::findOne(['id' => 1]);
        $content = $this->renderPartial('pdf', compact('order', 'requisite'));

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'filename'  => 'order_' . $order->order_id . '.pdf',
//            'tempPath'  => \Yii::getAlias('@frontend/web/uploads/files/orders'),
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // stream to browser inline
            'destination' => Pdf::DEST_FILE,
            'content' => $content,

            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting

            // set mPDF properties on the fly
            'methods' => [
                'SetTitle' => 'NSCOM Proforma Invoice Sample',]

        ]);

        file_put_contents(\Yii::getAlias('@frontend') . '/web/uploads/files/orders/order_' . $order->order_id. '.pdf',
            $pdf->output($content,'order_' . $order->order_id. '.pdf', 'S')
    );
//        $pdf->saveAs('/frontend/web/uploads/files/orders/order_' . $order->order_id. '.pdf');

        return 1;
    }

    
}