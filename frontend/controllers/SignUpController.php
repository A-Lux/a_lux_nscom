<?php

namespace frontend\controllers;

use common\models\Client;
use common\modules\user\models\VerifyEmailForm;
use frontend\models\ClientForm;
use frontend\models\SignupForm;
use yii\db\Exception;
use yii\web\BadRequestHttpException;

/**
 * SignUp controller
 */
class SignUpController extends FrontendController
{

    public function actionIndex()
    {
        $model  = new SignupForm();
        $user   = new ClientForm();

        if(\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post()) && $user->load(\Yii::$app->request->post())){

            try {
                if ($client = $model->signup()) {
                    if ($user->signClient($client)) {
                        \Yii::$app->session->setFlash('success', 'Спасибо за регистрацию.
                        Пожалуйста, проверьте ваш почтовый ящик для подтверждения по электронной почте.');
                        return $this->redirect('/');
                    } else {
                        $errors = $user->firstErrors;
                        throw new Exception(reset($errors));
                    }
                } else {
                    $errors = $model->firstErrors;
                    throw new Exception(reset($errors));
                }
            }catch (\Exception $e){
                \Yii::$app->session->addFlash('error', $e->getMessage());
            }

        }

        return $this->render('index', compact('model', 'user'));
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return mixed
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (\InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (\Yii::$app->user->login($user)) {
                \Yii::$app->session->setFlash('success', 'Ваш аккаунт был подтвержден!');
                return $this->goHome();
            }
        }

        \Yii::$app->session->setFlash('error', 'К сожалению, мы не можем подтвердить ваш аккаунт с помощью предоставленного токена.');
        return $this->goHome();
    }
}