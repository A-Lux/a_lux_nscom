<?php

namespace frontend\controllers;

use common\models\Catalog;
use common\models\FavoritesProduct;
use common\models\FilterEntity;
use common\models\Product;
use frontend\models\TreeCatalog;
use yii\web\NotFoundHttpException;

class CatalogController extends FrontendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionView($slug)
    {
        $model  = Catalog::findOne(['slug' => $slug]);
        $this->setMeta($model->metaName, $model->metaDesc, $model->metaKey);

        $product        = $this->get_tree($model->id);
        $filterValue    = $this->getFilterValue($model->id);
        $filterAttr     = $this->getFilterAttribute($model->id);

        $session        = $this->getSession();
        $favorite       = FavoritesProduct::allProduct();

        return $this->render('view', [
            'model'         => $model,
            'product'       => $product,
            'filterValue'   => $filterValue,
            'filterAttr'    => $filterAttr,
            'session'       => $session,
            'favorite'      => $favorite,
        ]);
    }

    public function getSession()
    {
        $array  = [];
        foreach ($_SESSION['basket'] as $value){
            $array[] = $value['id'];
        }

        return $array;
    }

    private function getFilterEntity($id)
    {
        $tree   = $this->get_tree($id);
        if (null == $tree) {
            return $tree;
        }

        $array  = [];
        foreach ($tree as $item){
            $array[] = $item->id;
        }

        $arr    = implode(',', $array);
        $filter = FilterEntity::find()->where('product_id in (' . $arr . ')')->all();

        return $filter;
    }

    public function getFilterValue($id)
    {
        $entity = $this->getFilterEntity($id);

        $array  = [];
        $temp   = [];
        foreach ($entity as $value){
            if(!in_array($value->value->id, $temp)) {
                $temp[]                     = $value->value->id;
                $array[$value->value->id]   = $value->value;
            }
        }

        return $array;
    }

    public function getFilterAttribute($id)
    {
        $filterValue    = $this->getFilterValue($id);
        $array          = [];
        $temp           = [];

        foreach ($filterValue as $value){
            if(!in_array($value->attribute0->id, $temp)) {
                $temp[]                         = $value->attribute0->id;
                $array[$value->attribute0->id]  = $value->attribute0;
            }
        }

        return $array;
    }

    public function get_tree($id)
    {
        $result = $this->getCat();

        $arr = [];
        foreach ($result as $val1) {
            foreach ($val1 as $val2) {
                if ($val2->id == $id) {
                    $arr[] = $val2->id;
                    if (!empty($result[$val2->id])) {
                        foreach ($result[$val2->id] as $val3) {
                            $arr[] = $val3->id;
                            if (!empty($result[$val3->id])) {
                                foreach ($result[$val3->id] as $val4) {
                                    $arr[] = $val4->id;
                                    if (!empty($result[$val4->id])) {
                                        foreach ($result[$val4->id] as $val5) {
                                            $arr[] = $val5->id;
                                            if (!empty($result[$val5->id])) {
                                                foreach ($result[$val5->id] as $val6) {
                                                    $arr[] = $val6->id;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        if ($arr) {
            $arr = Product::find()->where('category_id in (' . implode(',', $arr) . ')')->all();
        }

        return $arr;
    }

    public function getCat()
    {
        $catalog    = Catalog::findAll(['status' => Catalog::STATUS_ACTIVE]);
        $array = [];
        foreach ($catalog as $category){
            if(empty($array[$category['parent_id']])){
                $array[$category['parent_id']] = [];
            }
            $array[$category['parent_id']][]    = $category;
        }

        return $array;
    }
}