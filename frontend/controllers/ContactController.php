<?php

namespace frontend\controllers;

use common\models\Contact;
use common\models\Menu;
use frontend\models\search\RequisitesSearch;

/**
 * Contact controller
 */
class ContactController extends FrontendController
{
    public function actionIndex()
    {
        $contact            = Contact::getOne();
        $searchRequisites   = new RequisitesSearch();
        $dataProvider       = $searchRequisites->search(\Yii::$app->request->queryParams);
        $menu               = Menu::findOne(['url' => '/contact']);

        return $this->render('index', [
            'contact'           => $contact,
            'searchRequisites'  => $searchRequisites,
            'dataProvider'      => $dataProvider,
            'menu'              => $menu,
        ]);
    }
}