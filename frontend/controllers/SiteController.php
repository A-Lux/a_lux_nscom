<?php
namespace frontend\controllers;

use common\models\Banner;
use common\models\BannerMobile;
use common\models\BannerMobileProduct;
use common\models\BannerProduct;
use common\models\Brand;
use common\models\Catalog;
use common\models\FavoritesProduct;
use common\models\Product;
use frontend\models\Basket;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $banners        = Banner::getAll();
        $catalog        = Catalog::getMainCatalog();
        $brands         = Brand::getMain();
        $hitProduct     = Product::getHit();
        $stockProduct   = Product::getStock();
        $favorite       = FavoritesProduct::allProduct();
        $session        = Basket::getSession();
        $bannersMobile  = BannerMobile::getAll();

        return $this->render('index', [
            'banners'           => $banners,
            'catalog'           => $catalog,
            'brands'            => $brands,
            'hitProduct'        => $hitProduct,
            'stockProduct'      => $stockProduct,
            'favorite'          => $favorite,
            'session'           => $session,
            'bannersMobile'      => $bannersMobile,
        ]);
    }

    public function actionBanner($id)
    {
        $banner     = Banner::findOne(['id' => $id]);
        $products   = BannerProduct::getSearchProducts($banner);
        $favorite   = FavoritesProduct::allProduct();
        $session    = Basket::getSession();

        return $this->render('banner', [
            'banner'    => $banner,
            'products'  => $products,
            'favorite'  => $favorite,
            'session'   => $session,
        ]);
    }

    public function actionBannerMobile($id)
    {
        $banner     = BannerMobile::findOne(['id' => $id]);
        $products   = BannerMobileProduct::getSearchProducts($banner);
        $favorite   = FavoritesProduct::allProduct();
        $session    = Basket::getSession();

        return $this->render('banner-mobile', [
            'banner'    => $banner,
            'products'  => $products,
            'favorite'  => $favorite,
            'session'   => $session,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
