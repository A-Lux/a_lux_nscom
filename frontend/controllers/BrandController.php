<?php

namespace frontend\controllers;

use common\models\Brand;
use common\models\Menu;
use common\models\MultilingualActiveRecord;
use frontend\models\search\BrandSearch;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Brand controller
 */
class BrandController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function actionIndex()
    {
        $searchModel    = new BrandSearch();
        $dataProvider   = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    /**
     * Displays a single model.
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {
        $model  = $this->findModel($slug);
        $menu   = Menu::findOne(['url' => '/brand']);

        return $this->render('view', [
            'model'     => $model,
            'menu'      => $menu,
        ]);
    }

    /**
     * Finds the Brand model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Brand::find()->where(['slug' => $slug])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}