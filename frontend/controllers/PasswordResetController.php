<?php

namespace frontend\controllers;


use frontend\models\ResetPasswordForm;

/**
 * Password Reset controller
 */
class PasswordResetController extends FrontendController
{
    
    public function actionIndex()
    {
        $model  = new ResetPasswordForm();

        if ($model->load(\Yii::$app->request->post())) {
            if($model->resetPassword()){
                \Yii::$app->session->setFlash('success', 'На Ваш электронный адрес отправлено данные для авторизации!');
                return $this->redirect('/login');
            }else{
                \Yii::$app->session->setFlash('warning', 'Неправильно указали эл. адрес или Вы не были зарегистрированы!');
                return $this->redirect(['index']);
            }
        }

        return $this->render("index", compact('model'));
    }

    
}