<?php

namespace frontend\controllers;

use common\models\Menu;
use frontend\models\search\DeliverySearch;

/**
 * Delivery controller
 */
class DeliveryController extends FrontendController
{
    public function actionIndex()
    {
        $searchModel        = new DeliverySearch();
        $dataProvider       = $searchModel->search(\Yii::$app->request->queryParams);
        $menu               = Menu::findOne(['url' => '/delivery']);

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
            'menu'          => $menu,
        ]);
    }
}