<?php

namespace frontend\controllers;

use common\models\About;
use common\models\AboutItem;
use common\models\AboutPartners;
use common\models\Brand;

/**
 * About controller
 */
class AboutController extends FrontendController
{
    public function actionIndex()
    {
        $aboutMain          = About::getMain();
        $aboutSecond        = About::getSecond();
        $aboutThird         = About::getThird();
        $brands         = Brand::getMain();

        $itemSpecialization = AboutItem::getSpecialization();
        $itemPartners       = AboutItem::getPartners();

        $aboutPartners      = AboutPartners::getAll();

        return $this->render('index', [
            'aboutMain'             => $aboutMain,
            'aboutSecond'           => $aboutSecond,
            'aboutThird'            => $aboutThird,
            'itemSpecialization'    => $itemSpecialization,
            'itemPartners'          => $itemPartners,
            'aboutPartners'         => $aboutPartners,
            'brands'            => $brands,
        ]);
    }
}