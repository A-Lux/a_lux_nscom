<?php

namespace frontend\controllers;

use common\models\RequestSuggestion;
use frontend\models\RequestForm;
use frontend\models\search\SuggestionSearch;
use yii\db\Exception;

/**
 * Suggestion controller
 */
class SuggestionController extends FrontendController
{
    public function actionIndex()
    {
        $searchModel    = new SuggestionSearch();
        $dataProvider   = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionRequest()
    {
        $model  = new RequestForm();

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->request()) {
                $response   = ['status' => 1, 'message' => \Yii::t('main-message', 'Благодарим Вас за предложение!')];
                return json_encode($response);
            } else {
                $errors = $model->firstErrors;
                foreach ($errors as $error) {
                    $message   = \Yii::t('main-message', $error);
                    break;
                }
                $response   = ['status' => 0, 'message' => $message];
                return json_encode($response);
            }
        }else{
            \Yii::$app->response->statusCode = 405;
            $response   = ['status' => 0, 'message' => 'Ошибка сервера, попробуйте позднее!'];
            return json_encode($response);
        }
    }
}