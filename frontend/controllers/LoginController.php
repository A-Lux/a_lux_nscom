<?php

namespace frontend\controllers;


use common\modules\user\models\LoginForm;

/**
 * Login controller
 */
class LoginController extends FrontendController
{
    /**
     * Logs in a user.
     * @return mixed
     */
    public function actionIndex()
    {
        $model  = new LoginForm();

        try {
            if ($model->load(\Yii::$app->request->post()) && $model->login()) {
                \Yii::$app->session->setFlash('success', 'Вы успешно вошли в свой аккаунт');
                return $this->redirect('/profile');
            }
        }catch (\Exception $e){
            \Yii::$app->session->addFlash('error', $e->getMessage());
        }

        return $this->render('index', compact('model'));
    }

    /**
     * Logs out the current user.
     * @return mixed
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();

        return $this->goHome();
    }
}