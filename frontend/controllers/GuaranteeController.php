<?php

namespace frontend\controllers;

use common\models\Menu;
use frontend\models\search\GuaranteeSearch;
use yii\helpers\Url;

/**
 * Guarantee controller
 */
class GuaranteeController extends FrontendController
{
    public function actionIndex()
    {
        $searchModel    = new GuaranteeSearch();
        $dataProvider   = $searchModel->search(\Yii::$app->request->queryParams);
        $menu           = Menu::findOne(['url' => '/guarantee']);

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
            'menu'          => $menu,
        ]);
    }
}