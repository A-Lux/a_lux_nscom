<?php

namespace frontend\controllers;

use common\models\OrderedProducts;
use common\models\Orders;
use common\models\User;
use frontend\models\ClientForm;
use frontend\models\OrderForm;

class OrderController extends FrontendController
{
    public function actionCheckout()
    {
        $model      = new OrderForm();
        $ordered    = new OrderedProducts();

        if ($model->load(\Yii::$app->request->post()) && $order   = $model->order()) {
            if($ordered->createOrderedProduct($order)) {
                unset($_SESSION['basket']);
                \Yii::$app->session->setFlash('info', 'Выберите тип оплаты!');
                return $this->redirect('/payment-method/index?id=' . $order->id);
            }
        }
    }

    public function actionFormBasket()
    {
        $model      = new OrderForm();
        $ordered    = new OrderedProducts();

        if ($order   = $model->orderCreate(\Yii::$app->request->post()['Orders'])) {
            if($ordered->createOrderedProduct($order)) {
                if(\Yii::$app->user->isGuest){
                    $data   = 1;
                }else{
                    $data   = 0;
                }
                $response   = ['id' => $order->id, 'user' => $data];

                return json_encode($response);
            }
        }
    }

    public function actionRegistrationUser()
    {
        $model  = new ClientForm();
        $order  = new OrderForm();


        if(!$model->findClient(\Yii::$app->request->get()['id'])) {
            if ($user = $model->createClient(\Yii::$app->request->get()['id'])) {
                $order->orderUpdate($user->id, \Yii::$app->request->get()['id']);
                return 1;
            }
        }else{
            return 0;
        }
    }

    public function actionPaymentMethod($id)
    {
        $order  = Orders::findOne(['id' => $id]);

        return $this->render('payment-method', compact('order'));
    }

    public function actionTest()
    {
        $order_id   = 4;

        $model  = new ClientForm();
        $order  = new OrderForm();

        $user = 20;




        print_r("<pre>");
        print_r($order->orderUpdate($user, $order_id));die;
    }
}