<?php

namespace frontend\controllers;

use common\models\FavoritesProduct;

class FavoritesController extends FrontendController
{
    public function actionAddFavorite()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $product_id = $_GET['id'];
        $favorite   = FavoritesProduct::findOne(['product_id' => $product_id]);

        if(\Yii::$app->user->isGuest == false){
            if($favorite   !== null ){
                if($favorite->delete()){
                    $message    = 'Товар удален с избранного!';
                    $response   = ['status' => 2, 'message' => $message];
                    return $response;
                }
            }else{
                $favorite   = new FavoritesProduct();
                $favorite->user_id  = \Yii::$app->user->identity->id;
                $favorite->product_id   = $product_id;

                if ($favorite->save()){
                    $message    = 'Товар добавлен в избранное!';
                    $response   = ['status' => 1, 'message' => $message];
                    return $response;
                }
            }
        }else{
            $message    = 'Для добавления в избранное нужно войти в аккаунт';
            \Yii::$app->response->statusCode = 405;

            $response   = ['status' => 0, 'message' => $message];
            return $response;
        }
    }
}