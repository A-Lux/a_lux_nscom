<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/style/all.css',
        'css/style/bootstrap.min.css',
//        'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.3/toastr.min.css',
        'slick/slick-theme.css',
        'slick/slick.css',
        'css/owl/owl.theme.default.min.css',
        'css/owl/owl.carousel.min.css',
        'https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css',
        'css/jquery/jquery-ui.css',
        'css/jquery/jquery-ui.css',
        'css/jquery/jquery-ui.structure.css',
        'css/jquery/jquery-ui.theme.min.css',
    ];
    public $js = [
        "/js/request.js",
        "/js/basket.js",
        "/js/profile.js",
        "/js/favorites.js",
        "/js/ajax.js",
        "js/site.js",
        "js/bootstrap.min.js",
        "js/jquery-ui.js",
        "js/owl.carousel.min.js",
        "slick/slick.js",
        "js/main.js",
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
//        'yii\web\JqueryAsset',
    ];
}
