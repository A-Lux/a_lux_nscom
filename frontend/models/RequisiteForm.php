<?php

namespace frontend\models;

use common\models\UserRequisites;
use yii\base\Model;
use yii\helpers\Html;

class RequisiteForm extends Model
{
    public $legal_name;
    public $bin;
    public $legal_address;
    public $name_bank;
    public $bik;
    public $iik;
    public $phone;
    public $email;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'legal_name'    => 'Юридическое название компании',
            'bin'           => 'БИН/ИИН',
            'legal_address' => 'Юридический адрес',
            'name_bank'     => 'Наименование банка',
            'bik'           => 'БИК',
            'iik'           => 'ИИК',
            'phone'         => 'Номер телефона',
            'email'         => 'Электронная почта',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['legal_name', 'bin', 'legal_address', 'name_bank', 'bik', 'iik', 'phone', 'email'], 'required'],
            [['legal_name', 'bin', 'legal_address', 'name_bank', 'bik', 'iik', 'phone', 'email'], 'string', 'max' => 255],

        ];
    }

    /**
     * Create new user requisite
     * @return mixed
     */
    public function requisite()
    {
        if ($this->validate()) {
            if(!($requisites  = UserRequisites::getOne())){
                $requisites                      = new UserRequisites();
            }
            $requisites->user_id             = \Yii::$app->user->identity->id ? \Yii::$app->user->identity->id : null;
            $requisites->legal_name          = $this->legal_name;
            $requisites->bin                 = $this->bin;
            $requisites->legal_address       = $this->legal_address;
            $requisites->name_bank           = $this->name_bank;
            $requisites->bik                 = $this->bik;
            $requisites->iik                 = $this->iik;
            $requisites->email               = $this->email;
            $requisites->phone               = $this->phone;
            $requisites->save();

            return $requisites;
        }

        return false;
    }
}