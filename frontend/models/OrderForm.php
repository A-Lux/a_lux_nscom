<?php

namespace frontend\models;

use common\models\Client;
use common\models\Orders;
use common\models\User;
use yii\base\Model;
use yii\helpers\Html;

class OrderForm extends Model
{
    public $username;
    public $status;
    public $address;
    public $email;
    public $phone;
    public $message;
    public $sum;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status'        => 'Статус',
            'username'      => 'ФИО',
            'address'       => 'Адрес',
            'email'         => 'Электронная почта',
            'phone'         => 'Телефон',
            'message'       => 'Комментарий',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            [['status'], 'integer'],

            [['address', 'email', 'phone'], 'required'],
            [['address', 'email', 'phone'], 'string', 'max' => 128],

            [['message'], 'string'],

        ];
    }

    /**
     * Create new order
     * @return mixed
     */
    public function order()
    {
        if ($this->validate()) {
            $order                      = new Orders();
            $order->order_id            = time();
            $order->user_id             = \Yii::$app->user->identity->id ? \Yii::$app->user->identity->id : null;
            $order->username            = $this->username;
            $order->sum                 = $this->sum;
            $order->status              = 1;
            $order->address             = $this->address;
            $order->email               = $this->email;
            $order->phone               = $this->phone;
            $order->message             = $this->message;
            $order->save();

            return $order;
        }

        return false;
    }

    /**
     * Create new order
     * @param $model
     * @return mixed
     */
    public function orderCreate($model)
    {
        if (!empty($model)) {
            $order                      = new Orders();
            $order->order_id            = time();
            $order->user_id             = \Yii::$app->user->identity->id ? \Yii::$app->user->identity->id : null;
            $order->username            = $model['username'];
            $order->sum                 = $model['sum'];
            $order->status              = 1;
            $order->address             = $model['address'];
            $order->email               = $model['email'];
            $order->phone               = $model['phone'];
            $order->message             = $model['message'];
            $order->save();

            return $order;
        }

        return false;
    }

    /**
     * Update new order
     * @param $user_id
     * @param $order_id
     * @return mixed
     */
    public function orderUpdate($user_id, $order_id)
    {
        $order                      = Orders::findOne(['id' => $order_id]);
        $order->user_id             = $user_id;
        $order->save();

        return $order;
    }
}