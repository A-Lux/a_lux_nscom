<?php

namespace frontend\models;

use common\models\Client;
use common\models\Orders;
use common\models\User;
use common\modules\user\models\BaseUser;
use yii\base\Model;
use yii\helpers\Html;

class ClientForm extends Model
{
    public $username;
    public $company;
    public $status;
    public $region;
    public $city;
    public $work_phone;
    public $mobile_phone;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status'        => 'Статус',
            'username'      => 'Имя',
            'company'       => 'Компания',
            'region'        => 'Регион',
            'city'          => 'Город',
            'work_phone'    => 'Рабочий телефон',
            'mobile_phone'  => 'Мобильный телефон',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            [['status'], 'integer'],

            [['region', 'city'], 'required'],
            [['region', 'city', 'work_phone', 'mobile_phone', 'company'], 'string', 'max' => 128],

        ];
    }

    /**
     * Create new client
     * @param $user
     * @return mixed
     */
    public function signClient($user)
    {
        if ($this->validate()) {
            $client                     = new Client();
            $client->user_id            = $user->id;
            $client->username           = $this->username;
            $client->status             = $this->status;
            $client->company            = $this->company;
            $client->region             = $this->region;
            $client->city               = $this->city;
            $client->work_phone         = $this->work_phone;
            $client->mobile_phone       = $this->mobile_phone;
            $client->save();

            return $client;
        }

        return false;
    }

    public function findClient($order_id)
    {
        $order  = Orders::findOne(['id' => $order_id]);
        $user   = User::findOne(['email' => $order->email]);

        return $user;
    }

    /**
     * Create new user and client
     * @param $order_id
     * @return mixed
     */
    public function createClient($order_id)
    {
        if (!empty($order_id)) {
            $model  = Orders::findOne(['id' => $order_id]);
            $user                       = $this->createUser($model);
            $client                     = new Client();
            $client->user_id            = $user->id;
            $client->username           = $model->username;
            $client->status             = Client::STATUS_USER;
            $client->company            = null;
            $client->region             = null;
            $client->city               = null;
            $client->work_phone         = null;
            $client->mobile_phone       = $model->phone;
            $client->save();

            return $client;
        }

        return false;
    }

    public function createUser($model)
    {
        if(!$user    = User::findOne(['email' => $model->email])){
            $user           = new User();
            $user->email    = $model->email;
            $password       = $this->getRandomPassword();
            $user->setPassword($password);
            $user->role     = BaseUser::ROLE_USER;
            $user->status   = BaseUser::STATUS_ACTIVE;
            $user->generateAuthKey();
            $user->generateToken();

            if($user->save(false)){
                if($this->sendInformationRegistration($user->email, $password)){
                    return $user;
                }
            }
        }
    }

    protected function sendInformationRegistration($email, $password)
    {
        $host = \Yii::$app->request->hostInfo;

        $emailSend = \Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'ТОО «NSCOM»'])
            ->setTo($email)
            ->setSubject('Регистрация и данные')
            ->setHtmlBody("<p>Вы получили данное письмо, т.к. на Вы зарегистрировались на сайте <a href='$host'>«NSCOM»</a>.</p> </br>
                                 </br>
                                 <p>Ваши данные для авторизации:</p> </br></br>
                                 <p>E-mail: $email</p>
                                 <p>Ваш новый пароль: $password</p> </br></br>
                                 <p>---</p></br>
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>«NSCOM»</a></p>");

        return $emailSend->send();

    }

    public function getRandomPassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $password = "";
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $password.= $alphabet[$n];
        }
        return $password;
    }
}