<?php

namespace frontend\models;

use common\models\Catalog;
use common\models\Product;
use yii\base\Model;
use yii\helpers\Html;

class RecommendedProduct extends Model
{
    public static function getProduct($product)
    {
        $recommend    = self::recommended($product);

        return $recommend;
    }

    protected static function recommended($product)
    {
        $recommend      = [];
        $products       = Product::findAll(['category_id' => $product->category_id]);

        $i = 0;
        foreach ($products as $item){
            if($i !== 5 && $item->id !== $product->id) {
                $recommend[] = $item;
                $i++;
            }else{
                break;
            }
        }


        return $recommend;
    }
}