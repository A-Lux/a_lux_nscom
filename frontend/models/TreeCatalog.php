<?php

namespace frontend\models;

use common\models\Catalog;
use common\models\FilterEntity;
use common\models\Product;
use yii\base\Model;
use yii\helpers\Html;

class TreeCatalog extends Catalog
{
    private function getFilterEntity($tree)
    {
        $array  = [];
        foreach ($tree as $item){
            $array[] = $item->id;
        }

        $arr    = implode(',', $array);
        $filter = FilterEntity::find()->where('product_id in (' . $arr . ')')->all();

        return $filter;
    }

    public function getFilterValue($tree)
    {
        $entity = $this->getFilterEntity($tree);

        $array  = [];
        $temp   = [];
        foreach ($entity as $value){
            if(!in_array($value->value->id, $temp)) {
                $temp[]                     = $value->value->id;
                $array[$value->value->id]   = $value->value;
            }
        }

        return $array;
    }

    public function getFilterAttribute($tree)
    {
        $filterValue    = $this->getFilterValue($tree);
        $array          = [];
        $temp           = [];

        foreach ($filterValue as $value){
            if(!in_array($value->attribute0->id, $temp)) {
                $temp[]                         = $value->attribute0->id;
                $array[$value->attribute0->id]  = $value->attribute0;
            }
        }

        return $array;
    }

    public function get_tree($id)
    {
        $result = $this->getCat();

        $arr = [];
        foreach ($result as $val1) {
            foreach ($val1 as $val2) {
                if ($val2->id == $id) {
                    $arr[] = $val2->id;
                    if (!empty($result[$val2->id])) {
                        foreach ($result[$val2->id] as $val3) {
                            $arr[] = $val3->id;
                            if (!empty($result[$val3->id])) {
                                foreach ($result[$val3->id] as $val4) {
                                    $arr[] = $val4->id;
                                    if (!empty($result[$val4->id])) {
                                        foreach ($result[$val4->id] as $val5) {
                                            $arr[] = $val5->id;
                                            if (!empty($result[$val5->id])) {
                                                foreach ($result[$val5->id] as $val6) {
                                                    $arr[] = $val6->id;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        if ($arr) {
            $arr = Product::find()->where('category_id in (' . implode(',', $arr) . ')')->all();
        }

        return $arr;
    }

    public function getCat()
    {
        $catalog    = Catalog::findAll(['status' => self::STATUS_ACTIVE]);
        $array = [];
        foreach ($catalog as $category){
            if(empty($array[$category['parent_id']])){
                $array[$category['parent_id']] = [];
            }
            $array[$category['parent_id']][]    = $category;
        }

        return $array;
    }
}