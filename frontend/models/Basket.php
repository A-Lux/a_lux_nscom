<?php

namespace frontend\models;

use common\models\Product;
use yii\base\Model;
use yii\helpers\Html;

class Basket extends Model
{
    public static function getSession()
    {
        $array  = [];
        if (isset($_SESSION['basket'])) {
            foreach ($_SESSION['basket'] as $value) {
                $array[] = $value['id'];
            }

            return $array;
        }

        return $array;
    }

    public static function getAdd($id)
    {
        $array['id']        = $id;
        $array['count']     = 1;

        return $array;
    }

    public static function getProducts($basket)
    {
        $product    = [];
        foreach ($basket as $value){
            $temp  = Product::findOne(['id' => $value['id']]);
            $temp->count    = $value['count'];
            $product[] = $temp;
        }

        return $product;
    }

    public static function getPrice($basket)
    {
        $total  = 0;

        foreach ($basket as $value){
            $product    = Product::findOne(['id' => $value['id']]);
            $total     += (int)$value['count'] * (int)$product->price;
        }

        return $total;
    }

    public static function upProduct($id)
    {
        $count = 1;
        foreach ($_SESSION['basket'] as $key => $value){
            if($value['id'] == $id){
                $_SESSION['basket'][$key]['count'] += 1;
                $count = $_SESSION['basket'][$key]['count'];
                break;
            }
        }

        return $count;
    }

    public static function downProduct($id)
    {
        $count  = 1;
        foreach ($_SESSION['basket'] as $key => $value){
            if($value['id'] == $id && $_SESSION['basket'][$key]['count'] > 1){
                $_SESSION['basket'][$key]['count']  -= 1;
                $count  = $_SESSION['basket'][$key]['count'];
                break;
            }
        }

        return $count;
    }

    public static function countProduct($id, $count)
    {
        foreach ($_SESSION['basket'] as $key => $value){
            if($value['id'] == $id){
                if($count > 0){
                    $_SESSION['basket'][$key]['count'] = $count;
                }else{
                    $_SESSION['basket'][$key]['count'] = 1;
                }
            }
        }
    }

    public static function deleteProduct($id)
    {
        $status = false;
        foreach ($_SESSION['basket'] as $key => $value){
            if($value['id'] == $id){
                unset($_SESSION['basket'][$key]);
                $status = true;
                break;
            }
        }

        return $status;
    }
}