<?php

namespace frontend\models;

use common\models\FavoritesProduct;
use common\models\Product;
use frontend\models\Basket;
use yii\base\Model;
use yii\helpers\Html;

class ProductAttribute extends Model
{
    public static function getSession($product_id)
    {
        $session    = Basket::getSession();

        if(in_array($product_id, $session)){
            return 1;
        }else{
            return 0;
        }
    }

    public static function getFavorite($product_id)
    {
        $favorite   = FavoritesProduct::allProduct();

        if(in_array($product_id, $favorite)){
            return 1;
        }else{
            return 0;
        }
    }
}