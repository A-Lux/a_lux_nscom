<?php
namespace frontend\models;

use yii\base\InvalidArgumentException;
use yii\base\Model;
use common\models\User;
use yii\helpers\Html;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $email;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * Resets password.
     *
     * @return
     */
    public function resetPassword()
    {
       if($user    = User::findByEmail($this->email)){
           $password = $this->getRandomPassword();
           $user->setPassword($password);
           if($user->save(false)){
               if($this->sendInformationAboutNewPassword($user->email, $password)){
                   return 1;
               }
           }
       }

        return 0;
    }

    protected function sendInformationAboutNewPassword($email, $password)
    {
        $host = \Yii::$app->request->hostInfo;

        $emailSend = \Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'ТОО «NSCOM»'])
            ->setTo($email)
            ->setSubject('Восстановление доступа')
            ->setHtmlBody("<p>Вы получили данное письмо, т.к. на ваш e-mail был полуен запрос на восстановление пароля.</p> </br>
                                 </br>
                                 <p>Ваши данные для авторизации:</p> </br></br>
                                 <p>E-mail: $email</p>
                                 <p>Ваш новый пароль: $password</p> </br></br>
                                 <p>---</p></br>
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>«NSCOM»</a></p>");

        return $emailSend->send();

    }

    public function getRandomPassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $password = "";
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $password.= $alphabet[$n];
        }
        return $password;
    }
}
