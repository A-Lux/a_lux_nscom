<?php

namespace frontend\models;

use common\models\Client;
use yii\base\Model;
use yii\helpers\Html;

class ProfileForm extends Model
{
    public $username;
    public $company;
    public $status;
    public $region;
    public $city;
    public $work_phone;
    public $mobile_phone;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status'        => 'Статус',
            'username'      => 'Имя',
            'company'       => 'Компания',
            'region'        => 'Регион',
            'city'          => 'Город',
            'work_phone'    => 'Рабочий телефон',
            'mobile_phone'  => 'Мобильный телефон',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            [['status'], 'integer'],

            [['region', 'city', 'work_phone', 'mobile_phone', 'company'], 'string', 'max' => 128],

        ];
    }

    /**
     * Update client
     * @return mixed
     */
    public function updateClient()
    {
        $client                     = Client::findOne(['user_id' => \Yii::$app->user->identity->id]);
        if ( $client !== null && $this->validate()) {
            $client->username           = $this->username;
            $client->status             = $this->status;
            $client->company            = $this->company;
            $client->region             = $this->region;
            $client->city               = $this->city;
            $client->work_phone         = $this->work_phone;
            $client->mobile_phone       = $this->mobile_phone;
            $client->save();

            return $client;
        }

        return false;
    }
}