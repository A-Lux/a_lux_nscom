<?php

namespace api\controllers;

use Yii;
use common\models\User;
use yii\filters\AccessControl;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;

class UserController extends ActiveController
{
    public $modelClass = 'common\models\User';

    public function behaviors()
    {
		return array_merge(parent::behaviors(), [
			// For cross-domain AJAX request
			'corsFilter'  => [
				'class' => \yii\filters\Cors::className(),
			],
			[
				'class' => HttpBearerAuth::className(),
				'except' => ['create', 'index', 'getuser'],
			],
		]);
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create']);
        unset($actions['update']);

        return $actions;
    }
}