<?php

namespace api\controllers;

use Yii;
use yii\rest\Controller;
use api\models\LoginForm;

class SiteController extends Controller
{	
    public function actionIndex()
    {
        return 'api';
    }

    public static function actionLogin()
    {
        $model = new LoginForm();
        $model->load(Yii::$app->request->queryParams, '');
//        $model->load(Yii::$app->request->getBodyParams(), '');
        if ($token = $model->auth()) {
            return ['token' => $token];
        } else {
            return $model;
        }
    }

    protected function verbs()
    {
        return [
            'login' => ['post', 'get'],
        ];
    }
}
