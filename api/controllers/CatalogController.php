<?php

namespace api\controllers;

use frontend\models\ProductAttribute;
use common\models\Catalog;
use common\models\FilterEntity;
use common\models\Product;
use yii\rest\Controller;

class CatalogController extends Controller
{
    public function actionFilter($catalog)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $value          = $_GET['value'];

        $filter         = $this->getFilterEntity($catalog, $value);

        return $filter;
    }

    private function getFilterEntity($id, $value)
    {
        $tree   = $this->get_tree($id);

        $productId      = implode(',', $tree);
        $valueId        = implode(',', $value);

        $filter = FilterEntity::find()->where('product_id in (' . $productId . ')
                                AND value_id in (' . $valueId . ')')->all();

        $product    = [];
        $temp       = [];
        foreach ($filter as $value){
            if(!in_array($value->product_id, $temp)) {
                $temp[]         = $value->product_id;
                $item           = $value->product;

                $item->favorite = ProductAttribute::getFavorite($value->product_id);
                $item->session  = ProductAttribute::getSession($value->product_id);

                $product[]      = $item;
            }
        }

        return $product;
    }

    public function get_tree($id)
    {
        $result = $this->getCat();

        $arr = [];
        foreach ($result as $val1) {
            foreach ($val1 as $val2) {
                if ($val2->id == $id) {
                    $arr[] = $val2->id;
                    if (!empty($result[$val2->id])) {
                        foreach ($result[$val2->id] as $val3) {
                            $arr[] = $val3->id;
                            if (!empty($result[$val3->id])) {
                                foreach ($result[$val3->id] as $val4) {
                                    $arr[] = $val4->id;
                                    if (!empty($result[$val4->id])) {
                                        foreach ($result[$val4->id] as $val5) {
                                            $arr[] = $val5->id;
                                            if (!empty($result[$val5->id])) {
                                                foreach ($result[$val5->id] as $val6) {
                                                    $arr[] = $val6->id;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        if ($arr) {
            $arr = Product::find()->where('category_id in (' . implode(',', $arr) . ')')->all();
        }

        $productId = [];
        foreach ($arr as $item){
            $productId[]    = $item->id;
        }

        return $productId;
    }

    public function getCat()
    {
        $catalog    = Catalog::findAll(['status' => Catalog::STATUS_ACTIVE]);
        $array = [];
        foreach ($catalog as $category){
            if(empty($array[$category['parent_id']])){
                $array[$category['parent_id']] = [];
            }
            $array[$category['parent_id']][]    = $category;
        }

        return $array;
    }
}